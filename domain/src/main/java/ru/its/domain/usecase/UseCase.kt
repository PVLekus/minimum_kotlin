package ru.its.domain.usecase

import rx.Observable

/**
 * Created by oleg on 03.03.18.
 */
abstract class UseCase<T, Params> {

    abstract fun createObservable(params: Params): Observable<T>

}
