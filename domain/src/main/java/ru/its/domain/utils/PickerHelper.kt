package ru.its.domain.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by oleg on 18.12.17.
 */
class PickerHelper {

    private val rightNow = Calendar.getInstance()

    fun getSortedListOfHours() : List<String> {

        return rightNow.get(Calendar.HOUR_OF_DAY).let { hour ->
            (hour..23).map { if (it < 10) "0$it" else "$it" }
                    .toMutableList()
                    .also { it.addAll((0..hour).map { if (it < 10) "0$it" else "$it" }) }.distinct()

        }

    }

    fun getSortedListOfMinutes() : List<String> {

        return rightNow.get(Calendar.MINUTE).let { minute ->
            (minute..59).filter { it % 5 == 0 }.map { if (it < 10) "0$it" else "$it" }
                    .toMutableList()
                    .also { it.addAll((0..minute).filter { it % 5 == 0 }.map { if (it < 10) "0$it" else "$it" }) }.distinct()

        }

    }

    fun getSortedListOfDates() : List<String> {

        val date = rightNow
        date.add(Calendar.DAY_OF_MONTH, 1)
        val format = SimpleDateFormat("dd.MM.yy")
        val list = mutableListOf<String>("Сегодня","Завтра")
        (0..4).map { format.format( date.also {  it.add(Calendar.DAY_OF_MONTH, 1) }.time ) }.let { list.addAll(it)  }

        return list

    }

}