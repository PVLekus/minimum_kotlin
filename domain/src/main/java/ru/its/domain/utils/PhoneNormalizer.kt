package ru.its.domain.utils

/**
 * Created by oleg on 06.11.17.
 */
class PhoneNormalizer {

    fun normalize(phone: String, clearPrefix: Boolean = false) : String {

        var newPhone = phone.replace("(","")
        newPhone = newPhone.replace(")","")
        newPhone = newPhone.replace("-","")
        newPhone = newPhone.replace(" ","")

        if (!clearPrefix) return newPhone

        if (newPhone.startsWith("+7")) newPhone = newPhone.substring(2)
        else if (newPhone.startsWith("8")) newPhone = newPhone.substring(1)

        return newPhone
    }

    fun validateNumber(phone: String?): Boolean {

        if (phone?.matches("^[0-9]{10}$".toRegex()) == true) return true

        return false
    }

}