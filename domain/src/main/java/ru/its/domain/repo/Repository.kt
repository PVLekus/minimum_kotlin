package ru.its.domain.repo

/**
 * Created by oleg on 26.12.17.
 */
interface Repository {

    fun clear()

}

enum class SourceType {
    CACHE,
    NETWORK,
    BOTH,
    FIRST
}

