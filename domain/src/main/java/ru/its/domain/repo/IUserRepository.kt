package ru.its.domain.repo

import ru.its.domain.model.City
import ru.its.domain.model.Rate
import ru.its.domain.model.User

/**
 * Created by oleg on 03.03.18.
 */
interface IUserRepository {

    fun updatePhone(phone: String) : User?
    fun updateName(name: String) : User?
    fun updateToken(token: String) : User?
    fun updateCity(city: City) : User?
    fun updateRate(rate: Rate?) : User?
    fun isCardPayment(): Boolean
    fun getUser(): User?
    fun clear()
    fun save(user: User?): User?

}