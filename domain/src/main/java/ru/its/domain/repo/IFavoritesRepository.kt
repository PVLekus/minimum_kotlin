package ru.its.domain.repo

import ru.its.domain.model.FavoritePoint
import ru.its.domain.model.SearchPoint
import rx.Observable

interface IFavoritesRepository: Repository  {
    fun getAll(sourceType: SourceType): Observable<List<FavoritePoint>?>
    fun addToFavorite(searchPoint: SearchPoint, name: String?): Observable<Boolean>
    fun removeFavorite(favoritePoint: FavoritePoint): Observable<Boolean>
}