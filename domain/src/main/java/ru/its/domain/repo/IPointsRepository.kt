package ru.its.domain.repo

import ru.its.domain.model.SearchPoint
import rx.Observable

/**
 * Created by oleg on 03.03.18.
 */
interface IPointsRepository: Repository {
    fun getAll(): Observable<List<SearchPoint>?>
}

interface IHistoryPointsRepository: IPointsRepository
interface IAirportsRepository: IPointsRepository
interface IStationsRepository: IPointsRepository
interface IPopularRepository: IPointsRepository
