package ru.its.domain.repo

import ru.its.domain.model.Booking
import ru.its.domain.model.SearchPoint
import rx.Observable

/**
 * Created by oleg on 03.03.18.
 */
interface IBookingRepository: Repository {
    fun get(): Observable<Booking>
    fun updateComment(comment: String?): Observable<Booking>
    fun updateItemByPosition(searchPoint: SearchPoint, position: Int, comment: String?): Observable<List<SearchPoint>>
    fun deleteItemByPosition(position: Int): Observable<List<SearchPoint>>
    fun clearItemByPosition(position: Int): Observable<List<SearchPoint>>
    fun addNewPoint(): Observable<List<SearchPoint>>
    fun dicreaseItemPosition(position: Int): Observable<List<SearchPoint>>
    fun increaseItemPosition(position: Int): Observable<List<SearchPoint>>
    fun save(booking: Booking): Booking
    fun setCardPay(isCard: Boolean)
}