package ru.its.domain.repo

import ru.its.domain.model.City
import rx.Observable

/**
 * Created by oleg on 03.03.18.
 */
interface ICitiesRepository {
    fun isCityExist(city: City?): Boolean
    fun getFirstCity(): City?
    fun getFirstCity(name: String): City?
    fun getAll(sourceType: SourceType): Observable<List<City>?>

}