package ru.its.domain.repo

import ru.its.domain.model.Card
import rx.Observable

/**
 * Created by oleg on 09.03.18.
 */
interface ICardsRepository {

    fun getCard(): Observable<Card?>
    fun removeCard(): Observable<Boolean>
    fun addCard(): Observable<String>
    fun clear()

}