package ru.its.domain.repo

import ru.its.domain.model.Order
import rx.Observable

interface IHistoryRepository: Repository {
    fun refreshFirstPage(): Observable<List<Order>?>
    fun getFirstPage(sourceType: SourceType): Observable<List<Order>?>
    fun getPage(page: Int): Observable<Pair<Int,List<Order>?>>
    fun removeOrder(id: String): Observable<Boolean>
}