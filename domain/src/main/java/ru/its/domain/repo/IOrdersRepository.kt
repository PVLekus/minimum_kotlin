package ru.its.domain.repo

import ru.its.domain.model.Order
import rx.Observable

/**
 * Created by oleg on 03.03.18.
 */
interface IOrdersRepository: Repository {

    fun getAll(sourceType: SourceType): Observable<List<Order>?>
    fun updateOrder(order: Order): Observable<Order>
    fun getOrder(id: String): Observable<Order?>

}