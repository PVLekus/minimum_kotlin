package ru.its.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 13.09.17.
 */

open class City {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("name")
    var name: String? = null
    @SerializedName("currency")
    var currency: String = "RUB"
    @SerializedName("map_type")
    var map_type: String? = null
    @SerializedName("center")
    var center: Center? = null
    @SerializedName("defaultRate")
    var rate: Rate? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as City

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }

}

open class Center {

    @SerializedName("lat")
    var lat: String? = null
    @SerializedName("lon")
    var lon: String? = null

}