package ru.its.domain.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 22.10.17.
 */
open class SearchPoint {

    @SerializedName(value ="id", alternate = arrayOf("object_id"))
    var id: Int? = null
    @field:SerializedName("metatype")
    var metatype: Int? = null
    @field:SerializedName("name")
    var name: String? = null
    @field:SerializedName("house")
    var house: String? = null
    @field:SerializedName("enter")
    var enter: String? = null
    @field:SerializedName("type")
    var type: String? = null
    @field:SerializedName("address")
    var address: String? = null
    @field:SerializedName("isFavorite")
    var favorite = false
    @Expose(serialize = false)
    var preOrder: String? = null

    companion object {
        fun from(favoritePoint: FavoritePoint): SearchPoint =
                SearchPoint().also {
                    it.id = favoritePoint.objectId
                    it.metatype = favoritePoint.metatype
                    it.name = favoritePoint.details?.name
                    it.house = favoritePoint.house
                    it.enter = favoritePoint.enter
                    it.type = favoritePoint.type
                    it.address = favoritePoint.details?.address
                }
    }

}

class SearchPointWrapper {

    var point: SearchPoint? = null
    var type: Int = -1
    var typeSelect: PointType = PointType.AIRPORTS
    var name: String? = null

}

enum class PointType {
    AIRPORTS,
    STATIONS,
    POPULARS,
    FAVORITES
}