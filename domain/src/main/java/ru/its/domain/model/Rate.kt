package ru.its.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 21.10.17.
 */
data class Rate(

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("id")
        val id: Int? = null,

        @field:SerializedName("params")
        val params: Params? = null
)

data class Params(

        @field:SerializedName("card_payment")
        val cardPayment: String? = null,

        @field:SerializedName("auto_card_payment")
        val autoCardPayment: Int? = null,

        @field:SerializedName("sms_sender")
        val smsSender: String? = null,

        @field:SerializedName("corp")
        val corp: String? = null,

        @field:SerializedName("driver_selector")
        val driverSelector: String? = null,

        @field:SerializedName("no_smoke")
        val noSmoke: String? = null,

        @field:SerializedName("tax")
        val tax: String? = null,

        @field:SerializedName("pre_order")
        val preOrder: String? = null,

        @field:SerializedName("terminal")
        val terminal: String? = null,

        @field:SerializedName("driver_phone")
        val driverPhone: String? = null,

        @field:SerializedName("child_seat")
        val childSeat: String? = null,

        @field:SerializedName("operator_phone")
        val operatorPhone: String? = null,

        @field:SerializedName("fast")
        val fast: String? = null,

        @field:SerializedName("hour")
        val hour: String? = null,

        @field:SerializedName("max_time_for_preorder")
        val maxTimeForPreorder: String? = null,

        @field:SerializedName("luggage")
        val luggage: String? = null,

        @field:SerializedName("need_money_change")
        val needMoneyChange: String? = null,

        @field:SerializedName("yellow_number_car")
        val yellowNumberCar: Int? = null,

        @field:SerializedName("animal")
        val animal: String? = null,

        @field:SerializedName("use_table")
        val useTable: String? = null,

        @field:SerializedName("receipt")
        val receipt: String? = null,

        @field:SerializedName("vip")
        val vip: String? = null,

        @field:SerializedName("foreigner")
        val foreigner: String? = null
)