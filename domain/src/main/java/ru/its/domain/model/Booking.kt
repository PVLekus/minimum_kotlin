package ru.its.domain.model

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by oleg on 16.11.17.
 */
class Booking {

    @SerializedName("comment")
    var comment: String = ""
    @SerializedName("preOrderTime")
    var preOrderTime: String? = null
    @SerializedName("isCard")
    var isCardPay: Boolean? = null
    @SerializedName("points")
    var points: MutableList<SearchPoint> = mutableListOf(SearchPoint(), SearchPoint())

    fun getFormattedPreorderTime(): String? {

        if (preOrderTime == null) return null

        val calendar = GregorianCalendar()
        preOrderTime = preOrderTime!!.replace("Сегодня", SimpleDateFormat("dd.MM.yyy").format(calendar.time))

        calendar.add(Calendar.DATE, 1)
        preOrderTime = preOrderTime!!.replace("Завтра", SimpleDateFormat("dd.MM.yyy").format(calendar.time))

        return preOrderTime

    }

}