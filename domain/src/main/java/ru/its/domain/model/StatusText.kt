package ru.its.domain.model

/**
 * Created by oleg on 18.10.17.
 */
class StatusText {

    companion object {

        val UNKNOWN_ERROR = "Упс, что-то пошло не так.. \nМы знаем о проблеме и уже работаем. Попробуйте повторить запрос позже."
        val STATUS_NONAME = "неизвестный статус заказа"
        val OBJECT_TYPE_OTHER = "Магазин, Автосервис и прочие"


        private val STATUS_FAIL = "Операция прошла неудачно"
        private val STATUS_TOO_MANY_REQUESTS = "Слишком много подключений с одного ip"
        private val STATUS_SUCCESS = "Операция прошла успешно"
        private val STATUS_INCORRECT_PHONE = "Неверный формат номера телефона"
        private val STATUS_INCORRECT_CODE = "Неверный код подтверждения"
        private val STATUS_CITY_NOT_FOUND = "Город либо не задан либо не найден"
        private val STATUS_NEED_CODE = "Необходим код подтверждения"
        private val STATUS_USER_NOT_FOUND = "Пользователь не найден"
        private val STATUS_REG_FAILURE = "По техническим причинам Ваш номер не был зарегистрирован. Приносим извинения за временные неудобства!"
        private val STATUS_REG_SUCCESS_BUT_CANT_SEND_SMS = "Ваш номер был успешно зарегистрирован, но SMS с паролем по техническим причинам не было отправлено. Приносим извинения за временные неудобства!"
        private val STATUS_FORBIDDEN_ACCESS = "У вас нет доступа к данному разделу. Необходима авторизация."
        private val STATUS_ERROR_ORDER_NOT_FOUND = "Заказ не найден"
        private val STATUS_ORDER_FAIL = "Ошибка в параметрах заказа"
        private val STATUS_ERROR_IN_SAVE = "Ошибка при сохранении"
        private val STATUS_ERROR_DELETE = "Ошибка при удалении"
        private val STATUS_APP_NOT_FOUND = "Приложение не прошло авторизацию"
        private val STATUS_FAVORITE_OBJECT_NOT_FOUND = "Не найдено в избранном"

        private val STATUS_PREORDER = "Предварительный заказ"
        private val STATUS_FIND_CAR = "Поиск машины"
        private val STATUS_CAR_RIDE = "Машина едет к клиенту"
        private val STATUS_CAR_WAIT = "Машина ждёт клиента"
        private val STATUS_CLIENT_KNOW = "Клиент в курсе"
        private val STATUS_CLIENT_IN_CAR = "Клиент в машине"
        private val STATUS_CLIENT_REFUSED_WITHOUT_CAR = "Отказ без машины"
        private val STATUS_CLIENT_DONT_GO = "Клиент не поехал"
        private val STATUS_ORDER_FINISHED = "Завершен"
        private val STATUS_CANCEL = "Отменен"


        private val OBJECT_TYPE_STREET = "Улица"
        private val OBJECT_TYPE_BUS_STOP = "Остановка"

        fun internetError() = "Ошибка обращения к сети. Проверьте подключение и повторите попытку."

        fun fromErrorCode(code: Int?) : String {

            if (code == null) return UNKNOWN_ERROR

            return when (code) {

                400 -> STATUS_FAIL
                429 -> STATUS_TOO_MANY_REQUESTS
                220 -> STATUS_SUCCESS
                673 -> STATUS_INCORRECT_PHONE
                666 -> STATUS_INCORRECT_CODE
                674 -> STATUS_CITY_NOT_FOUND
                675 -> STATUS_NEED_CODE
                672 -> STATUS_USER_NOT_FOUND
                556 -> STATUS_REG_FAILURE
                557 -> STATUS_REG_SUCCESS_BUT_CANT_SEND_SMS
                401 -> STATUS_FORBIDDEN_ACCESS
                683 -> STATUS_ERROR_ORDER_NOT_FOUND
                690 -> STATUS_ORDER_FAIL
                692 -> STATUS_ERROR_IN_SAVE
                693 -> STATUS_ERROR_DELETE
                694 -> STATUS_APP_NOT_FOUND
                695 -> STATUS_FAVORITE_OBJECT_NOT_FOUND

                else -> UNKNOWN_ERROR
            }

        }

        fun fromStatusCode(code: Int?) : String {

            if (code == null) return STATUS_NONAME

            return when (code) {

                1 -> STATUS_PREORDER
                2 -> STATUS_FIND_CAR
                3 -> STATUS_CAR_RIDE
                4 -> STATUS_CAR_WAIT
                5 -> STATUS_CLIENT_KNOW
                6 -> STATUS_CLIENT_IN_CAR
                102 -> STATUS_CLIENT_REFUSED_WITHOUT_CAR
                103 -> STATUS_CLIENT_DONT_GO
                104 -> STATUS_ORDER_FINISHED
                111 -> STATUS_CANCEL
                0 -> STATUS_NONAME

                else -> STATUS_NONAME
            }

        }

        fun fromObjectCode(code: Int?) : String {

            if (code == null) return OBJECT_TYPE_OTHER

            return when (code) {

                200 -> OBJECT_TYPE_OTHER
                0 -> OBJECT_TYPE_STREET
                100 -> OBJECT_TYPE_BUS_STOP

                else -> OBJECT_TYPE_OTHER
            }

        }

    }

}