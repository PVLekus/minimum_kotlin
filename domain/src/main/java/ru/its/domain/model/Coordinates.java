package ru.its.domain.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by oleg on 21.10.17.
 */

public class Coordinates {

    @SerializedName("client")
    public List<Double> client;
    @SerializedName("driver")
    public List<Double> driver;

}
