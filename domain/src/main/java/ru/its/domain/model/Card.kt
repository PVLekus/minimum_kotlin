package ru.its.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 09.03.18.
 */
data class Card (
        @SerializedName("cardMask")
        val cardMask: String?,
        @SerializedName("cardType")
        val cardType: String?
)