package ru.its.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 02.11.17.
 */
open class User {

    @SerializedName("token")
    var token: String? = null
    @SerializedName("phone")
    var phone: String? = null
    @SerializedName("name")
    var name: String = "Пользователь"
    @SerializedName("guid")
    var guid: String? = null
    @SerializedName("city")
    var city: City? = null

}