package ru.its.domain.model

import com.google.gson.annotations.SerializedName

open class FavoritePoint {

    @SerializedName("id")
    var id: String? = null
    @SerializedName("object_id")
    var objectId: Int? = null
    @field:SerializedName("metatype")
    var metatype: Int? = null
    @field:SerializedName("name")
    var name: String? = null
    @field:SerializedName("house")
    var house: String? = null
    @field:SerializedName("enter")
    var enter: String? = null
    @field:SerializedName("type")
    var type: String? = null
    @field:SerializedName("object")
    var details: Details? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FavoritePoint

        if (id != other.id) return false
        if (objectId != other.objectId) return false
        if (name != other.name) return false
        if (details != other.details) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (objectId ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (details?.hashCode() ?: 0)
        return result
    }

}

class Details {

    @SerializedName("id")
    var id: Int? = null
    @field:SerializedName("name")
    var name: String? = null
    @field:SerializedName("type")
    var type: String? = null
    @field:SerializedName("metatype")
    var metatype: Int? = null
    @field:SerializedName("address")
    var address: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Details

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id ?: 0
    }


}

