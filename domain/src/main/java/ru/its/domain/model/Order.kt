package ru.its.domain.model

import com.google.gson.annotations.SerializedName

/**
 * Created by oleg on 21.10.17.
 */
open class Order {
    @SerializedName("id")
    var id: String? = null
    @SerializedName("status_message")
    var statusMessage: String? = null
    @SerializedName("cost")
    var cost: String? = null
    @SerializedName("is_preorder")
    var isPreorder: Int? = null
    @SerializedName("done_at")
    var doneAt: String? = null
    @SerializedName("is_corp_order")
    var isCorpOrder: Int? = null
    @SerializedName("is_hourly_order")
    var isHourlyOrder: Int? = null
    @SerializedName("created_at")
    var createdAt: String? = null
    @SerializedName("points")
    var points: List<SearchPoint>? = null
    @SerializedName("coordinates")
    var coordinates: Coordinates? = null
    @SerializedName("canceled")
    var canceled: Int? = null
    @SerializedName("preorder_at")
    var preorderAt: String? = null
    @SerializedName("comment")
    var comment: String? = null
    @SerializedName("status")
    var status: Int? = null
    @SerializedName("car_info")
    var car_info: CarInfo? = null
    @SerializedName("options")
    var options: Options? = null
    @SerializedName("payments")
    var payments: List<Payment>? = null
    @SerializedName("rate")
    var rate: Rate? = null

    fun isNeedCardPay(): Boolean =
            options?.card_payment == 1
                    && payments?.get(0)?.status == 0
                    && status == 6
                    && canceled != 1

    fun isAlreadyPayd(): Boolean =
            options?.card_payment == 1
                    && payments?.get(0)?.status != 0

    fun isCanCancel(): Boolean = status?.let { it < 6 } ?: true && canceled != 1

    fun getPaydCost(): String = payments?.get(0)?.cost ?: ""

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Order

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }


}

data class Payment(

        var status: Int?,
        var created_at: String?,
        var cost: String?

)

class CarInfo {

    @SerializedName("color")
    var color: String? = null
    @SerializedName("brand")
    var brand: String? = null
    @SerializedName("number")
    var number: String? = null

}

class Options {

    @SerializedName("auto_card_payment")
    var auto_card_payment: Int? = null
    @SerializedName("card_payment")
    var card_payment: Int? = null

}


