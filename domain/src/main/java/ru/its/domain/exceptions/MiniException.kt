package ru.its.domain.exceptions

/**
 * Created by oleg on 19.10.17.
 */
open class MiniException(message: String) : Exception(message)

class InternetConnectionException(message: String) : MiniException(message)
class ApiResponseException(message: String) : MiniException(message)
class ViewModelException(message: String) : MiniException(message)

