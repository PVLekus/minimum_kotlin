package its.ru.minimum.di.module

import org.koin.core.module.Module
import org.koin.dsl.module
import ru.its.data.usecase.CitySelectUseCase
import ru.its.data.usecase.ClearCacheUseCase
import ru.its.data.usecase.InitPrefsConstUseCase
import ru.its.data.usecase.UserExitUseCase

/**
 * Created by oleg on 21.12.17.
 */

val useCaseModule: Module = module {
    factory { ClearCacheUseCase(get(),get(),get(),get(),get(),get(),get(),get(), get()) }
    factory { InitPrefsConstUseCase(get(),get()) }
    factory { CitySelectUseCase(get(),get(),get()) }
    factory { UserExitUseCase(get(),get()) }
}