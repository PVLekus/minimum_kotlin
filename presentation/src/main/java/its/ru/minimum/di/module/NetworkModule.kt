package its.ru.minimum.di.module

import android.content.Context
import com.readystatesoftware.chuck.ChuckInterceptor
import its.ru.minimum.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.its.data.remote.EncodingInterceptor
import ru.its.data.remote.IRutaxiAPI
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.repo.IUserRepository
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

/**
 * Created by oleg on 03.03.18.
 */

private val TIMEOUT = 60
private val WRITE_TIMEOUT = 120
private val CONNECT_TIMEOUT = 10

val networkModule: Module = module {

    single { provideHttpLoggingInterceptor() }
    single { provideOkHttpClient(get(), androidApplication()) }
    single { getRetrofitService(get()) }
    single { provideRutaxiAPI(get(), get()) }

}


fun provideRutaxiAPI(userRepository: IUserRepository, service: IRutaxiAPI): RutaxiAPI = RutaxiAPI(userRepository, service, true)

fun getRetrofitService(httpClient: OkHttpClient): IRutaxiAPI {
    return Retrofit.Builder()
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .build()
            .create(IRutaxiAPI::class.java)
}


fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor,
                        context: Context): OkHttpClient {

    val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

        override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()

        @Throws(CertificateException::class)
        override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) = Unit

        @Throws(CertificateException::class)
        override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) = Unit
    })

    val sslContext = SSLContext.getInstance("SSL")
    sslContext.init(null, trustAllCerts, java.security.SecureRandom())
    val sslSocketFactory = sslContext.socketFactory

    val httpClientBuilder = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .hostnameVerifier { _, _ -> true }
            .addInterceptor { chain ->
                val request = chain!!.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("X-Parse-Application-Id", "App ${BuildConfig.APP_ID}") //real
//                            .addHeader("X-Parse-Application-Id","App ${BuildConfig.APP_TEST_ID}") // test

                chain!!.proceed(request.build())
            }
    httpClientBuilder.addInterceptor(EncodingInterceptor())

    if (BuildConfig.DEBUG) {
        httpClientBuilder.addInterceptor(ChuckInterceptor(context))
        httpClientBuilder.addInterceptor(httpLoggingInterceptor)
    }

    return httpClientBuilder.build();
}

fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return loggingInterceptor
}
