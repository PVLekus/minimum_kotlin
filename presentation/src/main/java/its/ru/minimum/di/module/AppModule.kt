package its.ru.minimum.di.module

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import its.ru.minimum.activities.login.LoginViewModel
import its.ru.minimum.activities.start.StartViewModel
import its.ru.minimum.pages.cities.CitiesViewModel
import its.ru.minimum.pages.confirm.ConfirmViewModel
import its.ru.minimum.pages.favorites.list.FavoriteListViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Created by oleg on 21.12.17.
 */
val appModule: Module = module {
    factory { androidApplication().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager }
    viewModel { StartViewModel(get(), get(), get(), get(), get(), androidApplication()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { CitiesViewModel(get(), get(), get()) }
    viewModel { ConfirmViewModel(get(), get(), get(), get()) }
    viewModel { FavoriteListViewModel(get(), get(), get()) }
}