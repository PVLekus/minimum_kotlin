package its.ru.minimum.di.module

import com.google.gson.Gson
import its.ru.minimum.MinimumApp
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.dsl.module
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.MapTileIndex
import ru.its.data.repo.*
import ru.its.domain.repo.*

/**
 * Created by oleg on 03.03.18.
 */

val repoModule: Module = module {
    single { Gson() }
    single { PrefHelper(androidApplication()) }
    single { CardsRepository(get()) as ICardsRepository }
    single { BookingRepository(get(), get()) as IBookingRepository }
    single { CitiesRepository(get(), get(), get(), MinimumApp.instance.firebaseRemoteConfig.getLong("time_cities_load")) as ICitiesRepository }
    single { HistoryRepository(get(), get(), get()) as IHistoryRepository }
    single { OrdersRepository(get()) as IOrdersRepository }
    single { UserRepository(get(), get()) as IUserRepository }
    single { HistoryPointsRepository(get(), get(), get(), MinimumApp.instance.firebaseRemoteConfig.getLong("time_history_points")) as IHistoryPointsRepository }
    single { AirportsRepository(get(), get(), get(), MinimumApp.instance.firebaseRemoteConfig.getLong("time_places_load")) as IAirportsRepository }
    single { StationsRepository(get(), get(), get(), MinimumApp.instance.firebaseRemoteConfig.getLong("time_places_load")) as IStationsRepository }
    single { PopularRepository(get(), get(), get(), MinimumApp.instance.firebaseRemoteConfig.getLong("time_places_load")) as IPopularRepository }
    single { FavoritesRepository(get(), get(), get()) as IFavoritesRepository }
    factory { getTileSource(get()) as ITileSource }
}

private fun getTileSource(userRepository: IUserRepository): ITileSource {
    val type = userRepository.getUser()?.city?.map_type ?: "osm"
    return if (type == "2gis") twoGisTileSource
    else osmTileSource
}

private val twoGisTileSource = object : XYTileSource("GIS",2, 18,256, "", arrayOf("http://tile0.maps.2gis.com/tiles")) {
    override fun getTileURLString(pMapTileIndex: Long): String {
        return "$baseUrl?x=${MapTileIndex.getX(pMapTileIndex)}&y=${MapTileIndex.getY(pMapTileIndex)}&z=${MapTileIndex.getZoom(pMapTileIndex)}&v=1"
    }
}

private val osmTileSource = object : XYTileSource("OSM",2, 18,256, "", arrayOf("http://a.tile.openstreetmap.org")) {
    override fun getTileURLString(pMapTileIndex: Long): String {
        return "$baseUrl/${MapTileIndex.getZoom(pMapTileIndex)}/${MapTileIndex.getX(pMapTileIndex)}/${MapTileIndex.getY(pMapTileIndex)}.png"
    }
}