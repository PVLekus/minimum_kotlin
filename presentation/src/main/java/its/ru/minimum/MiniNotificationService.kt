package its.ru.minimum

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import its.ru.minimum.notifications.NotificationHelper
import its.ru.minimum.notifications.PushNotification
import org.koin.android.ext.android.inject
import ru.its.data.repo.PrefHelper

class MiniNotificationService: FirebaseMessagingService() {

    private val prefHelper by inject<PrefHelper>()

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        token?.let { prefHelper.put(PrefHelper.Key.TOKEN, it) }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        println("Notification received")
        remoteMessage?.let {
            if (it.data != null && it.data.isNotEmpty() && PushNotification(it.data).isValid()) {
                NotificationHelper.sendNotification(it.data)
            }
        }
    }

}