package its.ru.minimum.activities.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import its.ru.minimum.BuildConfig
import its.ru.minimum.MinimumApp
import its.ru.minimum.R
import its.ru.minimum.general.makeRound
import its.ru.minimum.general.navigation.BackButtonListener
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.mvp.activity_interfaces.SnackBarView
import its.ru.minimum.mvp.activity_interfaces.ToolbarView
import its.ru.minimum.mvvm.BaseActivity
import its.ru.minimum.notifications.NotificationHelper
import its.ru.minimum.pages.card.CardsFragment
import its.ru.minimum.pages.card.web.BrowserFragment
import its.ru.minimum.pages.cities.CitiesFragment
import its.ru.minimum.pages.confirm.ConfirmFragment
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.pages.favorites.list.FavoritesFragment
import its.ru.minimum.pages.geocode.GeocodeFragment
import its.ru.minimum.pages.history.HistoryFragment
import its.ru.minimum.pages.order.OrderFragment
import its.ru.minimum.pages.order.order_details.OrderDetailsFragment
import its.ru.minimum.pages.order.order_list.OrdersListFragment
import its.ru.minimum.pages.places.PlacesFragment
import its.ru.minimum.pages.profile.ProfileFragment
import its.ru.minimum.pages.profile.my_info.MyInfoFragment
import its.ru.minimum.pages.search.SearchFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.drawer.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.osmdroid.config.Configuration
import ru.its.data.repo.PrefHelper
import ru.its.data.utils.ImageSaver
import ru.its.data.utils.MiniLog
import ru.its.domain.model.Order
import ru.its.domain.model.PointType
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IUserRepository
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import ru.terrakok.cicerone.commands.BackTo
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.PublishSubject
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity : BaseActivity(), MainView, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, SnackBarView, its.ru.minimum.mvp.activity_interfaces.LocationListener, ToolbarView {

    private var progress: ProgressDialog? = null
    private lateinit var navigator: Navigator
    private var snackBar: Snackbar? = null
    @InjectPresenter
    lateinit var presenter: MainPresenter
    private val manager by inject<InputMethodManager>()
    private val router by inject<Router>()
    private val navigatorHolder by inject<NavigatorHolder>()
    private val prefHelper by inject<PrefHelper>()
    private val userRepository by inject<IUserRepository>()

    @ProvidePresenter
    fun providePresenter(): MainPresenter = MainPresenter(get(), get())


    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval((1 * 1000).toLong())        // 10 seconds, in milliseconds
            .setFastestInterval((1 * 1000).toLong())

    companion object {
        private val TAG = "MainActivity"
        private val REQUEST_CALL = 33
        val POINT_TYPE = "point_type"
        val ORDER_POSITION = "order_position"
        val SEARCH_POINT = "search_point"
        val ORDER_SUM = "order_sum"
        val ORDER = "order"
        val WEB_URL = "web_url"
        val WEB_TITLE = "web_title"
        private val locationSubject = PublishSubject.create<Location?>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drawer)

        setSupportActionBar(toolbar)
        supportActionBar?.title = null

        val osmConf = Configuration.getInstance()
        val basePath = File(cacheDir.absolutePath, "osmdroid")
        osmConf.osmdroidBasePath = basePath
        val tileCache = File(osmConf.osmdroidBasePath.absolutePath, "tile")
        osmConf.osmdroidTileCache = tileCache
        osmConf.userAgentValue = BuildConfig.APPLICATION_ID


        if (savedInstanceState == null) router.newRootScreen(OrderFragment.TAG)

        userRepository.getUser()?.city?.name?.let {
            cityLabel.text = it
        }

        cityLabel.setOnClickListener { router.navigateTo(CitiesFragment.TAG) }

        historyLabel.setOnClickListener { openDrawerItem(HistoryFragment.TAG) }

        orderLabel.setOnClickListener { openDrawerItem(OrderFragment.TAG) }

        currentLabel.setOnClickListener { openDrawerItem(OrdersListFragment.TAG) }

        profileLabel.setOnClickListener { openDrawerItem(ProfileFragment.TAG) }

        favoriteLabel.setOnClickListener { openDrawerItem(FavoritesFragment.TAG) }

        navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.fragment_container) {

            override fun backTo(command: BackTo?) {
                super.backTo(command)
                hideKeyboard()
            }

            override fun createFragment(screenKey: String?, data: Any?): Fragment {

                hideKeyboard()

                return when (screenKey) {
                    CitiesFragment.TAG -> CitiesFragment.instance()
                    MyInfoFragment.TAG -> MyInfoFragment.instance()
                    OrderFragment.TAG -> OrderFragment.instance()
                    HistoryFragment.TAG -> HistoryFragment.instance()
                    OrderDetailsFragment.TAG -> OrderDetailsFragment.instance(data as Order)
                    OrdersListFragment.TAG -> OrdersListFragment.instance()
                    ProfileFragment.TAG -> ProfileFragment.instance()

                    SearchFragment.TAG -> SearchFragment.instance(data as Int)
                    SearchFragment.TAG_FAVORITE -> SearchFragment.instance()

                    GeocodeFragment.TAG -> GeocodeFragment.instance(data as Int)
                    GeocodeFragment.TAG_FAVORITE -> GeocodeFragment.instance()

                    BrowserFragment.TAG -> BrowserFragment.instance()
                    CardsFragment.TAG -> CardsFragment.instance()

                    FavoritesFragment.TAG -> FavoritesFragment.instance()
                    FavoritesFragment.TAG_ORDER -> FavoritesFragment.instance(data as Int)

                    ConfirmFragment.TAG -> {
                        val sum = data as String
                        ConfirmFragment.instance(sum)
                    }

                    PlacesFragment.TAG -> {
                        val pair = data as Pair<Int, PointType>
                        PlacesFragment.instance(pair.first, pair.second)
                    }

                    PlacesFragment.TAG_FAVORITES -> {
                        PlacesFragment.instance(data as PointType)
                    }

                    DetailsFragment.TAG -> {
                        val pair = data as Pair<Int, SearchPoint>
                        DetailsFragment.instance(pair.first, pair.second)
                    }

                    DetailsFragment.TAG_FAVORITE -> {
                        DetailsFragment.instance(data as SearchPoint)
                    }

                    else -> throw RuntimeException("Unknown screen key!")
                }
            }

            override fun exit() {
                finish()
            }

            override fun showSystemMessage(message: String?) {
                message?.let { toast(it) }
            }

        }

        reloadAvatar()
        reloadName()

        presenter.start()

    }

    override fun reloadName() {
        userRepository.getUser()?.let { user_name.text = it.name }
    }

    override fun reloadAvatar() {

        ImageSaver(this)
                .setFileName("avatar.jpeg")
                .setDirectoryName("minimum")
                .createFile()?.let {

                    Picasso.get().invalidate(it)

                    Picasso.get()
                            .load(it)
                            .placeholder(R.mipmap.icon_user)
                            .into(avatar, object : Callback {
                                override fun onError(e: java.lang.Exception?) {

                                }

                                override fun onSuccess() {
                                    avatar?.makeRound()
                                }
                            })
                }
    }

    override fun updateCityTitle(title: String) {
        cityLabel.text = title
    }

    override fun showSnackBar(message: String, length: Int): Snackbar? {
        snackBar = Snackbar.make(coordinator, message, length)
        return snackBar
    }

    override fun startProgress(message: String) {
        progress = ProgressDialog(this)
        progress?.setMessage(message)
        progress?.show()
    }

    override fun stopProgress() {
        progress?.dismiss()
        progress = null
    }

    override fun showToast(message: String) {

        toast(message)

    }

    override fun makeCall(number: String?) {

        if (number == null) {
            longToast("Невозможно совершить звонок")
            return
        }

        checkPermission {
            call(number)
        }

    }

    private fun checkPermission(func: ()->Unit) {

        val permissions = arrayOf<String>(Manifest.permission.CALL_PHONE)
        val rationale = getString(R.string.call_permission_rationale)
        val options = Permissions.Options()
                .setRationaleDialogTitle(getString(R.string.info))
                .setSettingsDialogTitle(getString(R.string.warning))

        Permissions.check(this, permissions, rationale, options, object: PermissionHandler() {
            override fun onGranted() {
                func.invoke()
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                showToast(getString(R.string.no_permission))
            }
        })

    }

    @SuppressLint("MissingPermission")
    private fun call(number: String) {

        val initCall = Intent(Intent.ACTION_CALL)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initCall.`package` = "com.android.server.telecom"
        } else {
            initCall.`package` = "com.android.phone"
        }
        initCall.data = Uri.parse("tel:$number")
        startActivity(initCall)

    }

    override fun startListenLocation(interval: Int): Observable<Location?> {

        return locationSubject
                .doOnSubscribe {
                    if (mGoogleApiClient == null) {
                        MiniLog.i(TAG, "start listen location")

                        mLocationRequest = LocationRequest.create()
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setInterval((interval * 1000).toLong())        // 10 seconds, in milliseconds
                                .setFastestInterval((1 * 1000).toLong())

                        mGoogleApiClient = GoogleApiClient.Builder(this)
                                .addConnectionCallbacks(this)
                                .addOnConnectionFailedListener(this)
                                .addApi(LocationServices.API)
                                .build()
                        mGoogleApiClient?.connect()

                    }
                }
                .doOnUnsubscribe {
                    MiniLog.i(TAG, "stop listen location")
                    mGoogleApiClient?.let {
                        try {
                            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
                            mGoogleApiClient?.disconnect()
                            mGoogleApiClient = null
                        } catch (e: Exception) {
                            MiniLog.e(TAG, "error on stop listen location")
                            e.printStackTrace()
                        }
                    }
                }

    }

    override fun onConnected(p0: Bundle?) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
    }

    override fun onConnectionSuspended(p0: Int) = Unit

    override fun onConnectionFailed(p0: ConnectionResult) = Unit

    override fun onLocationChanged(p0: Location?) {
        locationSubject.onNext(p0)
    }

    /* override fun replaceFragment(fragment: Fragment, tag: String, save: Boolean) {

        if (supportFragmentManager.findFragmentByTag(tag) == null) {

            var transaction = supportFragmentManager.beginTransaction()

            transaction.replace(R.id.fragment_container, fragment, tag)

            if (save) transaction.addToBackStack(tag)

            transaction.commit()

        }

        hideKeyboard()

        if (drawer.isDrawerOpen(Gravity.START)) drawer.closeDrawer(Gravity.START)

    }*/

    override fun switchToolbar(show: Boolean) {
        if (show) supportActionBar?.show()
        else supportActionBar?.hide()
    }

    override fun tuneToolbar(title_resource: Int, isDrawer: Boolean): Toolbar = tuneToolbar(getString(title_resource), isDrawer)

    override fun tuneToolbar(title: String, isDrawer: Boolean): Toolbar {

        switchToolbar(true)

        leftButton.setImageResource(if (isDrawer) R.mipmap.ic_appbar_menu else R.mipmap.ic_appbar_back)
        rightButton.visibility = View.INVISIBLE
        toolbar_title.text = title
        toolbar_title.setOnClickListener { }

        drawer.setDrawerLockMode(if (isDrawer) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        leftButton.setOnClickListener {
            if (isDrawer) {
                drawer.openDrawer(Gravity.START)
                hideKeyboard()
            } else {
                onBackPressed()
            }
        }

        return toolbar
    }

    override fun onBackPressed() {
        hideKeyboard()
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
            return
        } else {
            super.onBackPressed()
        }

    }

    override fun openDrawerItem(tag: String) {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as? MenuFragment ?: return
        if (fragment.getBaseTag() != tag) {
            router.replaceScreen(tag)
            Observable.interval(100, TimeUnit.MILLISECONDS).take(1).observeOn(AndroidSchedulers.mainThread()).subscribe { closeDrawer() }
        } else closeDrawer()
    }

    override fun closeDrawer() {
        if (drawer.isDrawerOpen(Gravity.START)) drawer.closeDrawer(Gravity.START)
        hideKeyboard()
    }

    override fun openDrawer() {
        drawer.openDrawer(Gravity.START)
        hideKeyboard()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onStart() {
        super.onStart()
        MinimumApp.isForeground = true
        removeNotifications()
    }

    override fun onStop() {
        super.onStop()
        MinimumApp.isForeground = false
    }

    private fun removeNotifications() {
        try {
            NotificationHelper.notificationManager.cancelAll()
        } catch (e: Exception) {
            /* do nothing */
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        snackBar?.dismiss()
    }

}
