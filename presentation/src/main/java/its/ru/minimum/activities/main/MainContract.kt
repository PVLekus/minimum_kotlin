package its.ru.minimum.activities.main

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.CallView
import its.ru.minimum.mvp.activity_interfaces.MenuView
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import ru.its.data.repo.PrefHelper
import ru.its.data.utils.addToComposite
import ru.its.domain.model.City
import ru.its.domain.repo.IOrdersRepository
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.PublishSubject

/**
 * Created by oleg on 30.11.17.
 */
interface MainView : MinimumMvpView, MenuView, CallView {
    fun updateCityTitle(title: String)
}

@InjectViewState
class MainPresenter(private val ordersRepository: IOrdersRepository,
                    private val prefHelper: PrefHelper) : BasePresenter<MainView>() {

    companion object {
        val cityChangeSubject = PublishSubject.create<City>()
    }

    fun start() {
        cityChangeSubject
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { it.name?.let { viewState.updateCityTitle(it) } }.addToComposite(compositeSubscription)

        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        try {
                            task.exception!!.printStackTrace()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    val token = task.result?.token ?: return@OnCompleteListener
                    println("token = $token")
                    prefHelper.put(PrefHelper.Key.TOKEN, token)
                })

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }
}