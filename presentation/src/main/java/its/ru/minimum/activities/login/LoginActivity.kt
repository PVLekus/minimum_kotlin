package its.ru.minimum.activities.login

import android.app.ProgressDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import its.ru.minimum.R
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.mvp.activity_interfaces.SnackBarView
import its.ru.minimum.mvvm.BaseActivity
import its.ru.minimum.pages.login.CheckCodeFragment
import its.ru.minimum.pages.login.GetCodeFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportFragmentNavigator

/**
 * Created by oleg on 08.05.17.
 */
class LoginActivity : BaseActivity(), SnackBarView, MinimumMvpView {

    var progress: ProgressDialog? = null
    private val router by inject<Router>()
    private val navigatorHolder by inject<NavigatorHolder>()
    private lateinit var navigator: Navigator

    companion object {
        val PHONE = "phone"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.hide()

        navigator = object : SupportFragmentNavigator(supportFragmentManager, R.id.fragment_container){

            override fun createFragment(screenKey: String?, data: Any?): Fragment {

                return when (screenKey) {

                    GetCodeFragment.TAG -> GetCodeFragment.instance()
                    CheckCodeFragment.TAG -> CheckCodeFragment.instance(data as String)
                    else -> throw RuntimeException("Unknown screen key!")

                }

            }

            override fun exit() {
                finish()
            }

            override fun showSystemMessage(message: String?) {
                message?.let { toast(it) }
            }
        }

        router.newRootScreen(GetCodeFragment.TAG)
    }

    override fun showSnackBar(message: String, length: Int): Snackbar? = null

    override fun showToast(message: String) { longToast(message) }

    override fun startProgress(message: String) {
        progress = ProgressDialog(this)
        progress?.setMessage(message)
        progress?.show()
    }

    override fun stopProgress() { progress?.dismiss() }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

}