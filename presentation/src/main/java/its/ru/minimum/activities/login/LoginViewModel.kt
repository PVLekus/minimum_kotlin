package its.ru.minimum.activities.login

import android.arch.lifecycle.MutableLiveData
import its.ru.minimum.general.Resource
import its.ru.minimum.mvvm.BaseViewModel
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.remote.RutaxiAPI
import ru.its.data.utils.addToComposite
import ru.its.domain.exceptions.ViewModelException
import ru.its.domain.repo.IUserRepository
import ru.its.domain.utils.PhoneNormalizer
import rx.Observable

class LoginViewModel(private val rutaxiAPI: RutaxiAPI,
                     private val userRepository: IUserRepository): BaseViewModel() {

    val onGetCode: MutableLiveData<Resource<String>> = MutableLiveData()
    val onCheckCode: MutableLiveData<Resource<String>> = MutableLiveData()

    fun getCode(phone: String?) {

        if (phone?.trim()?.isEmpty() ?: true)  {
            onGetCode.value = Resource.ERROR(ViewModelException("Введите корректно номер телефона"))
            return
        }

        val normal_phone = PhoneNormalizer().normalize(phone!!)

        println("normal_phone = $normal_phone")

        Observable.just(normal_phone)
                .doOnSubscribe { onGetCode.value = Resource.LOADING("Ожидайте..") }
                .flatMap {  rutaxiAPI.sendCode(it).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    onGetCode.value = Resource.SUCCESS(phone)
                },{
                    it.printStackTrace()
                    onGetCode.value = Resource.ERROR(it)
                }).addToComposite(composite)

    }

    fun checkCode(phone: String?, code: String?) {

        if (code?.trim()?.isEmpty() ?: true)  {
            onCheckCode.value = Resource.ERROR(ViewModelException("Введите код"))
            return
        }

        val normal_phone = PhoneNormalizer().normalize(phone!!)

        Observable.just(normal_phone)
                .doOnSubscribe { onCheckCode.value = Resource.LOADING("Ожидайте..") }
                .flatMap {  rutaxiAPI.checkCode(normal_phone, code).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    userRepository.updateToken(it)
                    userRepository.updatePhone(normal_phone)
                    onCheckCode.value = Resource.SUCCESS(normal_phone)
                },{
                    it.printStackTrace()
                    onCheckCode.value = Resource.ERROR(it)
                }).addToComposite(composite)

    }
}