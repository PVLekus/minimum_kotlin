package its.ru.minimum.activities.connection

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import its.ru.minimum.R
import its.ru.minimum.mvp.activity_interfaces.CallView
import kotlinx.android.synthetic.main.activity_connection.*
import org.jetbrains.anko.longToast
import org.koin.android.ext.android.inject
import ru.its.data.repo.PrefHelper
import ru.its.domain.repo.IUserRepository

/**
 * Created by oleg on 18.12.17.
 */
class NoConnectionActivity: AppCompatActivity(), CallView {

    private val prefHelper by inject<PrefHelper>()
    private val userRepository by inject<IUserRepository>()

    companion object {
        private val TAG = "NoConnectionActivity"
        private val REQUEST_CALL = 33
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connection)

        button_call_offline?.setOnClickListener {
            userRepository.getUser()?.city?.rate?.params?.operatorPhone?.let {
                makeCall(it)
            } ?: makeCall("")
        }

    }

    override fun makeCall(number: String?) {

        if (number == null) {
            longToast("Невозможно совершить звонок")
            return
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                prefHelper.put(PrefHelper.Key.TEMP_PHONE, number)
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), REQUEST_CALL)
            }
            return
        }
        call(number)

    }

    @SuppressLint("MissingPermission")
    private fun call(number: String) {

        val initCall = Intent(Intent.ACTION_CALL)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            initCall.`package` = "com.android.server.telecom"
        } else {
            initCall.`package` = "com.android.phone"
        }
        initCall.data = Uri.parse("tel:$number")
        startActivity(initCall)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CALL) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                prefHelper.get(PrefHelper.Key.TEMP_PHONE, String::class.java)?.let { call(it) }
            } else {
                longToast("Нет требуемого разрешения на звонок")
            }
            return
        }

    }

}