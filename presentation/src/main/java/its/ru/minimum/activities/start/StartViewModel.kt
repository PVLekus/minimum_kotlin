package its.ru.minimum.activities.start

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import its.ru.minimum.general.Resource
import its.ru.minimum.mvvm.BaseViewModel
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.remote.RutaxiAPI
import ru.its.data.usecase.InitPrefsConstUseCase
import ru.its.data.utils.AvailabilityUtils
import ru.its.data.utils.addToComposite
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.model.StatusText
import ru.its.domain.repo.IBookingRepository
import ru.its.domain.repo.ICitiesRepository
import ru.its.domain.repo.IUserRepository
import ru.its.domain.repo.SourceType

class StartViewModel(private val initPrefsConstUseCase: InitPrefsConstUseCase,
                     private val bookingRepository: IBookingRepository,
                     private val citiesRepository: ICitiesRepository,
                     private val userRepository: IUserRepository,
                     private val rutaxiAPI: RutaxiAPI,
                     private val application: Application): BaseViewModel() {

    val loadingCities: MutableLiveData<Resource<Boolean>> = MutableLiveData()
    val loadingRates: MutableLiveData<Resource<Boolean>> = MutableLiveData()
    val checkingAuthorization: MutableLiveData<Resource.SUCCESS<Boolean>> = MutableLiveData()
    val checkingConnection: MutableLiveData<Resource.SUCCESS<Boolean>> = MutableLiveData()

    fun start() {

        bookingRepository.clear()
        executeUseCase(initPrefsConstUseCase, true, {
            loadCities()
        },{
            it.printStackTrace()
            handleError(it)
        })

    }

    fun loadCities() {

        citiesRepository.getAll(SourceType.NETWORK)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    loadingCities.value = Resource.SUCCESS(true)
                },{
                    it.printStackTrace()
                    loadingCities.value = Resource.ERROR(it)
                }).addToComposite(composite)

    }

    fun selectCity() {

        var currentCity = userRepository.getUser()?.city?.name?.let { citiesRepository.getFirstCity(it) } ?: citiesRepository.getFirstCity()

        currentCity?.let { userRepository.updateCity(it) }

        if (currentCity == null) {
            handleError(ApiResponseException(StatusText.fromErrorCode(null)))
            return
        }

        loadRates()


    }

    fun loadRates() {

        userRepository.getUser()?.city?.let {

            if (it.rate != null) {
                loadingRates.value = Resource.SUCCESS(true)
            } else {
                rutaxiAPI.getRates()
                        .compose(SchedulerUtils.IoMainScheduler())
                        .subscribe({
                            userRepository.updateRate(it?.get(0))
                            loadingRates.value = Resource.SUCCESS(true)
                        },{
                            it.printStackTrace()
                            loadingRates.value = Resource.ERROR(it)
                        }).addToComposite(composite)
            }

        } ?: handleError(ApiResponseException(StatusText.fromErrorCode(null)))

    }

    fun checkConnection() {
        checkingConnection.value = Resource.SUCCESS(AvailabilityUtils.hasInternetAccess(application))
    }

    fun checkAuthorization() {
        checkingAuthorization.value = Resource.SUCCESS(userRepository.getUser()?.token != null)
    }
}