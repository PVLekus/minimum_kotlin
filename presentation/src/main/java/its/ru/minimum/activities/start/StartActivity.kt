package its.ru.minimum.activities.start

import android.arch.lifecycle.Observer
import android.os.Bundle
import its.ru.minimum.R
import its.ru.minimum.activities.connection.NoConnectionActivity
import its.ru.minimum.activities.login.LoginActivity
import its.ru.minimum.activities.main.MainActivity
import its.ru.minimum.general.Resource
import its.ru.minimum.mvvm.BaseActivity
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel
import ru.its.domain.exceptions.InternetConnectionException


/**
 * Created by oleg on 08.05.17.
 */
class StartActivity: BaseActivity() {

    private val viewModel by viewModel<StartViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)

        supportActionBar?.hide()


        viewModel.checkingAuthorization.observe(this, isAuthorizedObserver)
        viewModel.checkingConnection.observe(this, checkingConnectionObserver)

        viewModel.loadingCities.observe(this, Observer {
            when (it) {
                is Resource.SUCCESS -> it.getOnce { viewModel.selectCity() }
                is Resource.ERROR -> it.get {
                    if (it is InternetConnectionException) showRepeatDialog(R.string.network_error, R.string.repeat) { viewModel.loadCities() }
                    else handleError(it, false)
                }
            }
        })

        viewModel.loadingRates.observe(this, Observer {
            when (it) {
                is Resource.SUCCESS -> it.getOnce { viewModel.checkConnection() }
                is Resource.ERROR -> it.get {
                    if (it is InternetConnectionException) showRepeatDialog(R.string.network_error, R.string.repeat) { viewModel.loadRates() }
                    else handleError(it, false)
                }
            }
        })

        viewModel.start()
    }





    private val checkingConnectionObserver = Observer<Resource.SUCCESS<Boolean>> {
        it?.getOnce {
            if (!it) {
                startActivity<NoConnectionActivity>()
                finish()
            } else {
                viewModel.checkAuthorization()
            }
        }
    }

    private val isAuthorizedObserver = Observer<Resource.SUCCESS<Boolean>> {
        it?.getOnce {
            if (it) {
                startActivity<MainActivity>()
            } else {
                startActivity<LoginActivity>()
            }
            finish()
        }

    }

    override fun onPause() {
        super.onPause()
        dialog?.dismiss()
    }

}