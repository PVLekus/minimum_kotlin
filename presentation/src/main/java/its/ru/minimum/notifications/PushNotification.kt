package its.ru.minimum.notifications

import its.ru.minimum.R

class PushNotification(messageMap: Map<String, String>) {
    companion object {
        private const val MESSAGE_KEY = "message"
        private const val BRAZE_MESSAGE_KEY = "a"
        private const val TITLE_KEY = "title"
        private const val BRAZE_TITLE_KEY = "t"
        private const val TRUE = "true"
        var id = 0
    }

    var message: String? = null
    var title: String? = null
    val iconResource = R.mipmap.ic_mini_launcher

    init {
        message = messageMap[MESSAGE_KEY] ?: messageMap[BRAZE_MESSAGE_KEY]
        title = messageMap[TITLE_KEY] ?: messageMap[BRAZE_TITLE_KEY] ?: "Minimum"
    }

    fun isValid(): Boolean {
        return message?.let {
            !it.isBlank()
                    // костыль необходимый для фильтрации пушей с текстом true, природу которых не удалось установить
                    && !it.contains(TRUE) } ?: false
    }
}