package its.ru.minimum.notifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import its.ru.minimum.MinimumApp
import its.ru.minimum.R
import its.ru.minimum.activities.start.StartActivity
import java.util.*

object NotificationHelper {

    private const val NOTIFICATION_CHANNEL_ID = "minimum_push"
    val notificationManager = createNotificationManager()

    fun sendNotification(messageMap: Map<String, String>) {
        val pushNotification = PushNotification(messageMap)
        notificationManager.notify(Date().time.toInt(), pushNotification.createNotification())
    }

    private fun PushNotification.createNotification(): Notification {
        val builder = android.app.Notification.Builder(MinimumApp.instance)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(android.app.Notification.BigTextStyle().bigText(message))
                .setSmallIcon(iconResource)
                .setContentIntent(createPendingIntent())
                .setAutoCancel(true)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            builder.setChannelId(NOTIFICATION_CHANNEL_ID)
        }
        return builder.build()
    }

    private fun createPendingIntent(): PendingIntent {
        val intent = Intent(MinimumApp.instance, StartActivity::class.java)
        return PendingIntent.getActivity(MinimumApp.instance, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): NotificationChannel {
        val name = MinimumApp.instance.resources.getString(R.string.app_name)
        return NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH)
    }

    private fun createNotificationManager(): NotificationManager {
        val notificationManager = MinimumApp.instance.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(createNotificationChannel())
        }
        return notificationManager
    }

}