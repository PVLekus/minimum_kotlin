package its.ru.minimum.mvp.activity_interfaces

import android.location.Location
import rx.Observable

/**
 * Created by oleg on 21.12.17.
 */
interface LocationListener {
    fun startListenLocation(interval: Int) : Observable<Location?>?
}