package its.ru.minimum.mvp.activity_interfaces

import android.support.v7.widget.Toolbar

/**
 * Created by oleg on 27.11.17.
 */
interface ToolbarView {
    fun tuneToolbar(title_resource: Int, isDrawer: Boolean): Toolbar?
    fun tuneToolbar(title: String, isDrawer: Boolean): Toolbar?
    fun switchToolbar(show: Boolean)
}