package its.ru.minimum.mvp.activity_interfaces

/**
 * Created by oleg on 21.12.17.
 */
interface MenuView {
    fun openDrawerItem(tag: String)
    fun closeDrawer()
    fun openDrawer()
    fun reloadAvatar()
    fun reloadName()

}