package its.ru.minimum.mvp

import android.content.DialogInterface
import android.util.Log
import com.arellomobile.mvp.MvpPresenter
import com.crashlytics.android.Crashlytics
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import org.osmdroid.util.GeoPoint
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.exceptions.InternetConnectionException
import ru.its.domain.model.StatusText
import ru.its.domain.usecase.UseCase
import rx.subscriptions.CompositeSubscription

/**
 * Created by oleg on 08.05.17.
 */
 abstract class BasePresenter<T: MinimumMvpView> : MvpPresenter<T>() {

    var compositeSubscription: CompositeSubscription = CompositeSubscription()

    fun handleError(throwable: Throwable?, silent: Boolean, action: DialogInterface.() -> Unit, show: Boolean = true) {

        throwable?.printStackTrace()

        var message = StatusText.fromErrorCode(null)

        when(throwable) {
            is InternetConnectionException -> {
                message = StatusText.internetError()
                if (silent && show) viewState?.showToast(message)
                else if (!silent && show) onInternetConnectionException(action)
            }
            is ApiResponseException -> {
                message = throwable.message ?: StatusText.fromErrorCode(null)
                if (silent && show) viewState?.showToast(message)
                else if (!silent && show) viewState?.showDialog(message)
            }
            else -> {
                Crashlytics.log(Log.ERROR, "PRESENTER", "info \n${throwable?.message}")
                Crashlytics.logException(throwable)
                if (silent && show) viewState?.showToast(message)
                else if (silent && show) viewState?.showDialog(message)
            }
        }



    }

    fun <Result, Params> executeUseCase(useCase: UseCase<Result, Params>, params: Params, onNext: (t: Result) -> Unit, onError: (e: Throwable) -> Unit) {
        useCase.createObservable(params).compose(SchedulerUtils.IoMainScheduler()).subscribe(onNext, onError)
    }

    abstract fun onInternetConnectionException(action: DialogInterface.() -> Unit)

    override fun onDestroy() {
        super.onDestroy()
        compositeSubscription.clear()
        compositeSubscription = CompositeSubscription()
    }

}

interface IMap {

    fun zoomTo(zoomLevel: Int, geoPoint: GeoPoint)
    fun replaceMarker(point: GeoPoint?)
    fun setUpUserLocation(point: GeoPoint?)
    fun dropSearchMyLocation()

}