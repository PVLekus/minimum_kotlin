package its.ru.minimum.mvp.activity_interfaces

import android.support.design.widget.Snackbar

/**
 * Created by oleg on 05.03.18.
 */
interface SnackBarView {
    fun showSnackBar(message: String, length: Int) : Snackbar?

}