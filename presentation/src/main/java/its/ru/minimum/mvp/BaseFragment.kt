package its.ru.minimum.mvp

import android.location.Location
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.DialogFragment
import android.support.v7.widget.Toolbar
import android.view.View
import com.arellomobile.mvp.MvpAppCompatFragment
import its.ru.minimum.general.bindViews
import its.ru.minimum.general.unbindViews
import its.ru.minimum.mvp.activity_interfaces.*
import its.ru.minimum.mvvm.BaseActivity
import its.ru.minimum.pages.spinner.SpinnerDialog
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import rx.Observable

/**
 * Created by oleg on 21.12.17.
 */
abstract class BaseFragment: MvpAppCompatFragment(), MinimumMvpView, ToolbarView, LocationListener, CallView, SnackBarView, MenuView {

    protected val router by inject<Router>()
    private val DIALOG_TAG = "loading_dialog"

    override fun onDestroyView() {
        super.onDestroyView()
        unbindViews()
    }

    fun handleError(throwable: Throwable?, silent: Boolean, show: Boolean = true) {
        (activity as? BaseActivity)?.handleError(throwable, silent, show)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews(view)



    }

    override fun showToast(message: String) {
        (activity as? MinimumMvpView)?.showToast(message)
    }

    override fun showDialog(message: String) {
        (activity as? MinimumMvpView)?.showToast(message)
    }

    override fun startProgress(message: String) {
        stopProgress()
        SpinnerDialog().show(childFragmentManager, DIALOG_TAG)
        childFragmentManager.executePendingTransactions()
    }

    override fun stopProgress() {
        val currentDialog = childFragmentManager.findFragmentByTag(DIALOG_TAG)
        (currentDialog as? DialogFragment)?.dismiss()
        childFragmentManager.executePendingTransactions()
    }

    override fun tuneToolbar(title_resource: Int, isDrawer: Boolean): Toolbar? {
        return (activity as? ToolbarView)?.tuneToolbar(title_resource, isDrawer)
    }

    override fun tuneToolbar(title: String, isDrawer: Boolean): Toolbar? {
        return (activity as? ToolbarView)?.tuneToolbar(title, isDrawer)
    }

    override fun switchToolbar(show: Boolean) {
        (activity as? ToolbarView)?.switchToolbar(show)
    }

    override fun showSnackBar(message: String, length: Int): Snackbar? {
        return (activity as? SnackBarView)?.showSnackBar(message, length)
    }

    override fun hideKeyboard() {
        (activity as? MinimumMvpView)?.hideKeyboard()
    }

    override fun startListenLocation(interval: Int): Observable<Location?>? {
        return (activity as? LocationListener)?.startListenLocation(interval)
    }

    override fun makeCall(number: String?) {
        (activity as? CallView)?.makeCall(number)
    }

    override fun closeDrawer() {
        (activity as? MenuView)?.closeDrawer()
    }

    override fun openDrawer() {
        (activity as? MenuView)?.openDrawer()
    }

    override fun reloadAvatar() {
        (activity as? MenuView)?.reloadAvatar()
    }

    override fun reloadName() {
        (activity as? MenuView)?.reloadName()
    }

    override fun openDrawerItem(tag: String) {
        (activity as? MenuView)?.openDrawerItem(tag)
    }

}