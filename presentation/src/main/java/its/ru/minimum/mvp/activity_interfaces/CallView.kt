package its.ru.minimum.mvp.activity_interfaces

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by oleg on 21.12.17.
 */
interface CallView {
    @StateStrategyType(SkipStrategy::class)
    fun makeCall(number: String?)

}