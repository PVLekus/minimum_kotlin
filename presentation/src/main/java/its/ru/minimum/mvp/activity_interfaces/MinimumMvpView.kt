package its.ru.minimum.mvp.activity_interfaces

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by oleg on 27.11.17.
 */
interface MinimumMvpView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showToast(message: String)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showDialog(message: String)
    @StateStrategyType(SkipStrategy::class)
    fun startProgress(message: String)
    fun stopProgress()
    fun hideKeyboard()

}