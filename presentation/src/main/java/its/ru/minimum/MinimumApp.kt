package its.ru.minimum

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import io.fabric.sdk.android.Fabric
import its.ru.minimum.di.module.*
import net.danlew.android.joda.JodaTimeAndroid
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


/**
 * Created by oleg on 13.09.17.
 */
class MinimumApp : Application() {

    lateinit var firebaseRemoteConfig: FirebaseRemoteConfig

    companion object {
        lateinit var instance: MinimumApp
        var isForeground = false
    }

    override fun onCreate() {
        super.onCreate()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        JodaTimeAndroid.init(this)

        instance = this
        Fabric.with(this, Crashlytics())

        AppsFlyerLib.getInstance().init("iYgharVuegtDC5pC8B3odN", object : AppsFlyerConversionListener {
            override fun onAppOpenAttribution(p0: MutableMap<String, String>?) {
            }

            override fun onAttributionFailure(p0: String?) {
            }

            override fun onInstallConversionDataLoaded(p0: MutableMap<String, String>?) {
            }

            override fun onInstallConversionFailure(p0: String?) {
            }

        }, applicationContext)

        startKoin {
            androidLogger()
            androidContext(this@MinimumApp)
            modules(listOf(appModule, navigationModule, repoModule, networkModule, useCaseModule))
        }

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance()
        firebaseRemoteConfig.setConfigSettings(FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build())
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults)

        fetchConfig()

        FirebaseInstanceId.getInstance().instanceId
                .addOnSuccessListener {
                    println("onsuccess = ${it.token}")
                }
                .addOnCompleteListener {
                    if (!it.isSuccessful) return@addOnCompleteListener
                    val token = it.result?.token
                    println("onComplete = $token")
                }
                .addOnFailureListener {
                    it.printStackTrace()
                }

    }

    private fun fetchConfig() {

        var cacheExpiration: Long = 3600 // 1 hour in seconds.
        // If your app is using developer mode, cacheExpiration is set to 0, so each fetch will
        // retrieve values from the service.
        if (firebaseRemoteConfig.info.configSettings.isDeveloperModeEnabled) {
            cacheExpiration = 0
        }

        firebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener { task -> if (task.isSuccessful) firebaseRemoteConfig.activateFetched() }

    }

}