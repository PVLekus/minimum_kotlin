package its.ru.minimum.general

import android.text.Editable
import android.text.TextWatcher
import android.widget.ImageView

/**
 * Created by oleg on 27.11.17.
 */
class DetailsTextWatcher(private val imageView: ImageView?, private val emptyIcon: Int, private val fillIcon: Int) : TextWatcher {
    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        imageView?.setImageResource(if (s?.trim()?.count() ?: 0 == 0 ) emptyIcon else fillIcon )
    }
}