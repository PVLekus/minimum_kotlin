package its.ru.minimum.general

import android.app.Activity
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import its.ru.minimum.MinimumApp

/**
 * Created by oleg on 02.11.17.
 */


infix fun ViewGroup?.inflate(layoutResId: Int): View? =
        LayoutInflater.from(this?.context).inflate(layoutResId, this, false)

infix fun RecyclerView.insertAdapter(adapter: RecyclerView.Adapter<*>) {
    this.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
    this.adapter = adapter
}

fun <T> Collection<T>?.isEmptyOrNull() : Boolean {
    if (this == null) return true
    if (this.isEmpty()) return true
    return false
}

fun ImageView.makeRound() {
    val imageBitmap = (this.drawable as BitmapDrawable).bitmap
    val imageDrawable = RoundedBitmapDrawableFactory.create(resources, imageBitmap)
    imageDrawable.isCircular = true
    imageDrawable.cornerRadius = Math.max(imageBitmap.width, imageBitmap.height) / 2.0f
    this.setImageDrawable(imageDrawable)
}

val Activity.app: MinimumApp
    get() = application as MinimumApp

fun Bundle?.getIntOrNull(key: String): Int? {
    return if (this?.containsKey(key) == true) this.getInt(key)
    else null
}