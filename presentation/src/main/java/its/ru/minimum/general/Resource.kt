package its.ru.minimum.general

sealed class Resource <T> {
    var hasBeenHandled = false
    class SUCCESS <T> (private val data: T?): Resource<T>() {
        fun get(func: (T)->Unit) {
            data?.let {
                func.invoke(it)
            }
        }

        fun getOnce(func: (T)->Unit) {
            data?.let {
                if (!hasBeenHandled) {
                    hasBeenHandled = true
                    func.invoke(it)
                }
            }
        }

    }
    class LOADING<T>(val message: String = ""): Resource<T>()

    class ERROR <T> (private val throwable: Throwable): Resource<T>() {
        fun get(func: (Throwable)->Unit) {
            if (!hasBeenHandled) {
                hasBeenHandled = true
                func.invoke(throwable)
            }
        }
    }
}

fun <T> T.asSuccess() = Resource.SUCCESS(this)
fun <T> Throwable.asError() = Resource.ERROR<T>(this)
fun <T> asLoading()  = Resource.LOADING<T>()