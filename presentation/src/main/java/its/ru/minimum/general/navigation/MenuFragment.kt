package its.ru.minimum.general.navigation

import its.ru.minimum.mvp.BaseFragment

/**
 * Created by oleg on 03.11.17.
 */
abstract class MenuFragment : BaseFragment() {
    abstract fun getBaseTag() : String
}