package its.ru.minimum.general.navigation

/**
 * Created by oleg on 16.11.17.
 */
interface BackButtonListener {
    fun onBackPressed() : Boolean
}