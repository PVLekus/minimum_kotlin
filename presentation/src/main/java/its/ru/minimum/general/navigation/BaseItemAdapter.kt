package its.ru.minimum.general.navigation

import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.commons.utils.FastAdapterDiffUtil

interface IItemsAdapter<T> {
    fun update(list: List<T>)
    fun addItems(list: List<T>)
    fun addItem(item: T)
    fun removeItem(item: T)
    fun updateItem(item: T)
}

open class BaseItemAdapter<I : IItem<I, *>> : ItemAdapter<I>(), IItemsAdapter<I> {

    var listener: OnItemsChangeListener? = null

    override fun update(list: List<I>) {

        val results = FastAdapterDiffUtil.calculateDiff(this, list)
        FastAdapterDiffUtil.set(this, results)
        listener?.onChanged(adapterItemCount == 0)
    }

    override fun addItems(list: List<I>) {
        add(list)
    }

    override fun addItem(item: I) {
        itemList.items.indexOfFirst { it == item }.let {
            if (it < 0) add(0, item)
        }
        listener?.onChanged(adapterItemCount == 0)
    }

    override fun removeItem(item: I) {
        itemList.items.indexOfFirst { it == item }.let {
            if (it >= 0) remove(it)
        }
        listener?.onChanged(adapterItemCount == 0)
    }

    override fun updateItem(item: I) {
        itemList.items.indexOfFirst { it == item }.let {
            if (it >= 0) fastAdapter.notifyItemChanged(it)
        }
    }

    interface OnItemsChangeListener {
        fun onChanged(isEmpty: Boolean)
    }

}

class FastAdapterExtension {
    companion object {
        fun <Item : IItem<*, *>> createDefaultItemAdapter() = ItemAdapter<Item>()

        @JvmOverloads
        fun <Item : IItem<*, *>> createDefaultAdapter(itemAdapter: IAdapter<out Item> = createDefaultItemAdapter()) =
                FastAdapter.with<Item, IAdapter<out Item>>(itemAdapter)!!

        fun <Item : IItem<*, *>> createAdapter(vararg adapter: IAdapter<out Item>) =
                FastAdapter.with<Item, IAdapter<out Item>>(adapter.toList())
    }
}