package its.ru.minimum.general

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import its.ru.minimum.R

/**
 * Created by oleg on 07.11.17.
 */
abstract class SimpleAdapter<T>(val layout: Int) : BaseAdapter<T>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH<T> {
        return getHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
    }


    abstract fun getHolder(view: View) : BaseVH<T>
}

abstract class BaseVH <T> (view: View) : RecyclerView.ViewHolder(view) {

    abstract fun fill(item: T)

}

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseVH<T>>() {

    var list = mutableListOf<T>()

    override fun getItemCount(): Int = list.count()

    open fun update(list: List<T>) {

        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: BaseVH<T>, position: Int) {
        holder?.fill(list[position])
    }

}

abstract class SimpleEndlessAdapter<T>(private val layout: Int, private val itemsPerPage: Int, private val adapterListener: AdapterListener<T>) : BaseAdapter<T>() {
    protected var mKeepAppending = true
    protected var isLoading = false
    protected val ITEM_TYPE_OTHER = 200
    protected val ITEM_TYPE_PENDING = 100
    private var mNextPage = 1

    override fun getItemViewType(position: Int): Int {
        return if (mKeepAppending && position == itemCount - 1) {
            ITEM_TYPE_PENDING
        } else ITEM_TYPE_OTHER
    }

    override fun update(list: List<T>) {
        mKeepAppending = list.size >= itemsPerPage
        super.update(list)
        mNextPage = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH<T> {
        return if (viewType == ITEM_TYPE_PENDING) {
            getPendingHolder(parent)
        } else getHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
    }

    override fun onBindViewHolder(holder: BaseVH<T>, position: Int) {
        if (mKeepAppending && !isLoading && itemCount - position <= itemsPerPage / 2) {
            onLoadNewItems()
        }
        if (getItemViewType(position) == ITEM_TYPE_PENDING) {
            //            onLoadNewItems();
            return
        }
        super.onBindViewHolder(holder, position)
    }

    protected abstract fun getHolder(view: View): BaseVH<T>

    protected fun onLoadNewItems() {
        isLoading = true
        adapterListener.onLoadNewItems(++mNextPage, itemsPerPage)
    }

    protected fun getPendingHolder(parent: ViewGroup): BaseVH<T> {

        val pendingView = LayoutInflater.from(parent.context).inflate(R.layout.row_pending_view, parent, false)

        return object : BaseVH<T>(pendingView) {
            override fun fill(item: T) {
            }
        }
    }

    fun addNewItems(items: List<T>?) {
        isLoading = false
        setKeepAppending(items != null && items.size >= itemsPerPage)
        if (items == null) {
            return
        }
        val insertPosition = itemCount
        list.addAll(items)
        notifyItemRangeInserted(insertPosition, items.size)

    }

    fun onGetNewItemsFailure() {
        setKeepAppending(false)
    }

    fun setKeepAppending(keepAppending: Boolean) {
        if (mKeepAppending != keepAppending) {
            mKeepAppending = keepAppending
            val pendingViewPosition = itemCount
            if (keepAppending) {
                notifyItemInserted(pendingViewPosition)
            } else {
                notifyItemRemoved(pendingViewPosition)
            }
        }
    }

    interface AdapterListener<T> {
        fun onItemClicked(item: T)
        fun onLoadNewItems(page: Int, count: Int)
    }

    fun removeItem(item: T) {
        val position = list.indexOf(item)
        if (position >= 0) {
            list.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun addItemToStart(item: T) {
        list.add(0, item)
        notifyItemInserted(0)
    }

}
