package its.ru.minimum.pages.details

import android.content.DialogInterface
import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import com.google.gson.Gson
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.activities.main.MainActivity.Companion.SEARCH_POINT
import its.ru.minimum.general.getIntOrNull
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.pages.favorites.list.FavoritesFragment
import its.ru.minimum.pages.order.OrderFragment
import its.ru.minimum.pages.search.SearchFragment
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.utils.addToComposite
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IBookingRepository
import ru.its.domain.repo.IFavoritesRepository
import ru.its.domain.repo.IUserRepository
import ru.terrakok.cicerone.Router

/**
 * Created by oleg on 16.11.17.
 */
interface DetailsView: MinimumMvpView {

    fun updateComment(comment: String?)
    fun showPreOrder(boolean: Boolean)
    fun updateView(orderPosition: Int?, searchPoint: SearchPoint?)

}

@InjectViewState
class DetailsPresenter (private val bookingRepository: IBookingRepository,
                        private val userRepository: IUserRepository,
                        private val favoritesRepository: IFavoritesRepository,
                        private val gson: Gson,
                        private val router: Router): BasePresenter<DetailsView>() {

    var searchPoint: SearchPoint? = null
    var orderPosition: Int? = null

    fun readArguments(args: Bundle?) {

        orderPosition = args.getIntOrNull(ORDER_POSITION)

        args?.let {
            searchPoint = it.getString(SEARCH_POINT, null)?.let { gson.fromJson(it, SearchPoint::class.java) }
        }

        viewState.updateView(orderPosition, searchPoint)

    }

    fun openSearch() {
        orderPosition?.let {
            router.navigateTo(SearchFragment.TAG, orderPosition)
        } ?: router.navigateTo(SearchFragment.TAG_FAVORITE)
    }

    fun confirm(favName: String?, house: String?, enter: String?, comment: String?) {

        searchPoint?.let {
            if (it.metatype == 0 && house?.trim().isNullOrEmpty()) {
                viewState.showDialog("Заполните номер дома")
                return
            }

            it.enter = enter
            it.house = house

            if (orderPosition == null) {
                val name =  if (favName?.trim().isNullOrBlank()) null else favName
                viewState.startProgress("Добавление..")
                favoritesRepository.addToFavorite(it, name)
                        .compose(SchedulerUtils.IoMainScheduler())
                        .subscribe({
                            viewState.stopProgress()
                            router.backTo(FavoritesFragment.TAG_ORDER)
                        },{
                            viewState.stopProgress()
                            handleError(it, true, {})
                        }).addToComposite(compositeSubscription)
            } else {
                bookingRepository.updateItemByPosition(it,orderPosition!!, comment)
                        .subscribe({
                            router.backTo(OrderFragment.TAG)
                        },{
                            handleError(it, true, {})
                        }).addToComposite(compositeSubscription)
            }

        } ?: handleError(NullPointerException("empty point"), true, {})

    }

    fun checkPreOrder() {
        if (orderPosition != null) viewState.showPreOrder(userRepository.getUser()?.city?.rate?.params?.preOrder == "1")
        else viewState.showPreOrder(false)
    }

    fun onBackPressed() {
        orderPosition?.let {
            router.backTo(SearchFragment.TAG)
        } ?: router.backTo(SearchFragment.TAG_FAVORITE)
    }

    fun init() {
        bookingRepository.get()
                .subscribe({
                    viewState.updateComment(it.comment)
                    searchPoint?.preOrder = it.preOrderTime
                },{
                    handleError(it, true, {})
                })
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
    }
}