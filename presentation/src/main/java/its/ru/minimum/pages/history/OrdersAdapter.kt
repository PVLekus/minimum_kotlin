package its.ru.minimum.pages.history

import android.view.LayoutInflater
import android.view.View
import its.ru.minimum.R
import its.ru.minimum.general.BaseVH
import its.ru.minimum.general.SimpleEndlessAdapter
import kotlinx.android.synthetic.main.item_address.view.*
import kotlinx.android.synthetic.main.item_history.view.*
import org.joda.time.format.DateTimeFormat
import ru.its.data.utils.AddressBuilder
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.model.StatusText

/**
 * Created by oleg on 07.11.17.
 */
class OrdersAdapter(val adapterView: AdapterListener, val showCarInfo: Boolean = true): SimpleEndlessAdapter<Order>(R.layout.item_history, 20, adapterView) {

    override fun getHolder(view: View): BaseVH<Order> {
        return object : BaseVH<Order>(view) {

            override fun fill(item: Order) {
                val inflater = LayoutInflater.from(view?.context)

                view?.histContainer?.removeAllViews()

                view?.options?.setOnClickListener { adapterView.onOptionClick(item, it) }
                view?.retry?.setOnClickListener { adapterView.onRepeatClick(item) }
                view?.histStatusText?.text = StatusText.fromStatusCode(if (item?.canceled == 1) 111 else item?.status)
                val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
                view?.histDateText?.text = formatter.parseDateTime(item.createdAt).toString("dd MMMM в HH:mm")

                item?.points?.forEachIndexed { index, pointsItem ->

                    val address = inflater?.inflate(R.layout.item_address, null)

                    address?.addressText?.text = AddressBuilder().build(pointsItem)

                    pointsItem.favorite = adapterView.isPointFavorite(pointsItem)

                    address?.ic_favorite?.setImageLevel(if (pointsItem.favorite) 1 else 0)

                    address?.ic_favorite?.setOnClickListener {
                        if (!pointsItem.favorite) adapterView.onFavClick(pointsItem)
                    }

                    address?.addressShape?.setBackgroundResource(if (index == 0) R.drawable.circle_green else R.drawable.circle_red)

                    view?.histContainer?.addView(address)
                }

            }

        }
    }

    fun removeById(id: String) {

        val index = list.indexOfFirst { it.id == id }
        if (index >= 0) {
            list.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    interface AdapterListener: SimpleEndlessAdapter.AdapterListener<Order> {
        fun isPointFavorite(searchPoint: SearchPoint): Boolean
        fun onRepeatClick(item: Order)
        fun onOptionClick(item: Order, view: View?)
        fun onFavClick(searchPoint: SearchPoint)
    }

}