package its.ru.minimum.pages.order.order_list

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.BuildConfig
import its.ru.minimum.MinimumApp
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import org.jetbrains.anko.toast
import ru.its.domain.repo.IOrdersRepository
import ru.its.domain.repo.SourceType
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 20.11.17.
 */
interface OrdersListView: MinimumMvpView {
    fun updateAdapter(list: List<OrderItem>)
    fun hideViews()
    fun stopRefresh()
    fun onInternetError()
}

@InjectViewState
class OrdersListPresenter (private val ordersRepository: IOrdersRepository): BasePresenter<OrdersListView>() {

    private var ordersSubscription: Subscription? = null

    fun init() {
        Observable.just(1)
                .doOnSubscribe {
                    viewState.hideViews()
                    viewState.startProgress("Загрузка заказов..")
                }
                .flatMap { ordersRepository.getAll(SourceType.CACHE).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    it?.let {
                        viewState.stopProgress()
                        viewState.updateAdapter(it.map { OrderItem(it) })
                    }
                },{
                    handleError(it, false, {})
                })
    }

    fun startLoad() {
        stopLoad()

        val time = MinimumApp.instance.firebaseRemoteConfig.getLong("time_orders_list")

        ordersSubscription = Observable.interval(0, time, TimeUnit.SECONDS)
                .flatMap { ordersRepository.getAll(SourceType.NETWORK) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказы не получены")
                    handleError(it, true, {}, false )
                }
                .onErrorReturn { null }
                .subscribe {
                    viewState.stopProgress()
                    if (it != null) {
                        if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказы получены")
                        viewState.updateAdapter(it.map { OrderItem(it) })
                    }
                }
    }

    fun stopLoad() {
        if (ordersSubscription?.isUnsubscribed == false) ordersSubscription?.unsubscribe()
    }

    fun refresh() {
        ordersRepository.getAll(SourceType.NETWORK).compose(SchedulerUtils.IoMainScheduler())
                .doOnTerminate {
                    viewState.stopProgress()
                    viewState.stopRefresh()
                }
                .subscribe({
                    it?.let { viewState.updateAdapter(it.map { OrderItem(it) }) }
                },{
                    handleError(it, false, {})
                })
    }

    fun reload() {
        viewState.hideViews()
        viewState.startProgress("Загрузка заказов..")
        refresh()
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
        viewState.onInternetError()
    }
}