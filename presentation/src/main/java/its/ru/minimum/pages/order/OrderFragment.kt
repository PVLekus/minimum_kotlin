package its.ru.minimum.pages.order

import android.Manifest
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import its.ru.minimum.pages.cities.CitiesFragment
import its.ru.minimum.pages.confirm.ConfirmFragment
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.pages.order.order_details.OrderDetailsFragment
import its.ru.minimum.pages.order.order_list.OrdersListFragment
import its.ru.minimum.pages.search.SearchFragment
import kotlinx.android.synthetic.main.fragment_order.view.*
import kotlinx.android.synthetic.main.item_order.view.*
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import ru.its.data.repo.PrefHelper
import ru.its.domain.model.City
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint


/**
 * Created by oleg on 07.11.17.
 */
class OrderFragment : MenuFragment(), OrderView, MainAdapter.AdapterListener, LocationListener {

    private lateinit var adapter: MainAdapter
    private var popUpMenu: PopupMenu? = null
    private var snackBar: Snackbar? = null
    private var isCheckCity = false
    private var alert: AlertDialogBuilder? = null

    private val prefHelper by inject<PrefHelper>()
    @InjectPresenter lateinit var presenter: OrderPresenter

    @ProvidePresenter
    fun providePresenter(): OrderPresenter
            = OrderPresenter(get(), get(), get(), get(), get(), get(), get(), this)

    companion object {
        val TAG = "OrderFragment"
        fun instance() = OrderFragment()
    }

    override fun getBaseTag(): String = TAG

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_order

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.tuneToolbar()

        adapter = MainAdapter(this)
        view.pointsList insertAdapter adapter

        view.addNewPoint?.setOnClickListener { presenter.addPoint() }

    }

    override fun onFindActiveOrders(list: List<Order>) {
        if (list.isNotEmpty() && snackBar?.isShown != true) {
            snackBar = showSnackBar("Есть заказы", Snackbar.LENGTH_INDEFINITE)
            snackBar?.setAction("Открыть") { if (list.size == 1) router.navigateTo(OrderDetailsFragment.TAG, list[0]) else openDrawerItem(OrdersListFragment.TAG) }
            snackBar?.show()
        } else if (list.isEmpty()) {
            snackBar?.dismiss()
            if (!isCheckCity) {
                checkPermission {
                    presenter.checkCity()
                    isCheckCity = true
                }
            }
        }
    }

    private fun checkPermission(func: ()->Unit) {

        val permissions = arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION)
        val rationale = getString(R.string.city_location_permission_rationale)
        val options = Permissions.Options()
                .setRationaleDialogTitle(getString(R.string.info))
                .setSettingsDialogTitle(getString(R.string.warning))

        Permissions.check(context, permissions, rationale, options, object: PermissionHandler() {
            override fun onGranted() {
                func.invoke()
            }
        })

    }

    override fun onItemClick(item: SearchPoint, position: Int, type: Int, view: View?) {


        when (view?.id) {

            R.id.pointMenu -> {

                popUpMenu?.dismiss()
                popUpMenu = PopupMenu(view.context, view.pointMenu)
                        .also {
                            it.inflate(R.menu.main_menu)
                            it.menu.findItem(R.id.menu_up).isVisible = position != 0
                            it.menu.findItem(R.id.menu_down).isVisible = position != adapter.list.lastIndex
                            it.menu.findItem(R.id.menu_delete).isVisible = true /*!(adapter.list.count() == 2 && (position == adapter.list.lastIndex || position == 0))*/
                        }.also {

                    it.setOnMenuItemClickListener {

                        when (it.itemId) {

                            R.id.menu_up -> presenter.moveItemUp(position).let { true }

                            R.id.menu_down -> presenter.moveItemDown(position).let { true }

                            R.id.menu_delete -> presenter.deleteItemAtPosition(position).let { true }

                            else -> false
                        }
                    }

                }
                popUpMenu?.show()

            }

            else -> {
                if (item.id != null) router.navigateTo(DetailsFragment.TAG, Pair(position, item))
                else router.navigateTo(SearchFragment.TAG, position)
            }

        }

    }

    override fun tuneToolbar(title: String?) {
        tuneToolbar(title ?: "Ошибка", true)?.let {
            it.findViewById<ImageButton>(R.id.rightButton)?.let {
                it.setImageResource(R.drawable.ic_call)
                it.visibility = View.VISIBLE
                it.setOnClickListener { presenter.makeCallToOperator() }
            }
            it.findViewById<TextView>(R.id.toolbar_title)?.setOnClickListener { router.navigateTo(CitiesFragment.TAG) }
        }
    }

    override fun updateAdapter(list: List<SearchPoint>) {
        adapter?.update(list)
        view?.addNewPoint?.visibility = if (list.count() < 6) View.VISIBLE else View.GONE
        presenter?.getCost(list)
    }

    override fun onGetCost() {
        view?.priceText?.text = "..."
        view?.btnMakeOrder?.isClickable = false
    }

    override fun onGetCostSuccess(cost: String) {
        view?.priceText?.text = if (!cost.isNullOrBlank() && cost != "Ошибка") "$cost ${presenter.currency}" else cost
        view?.btnMakeOrder?.isClickable = !cost.isNullOrBlank() && cost != "Ошибка"
        view?.btnMakeOrder?.setOnClickListener { if (!cost.isNullOrBlank()) router.navigateTo(ConfirmFragment.TAG, cost) }
    }

    override fun onChangeCity(city: City) {
        presenter.tuneToolbar()
        view?.priceText?.hint = "0 ${city.currency}"
    }

    override fun updateCurrency(currency: String) {
        view?.priceText?.hint = "0 $currency"
    }

    override fun onFindCloserCity(city: City) {
        alert?.dismiss()
        alert = activity!!.alert {
            message("Ваш текущий город: ${city.name}")
            positiveButton(R.string.yes) {
                presenter.changeCity(city)
                alert?.dismiss()
            }
            neutralButton(R.string.disable) {
                prefHelper.put(PrefHelper.Key.AUTO_LOCATION, false)
                alert?.dismiss()
            }
            negativeButton(R.string.no) {
                prefHelper.put(PrefHelper.Key.AUTO_LOCATION_TEMP, false)
                alert?.dismiss()
            }
        }.show()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        popUpMenu?.dismiss()
        snackBar?.dismiss()
    }

    override fun onStop() {
        super.onStop()
        isCheckCity = false
        presenter.stopLoad()
    }

}