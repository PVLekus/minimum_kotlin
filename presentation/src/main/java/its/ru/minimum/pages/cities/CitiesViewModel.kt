package its.ru.minimum.pages.cities

import android.arch.lifecycle.MutableLiveData
import its.ru.minimum.activities.main.MainPresenter
import its.ru.minimum.general.Resource
import its.ru.minimum.mvvm.BaseViewModel
import its.ru.minimum.pages.cities.items.OtherCityItem
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.usecase.CitySelectUseCase
import ru.its.domain.model.City
import ru.its.domain.repo.ICitiesRepository
import ru.its.domain.repo.IUserRepository
import ru.its.domain.repo.SourceType

class CitiesViewModel(private val citiesRepository: ICitiesRepository,
                      private val userRepository: IUserRepository,
                      private val citySelectUseCase: CitySelectUseCase): BaseViewModel(), OtherCityItem.IPresenter {

    private var userCity: City? =null

    init {
        userCity = userRepository.getUser()?.city
    }

    val loadingCities: MutableLiveData<Resource<UpdateCitiesData>> = MutableLiveData()
    val selectingCity: MutableLiveData<Resource<Boolean>> = MutableLiveData()

    fun loadContent() {
        loadingCities.value = Resource.LOADING()
        loadCities(SourceType.FIRST)
    }

    private fun loadCities(sourceType: SourceType) {

        loadingCities.value = Resource.LOADING()

        citiesRepository.getAll(sourceType)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    loadingCities.value = Resource.SUCCESS(UpdateCitiesData(it!!, userCity))
                },{
                    it.printStackTrace()
                    loadingCities.value = Resource.ERROR(it)
                })

    }

    fun refresh() {
        loadCities(SourceType.NETWORK)
    }

    fun selectCity(city: City) {

        selectingCity.value = Resource.LOADING()

        executeUseCase(citySelectUseCase, city, {
            selectingCity.value = Resource.SUCCESS(true)
            MainPresenter.cityChangeSubject.onNext(city)
        },{
            it.printStackTrace()
            selectingCity.value = Resource.ERROR(it)
        })

    }

    override fun isSelected(city: City): Boolean {
        return userCity?.let {
            city.id == it.id
        } ?: false
    }
}

data class UpdateCitiesData(
        val list: List<City>,
        val default: City?
)