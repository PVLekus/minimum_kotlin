package its.ru.minimum.pages.order.order_list

import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import its.ru.minimum.R
import its.ru.minimum.general.bindView
import its.ru.minimum.general.bindViews
import kotlinx.android.synthetic.main.item_address.view.*
import org.joda.time.format.DateTimeFormat
import ru.its.domain.model.Order
import ru.its.domain.model.StatusText

class OrderItem(val order: Order): AbstractItem<OrderItem, OrderItem.ViewHolder>() {

    override fun getIdentifier(): Long {
        return (order.hashCode()*33).toLong()
    }

    override fun getType(): Int = R.id.item_order

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun getLayoutRes(): Int = R.layout.item_current_order

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as OrderItem

        if (order != other.order) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + order.hashCode()
        return result
    }


    class ViewHolder(view: View): FastAdapter.ViewHolder<OrderItem>(view) {

        private val histContainer by bindView<LinearLayout>(R.id.histContainer)
        private val status by bindView<AppCompatTextView>(R.id.histStatusText)
        private val date by bindView<AppCompatTextView>(R.id.histDateText)
        private val car by bindView<AppCompatTextView>(R.id.car_info)

        init {
            bindViews(itemView)
        }

        override fun unbindView(item: OrderItem) {

        }

        override fun bindView(item: OrderItem, payloads: MutableList<Any>) {

            val inflater = LayoutInflater.from(itemView.context)
            histContainer.removeAllViews()

            status.text = StatusText.fromStatusCode(if (item.order.canceled == 1) 111 else item.order.status)
            val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
            date.text = formatter.parseDateTime(item.order.createdAt).toString("dd MMMM в HH:mm")

            car.visibility = if (item.order.car_info?.number.isNullOrBlank()) View.GONE else View.VISIBLE
            car.text = "${item.order.car_info?.color?.capitalize()} ${item.order.car_info?.brand?.capitalize()} ${item.order.car_info?.number?.toUpperCase()}"

            item.order.points?.forEachIndexed { index, pointsItem ->

                val address = inflater?.inflate(R.layout.item_address, null)

                address?.addressText?.text = pointsItem.name

                address?.ic_favorite?.setImageLevel(if (pointsItem.favorite) 1 else 0)

                address?.addressShape?.setBackgroundResource(if (index == 0) R.drawable.circle_green else R.drawable.circle_red)

                histContainer.addView(address)
            }

        }
    }

}