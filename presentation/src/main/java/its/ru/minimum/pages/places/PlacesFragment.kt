package its.ru.minimum.pages.places

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.activities.main.MainActivity.Companion.POINT_TYPE
import its.ru.minimum.general.SimpleAdapter
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.pages.order.OrderFragment
import kotlinx.android.synthetic.main.fragment_list.view.*
import org.koin.android.ext.android.get
import ru.its.domain.model.PointType
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 14.11.17.
 */
class PlacesFragment: BaseFragment(), PlacesView, PlacesAdapter.AdapterListener {

    private lateinit var adapter: SimpleAdapter<SearchPoint>
    @InjectPresenter lateinit var presenter: PlacesPresenter

    @ProvidePresenter fun providePresenter(): PlacesPresenter =
            PlacesPresenter(get(), get(), get(), get(), get())

    companion object {
        val TAG = "PlacesFragment"
        val TAG_FAVORITES = "PlacesFragment_FAVORITES"

        fun instance(type: PointType): PlacesFragment {
            return PlacesFragment().also {
                it.arguments = Bundle().also {
                    it.putString(POINT_TYPE, type.name)
                }

            }
        }

        fun instance(orderPosition: Int, type: PointType): PlacesFragment {
            return PlacesFragment().also {
                it.arguments = Bundle().also {
                    it.putString(POINT_TYPE, type.name)
                    it.putInt(ORDER_POSITION, orderPosition)
                }

            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.readArguments(arguments)

        adapter = PlacesAdapter(this)

        view.list insertAdapter adapter

        view.empty_list?.setText(R.string.empty_list_search)
        view.btnReloadList?.setOnClickListener { presenter.start() }

        view.swipeRefresh.isEnabled = false

        presenter.start()

    }

    override fun setToolbarTitle(title: String) {
        tuneToolbar(title, false)
    }

    override fun onItemClick(item: SearchPoint) {
        presenter.openDetails(item)
    }

    override fun onSuccessUpdate() {
        router.backTo(OrderFragment.TAG)
    }

    override fun updateAdapter(list: List<SearchPoint>) {
        view?.empty_list?.visibility = if (list.isEmpty()) View.VISIBLE else View.INVISIBLE
        adapter.update(list)
    }

    override fun onInternetError() {
        view?.no_internet_container?.visibility = View.VISIBLE
    }

    override fun hideViews() {
        view?.empty_list?.visibility = View.INVISIBLE
        view?.no_internet_container?.visibility = View.INVISIBLE
    }

}