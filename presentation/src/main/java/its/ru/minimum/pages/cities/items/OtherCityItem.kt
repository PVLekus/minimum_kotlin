package its.ru.minimum.pages.cities.items

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import its.ru.minimum.R
import kotlinx.android.synthetic.main.item_other_city.view.*
import ru.its.domain.model.City

class OtherCityItem(val city: City, private val presenter: IPresenter?): AbstractItem<OtherCityItem, OtherCityItem.ViewHolder>() {

    override fun getIdentifier(): Long {
        return city.hashCode().toLong()
    }

    override fun getType(): Int = R.id.item_other_city

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun getLayoutRes(): Int = R.layout.item_other_city

    fun isCitySelected() = presenter?.isSelected(city) == true

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as OtherCityItem

        if (city != other.city) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + city.hashCode()
        return result
    }


    class ViewHolder(view: View): FastAdapter.ViewHolder<OtherCityItem>(view) {

        override fun bindView(item: OtherCityItem, payloads: MutableList<Any>) {
            itemView.icon_selected.visibility = if (item.isCitySelected()) View.VISIBLE else View.INVISIBLE
            itemView.city_text.text = item.city.name
        }

        override fun unbindView(item: OtherCityItem) {
            itemView.city_text.text = null
        }

    }

    interface IPresenter {
        fun isSelected(city: City): Boolean
    }

}