package its.ru.minimum.pages.geocode

import android.content.DialogInterface
import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.general.getIntOrNull
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.IMap
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.utils.SchedulerUtils
import org.osmdroid.util.GeoPoint
import ru.its.data.remote.RutaxiAPI
import ru.its.data.utils.LocationParser
import ru.its.data.utils.LocationUtil
import ru.its.data.utils.addToComposite
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IUserRepository
import ru.terrakok.cicerone.Router
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 15.11.17.
 */
interface GeocodeView: MinimumMvpView, IMap {
    fun dropViews(result: String)
    fun fillViews(point: SearchPoint)
    fun setZoomPoint(point: GeoPoint, zoom: Int)
}

@InjectViewState
class GeocodePresenter(private val rutaxiAPI: RutaxiAPI,
                       private val userRepository: IUserRepository,
                       private val locationListener: LocationListener,
                       private val router: Router): BasePresenter<GeocodeView>() {

    private var subject = PublishSubject.create<GeoPoint?>()
    var orderPosition: Int?= null

    fun readArguments(args: Bundle?) {

        orderPosition = args.getIntOrNull(ORDER_POSITION)

    }

    fun start() {

        subject
                .debounce(500, TimeUnit.MILLISECONDS)
                .doOnNext { if (it == null) viewState.dropViews("Ошибка") }
                .map { it!! }
                .subscribe {

                    rutaxiAPI.searchNearObjects(it.latitude, it.longitude)
                            .compose(SchedulerUtils.IoMainScheduler())
                            .subscribe({
                                if (it.isNotEmpty()) viewState.fillViews(it[0])
                                else viewState.dropViews("Ошибка")
                            },{
                                handleError(it, false, {})
                            })

                }.addToComposite(compositeSubscription)



    }

    fun startListenUserLocation() {

        locationListener.startListenLocation(1)
                ?.filter {  it != null }
                ?.flatMap { location -> LocationUtil().isPointInCity(location!!, userRepository.getUser()?.city).filter { it }.map { location } }
                ?.map { GeoPoint(it!!.latitude, it!!.longitude) }
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    viewState.setUpUserLocation(it)
                    viewState.setZoomPoint(it, 18)
                }?.addToComposite(compositeSubscription)

    }

    fun openDetails(point: SearchPoint) {
        orderPosition?.let {
            router.navigateTo(DetailsFragment.TAG, Pair(it, point))
        } ?: router.navigateTo(DetailsFragment.TAG_FAVORITE, point)
    }

    fun loadCityMap() {

        LocationParser().parse(userRepository.getUser()?.city)
                .subscribe({
                    viewState.zoomTo(12, it)
                    viewState.setZoomPoint(it, 12)
                },{
                    handleError(it, true, {})
                })

    }

    fun searchPoint(geoPoint: GeoPoint?) {
        viewState.dropSearchMyLocation()
        subject.onNext(geoPoint)
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }
}