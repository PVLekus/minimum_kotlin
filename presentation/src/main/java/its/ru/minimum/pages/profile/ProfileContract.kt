package its.ru.minimum.pages.profile

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import ru.its.data.usecase.UserExitUseCase
import ru.its.domain.repo.IUserRepository

/**
 * Created by oleg on 15.12.17.
 */
@StateStrategyType(SingleStateStrategy::class)
interface  ProfileView: MinimumMvpView {
    fun exit()
    fun updateUserName(name: String)
}

@InjectViewState
class ProfilePresenter (private val userExitUseCase: UserExitUseCase,
                        private val userRepository: IUserRepository): BasePresenter<ProfileView>() {

    fun exitFromProfile() {
        executeUseCase(userExitUseCase, true, {
            viewState.exit()
        },{
            handleError(it, false, {})
        })
    }

    fun init() {

        userRepository.getUser()?.let { viewState.updateUserName(it.name) }

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
    }
}