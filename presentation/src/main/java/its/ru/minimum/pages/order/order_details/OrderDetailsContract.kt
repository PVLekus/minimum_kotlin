package its.ru.minimum.pages.order.order_details

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import its.ru.minimum.BuildConfig
import its.ru.minimum.MinimumApp
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.IMap
import its.ru.minimum.mvp.activity_interfaces.CallView
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import org.jetbrains.anko.toast
import org.osmdroid.util.GeoPoint
import ru.its.data.remote.RutaxiAPI
import ru.its.data.utils.LocationParser
import ru.its.data.utils.addToComposite
import ru.its.domain.model.Coordinates
import ru.its.domain.model.Order
import ru.its.domain.repo.IOrdersRepository
import ru.its.domain.repo.IUserRepository
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 04.12.17.
 */
interface OrderDetailsView: MinimumMvpView, IMap, CallView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateView(order: Order?)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun updateCoordinates(coordinates: Coordinates?, userLocation: GeoPoint?)
}

@InjectViewState
class OrderDetailsPresenter(private val userRepository: IUserRepository,
                            private val ordersRepository: IOrdersRepository,
                            private val rutaxiAPI: RutaxiAPI,
                            private val locationListener: LocationListener): BasePresenter<OrderDetailsView>() {

    private var coordinates: Coordinates? = null
    private var userLocation: GeoPoint? = null
    private var ordersSubscription: Subscription? = null
    val currency by lazy { userRepository?.getUser()?.city?.currency ?:"RUB" }

    fun loadOrder(id: String?) {

        if (id == null) {
            handleError(NullPointerException("null id in order"), true, {}, false)
            return
        }

        val time = MinimumApp.instance.firebaseRemoteConfig.getLong("time_orders_current")

        stopLoad()
        ordersSubscription = Observable.interval(0, time, TimeUnit.SECONDS)
                .flatMap { ordersRepository.getOrder(id) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказы не получены")
                    handleError(it, true, {}, false )
                }
                .onErrorReturn { null }
                .subscribe {
                    it?.let {
                        it.coordinates?.let { coordinates = it }
                        viewState.updateView(it)
                        viewState.updateCoordinates(coordinates, userLocation)
                        if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказ успешно получен")
                    } ?: if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказ не найден")
                }

    }

    fun reloadOrder(id: String?) {
        if (id == null) {
            handleError(NullPointerException("null id in order"), true, {}, false)
            return
        }

        viewState.startProgress("Обновление заказа..")

        ordersRepository.getOrder(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { null }
                .subscribe {
                    viewState.stopProgress()
                    it?.let {
                        it.coordinates?.let { coordinates = it }
                        viewState.updateView(it)
                        viewState.updateCoordinates(coordinates, userLocation)
                        if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказ успешно получен")
                    } ?: if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказ не найден")
                }

    }

    fun stopLoad() {
        if (ordersSubscription?.isUnsubscribed == false) ordersSubscription?.unsubscribe()
    }

    fun cancelOrder(id: String) {

        stopLoad()

        Observable.just(1)
                .doOnSubscribe { viewState.startProgress("Отмена заказа..") }
                .flatMap { rutaxiAPI.cancelOrder(id).compose(SchedulerUtils.IoMainScheduler()) }
                .flatMap { ordersRepository.updateOrder(it).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    viewState.stopProgress()
                    viewState.updateView(it)
                    loadOrder(id)
                },{
                    viewState.stopProgress()
                    handleError(it, false, {})
                    loadOrder(id)
                })

    }

    fun payByCard(id: String) {

        stopLoad()

        Observable.just(1)
                .doOnSubscribe { viewState.startProgress("Оплата заказа..") }
                .flatMap { rutaxiAPI.payCard(id).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    viewState.stopProgress()
                    viewState.showDialog("Успешная оплата заказа")
                    loadOrder(id)
                },{
                    viewState.stopProgress()
                    handleError(it, false, {})
                    loadOrder(id)
                })


    }

    fun loadCityMap() {

        LocationParser().parse(userRepository.getUser()?.city)
                .subscribe({
                    viewState.zoomTo(13, it)
                },{
                    handleError(it, true, {})
                })

    }

    fun loadUserLocation() {

        locationListener.startListenLocation(1)
                ?.filter {  it != null }
                ?.take(1)
                ?.map { GeoPoint(it!!.latitude, it!!.longitude) }
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    userLocation = it
                    viewState.setUpUserLocation(it)
                }
                ?.addToComposite(compositeSubscription)

    }

    fun makeCallToOperator() {
        userRepository.getUser()?.city?.rate?.params?.operatorPhone?.let {
            viewState.makeCall(it)
        } ?: viewState.showToast("Ошибка получения номера оператора")

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }
}