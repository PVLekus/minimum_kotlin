package its.ru.minimum.pages.search

import android.content.DialogInterface
import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.general.getIntOrNull
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.pages.favorites.list.FavoritesFragment
import its.ru.minimum.pages.geocode.GeocodeFragment
import its.ru.minimum.pages.order.OrderFragment
import its.ru.minimum.pages.places.PlacesFragment
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.model.PointType
import ru.its.domain.model.SearchPoint
import ru.its.domain.model.SearchPointWrapper
import ru.its.domain.repo.IHistoryPointsRepository
import ru.terrakok.cicerone.Router
import rx.Observable
import rx.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 13.11.17.
 */
interface SearchContentView : MinimumMvpView {
    fun loadDefaults()
    fun updateAdapter(list: List<SearchPointWrapper>)
    fun onInternetError()
    fun setToolbarTitle(title: Int)
}

@InjectViewState
class SearchPresenter(private val historyRepository: IHistoryPointsRepository,
                      private val rutaxiAPI: RutaxiAPI,
                      private val router: Router) : BasePresenter<SearchContentView>() {

    private var subject = PublishSubject.create<String?>()
    var orderPosition: Int? = null

    fun readArguments(args: Bundle?) {

        orderPosition = args.getIntOrNull(ORDER_POSITION)

        viewState.setToolbarTitle(if (orderPosition == null) R.string.fav_title else if (orderPosition == 0) R.string.from_title else R.string.to_title)

    }

    fun loadDefaultList() {

        getDefaultList()
                .doOnNext { viewState.updateAdapter(it) }
                .map {

                    it.also {

                        it.add(SearchPointWrapper().also {
                            it.name = "История"
                            it.type = SearchAdapter.TYPE_SEPARATOR
                        })

                    }

                }
                .flatMap { defaults -> historyRepository.getAll().compose(SchedulerUtils.IoMainScheduler())
                        .map {
                            it?.forEach {
                                val point = SearchPointWrapper()
                                point.type = SearchAdapter.TYPE_HISTORY
                                point.point = it
                                defaults.add(point)
                            }
                            defaults
                        }
                }
                .subscribe({
                    viewState.updateAdapter(it)
                }, {
                    handleError(it, true, {})
                })
    }

    fun openMap() {
        orderPosition?.let { router.navigateTo(GeocodeFragment.TAG, it) } ?: router.navigateTo(GeocodeFragment.TAG_FAVORITE)
    }

    fun onItemClick(item: SearchPointWrapper) {
        if (item.typeSelect == PointType.FAVORITES) router.navigateTo(FavoritesFragment.TAG_ORDER, orderPosition)
        else orderPosition?.let {
            router.navigateTo(PlacesFragment.TAG, Pair(it, item.typeSelect))
        } ?: router.navigateTo(PlacesFragment.TAG_FAVORITES, item.typeSelect)
    }

    fun onPointClick(point: SearchPoint?) {

        point?.let {
            orderPosition?.let {
                router.navigateTo(DetailsFragment.TAG, Pair(it, point))
            } ?: router.navigateTo(DetailsFragment.TAG_FAVORITE, point)
        }

    }

    fun onBackPressed() {
        orderPosition?.let {
            router.backTo(OrderFragment.TAG)
        } ?: router.backTo(FavoritesFragment.TAG)
    }

    fun init() {

        subject
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter { it != null }
                .filter { it!!.count() >= 3 }
                .subscribe {

                    rutaxiAPI.searchObjects(it!!)
                            .compose(SchedulerUtils.IoMainScheduler())
                            .subscribe({ points ->

                                val wrappers = mutableListOf<SearchPointWrapper>().also { list ->
                                    points?.forEach { point ->
                                        list.add(SearchPointWrapper().also {
                                            it.point = point
                                            it.type = SearchAdapter.TYPE_POINT
                                        })
                                    }
                                }

                                viewState.updateAdapter(wrappers)

                            }, {
                                handleError(it, true, {})
                            })

                }


    }

    fun search(word: String?) {

        subject.onNext(word)

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
        viewState.onInternetError()
    }

    private fun getDefaultList(): Observable<MutableList<SearchPointWrapper>> {

        return Observable.just(mutableListOf<SearchPointWrapper>())
                .map {
                    it.also {

                        it.add(SearchPointWrapper().also {
                            it.name = "Основные"
                            it.type = SearchAdapter.TYPE_SEPARATOR
                        })

                        if (orderPosition != null) it.add(SearchPointWrapper().also {
                            it.name = "Избранные адреса"
                            it.type = SearchAdapter.TYPE_ITEM
                            it.typeSelect = PointType.FAVORITES
                        })

                        it.add(SearchPointWrapper().also {
                            it.name = "Популярные"
                            it.type = SearchAdapter.TYPE_ITEM
                            it.typeSelect = PointType.POPULARS
                        })

                        it.add(SearchPointWrapper().also {
                            it.name = "Аэропорты"
                            it.type = SearchAdapter.TYPE_ITEM
                            it.typeSelect = PointType.AIRPORTS
                        })

                        it.add(SearchPointWrapper().also {
                            it.name = "Вокзалы"
                            it.type = SearchAdapter.TYPE_ITEM
                            it.typeSelect = PointType.STATIONS
                        })

                    }
                }

    }
}