package its.ru.minimum.pages.order.order_details

import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.gson.Gson
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER
import its.ru.minimum.general.inflate
import its.ru.minimum.general.navigation.BackButtonListener
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import its.ru.minimum.pages.order.order_list.OrdersListFragment
import kotlinx.android.synthetic.main.dialog_call.view.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import kotlinx.android.synthetic.main.fragment_order_details.view.*
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.jetbrains.anko.longToast
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import ru.its.data.utils.AddressBuilder
import ru.its.data.utils.LocationParser
import ru.its.domain.model.Coordinates
import ru.its.domain.model.Order
import ru.its.domain.model.StatusText

/**
 * Created by oleg on 24.11.17.
 */
class OrderDetailsFragment : BaseFragment(), OrderDetailsView, BackButtonListener, LocationListener {

    private val tileSource by inject<ITileSource>()

    private var order: Order? = null
    private var orderMarker: Marker? = null
    private var driverMarker: Marker? = null
    private var isZoomToSpan = false
    private var alert: AlertDialogBuilder? = null

    private val gson by inject<Gson>()
    @InjectPresenter
    lateinit var presenter: OrderDetailsPresenter

    @ProvidePresenter
    fun providePresenter(): OrderDetailsPresenter = OrderDetailsPresenter(get(), get(), get(), this)

    companion object {
        val TAG = "OrderDetailsFragment"
        fun instance(order: Order): OrderDetailsFragment = OrderDetailsFragment().also { it.arguments = Bundle().also { it.putString(ORDER, Gson().toJson(order)) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_order_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            order = it.getString(ORDER, null)?.let { gson.fromJson(it, Order::class.java) }
        }

        switchToolbar(false)

        view.map?.let {
            it.setTileSource(tileSource)
            it.setBuiltInZoomControls(true)
            it.setMultiTouchControls(true)
        }

        view.btnFullscreen?.setOnClickListener {
            if (view.orderBlock?.visibility == View.GONE) view?.orderBlock?.visibility = View.VISIBLE
            else view.orderBlock?.visibility = View.GONE
        }

        view.orderBack?.setOnClickListener { activity?.onBackPressed() }

        presenter.loadCityMap()
        presenter.loadUserLocation()

        updateView(order)
        updateCoordinates(order?.coordinates, null)

        view.orderReload.setOnClickListener { presenter.reloadOrder(order?.id) }

    }

    override fun onStart() {
        super.onStart()
        presenter.loadOrder(order?.id)
    }

    override fun onPause() {
        super.onPause()
        alert?.dismiss()
    }

    override fun onStop() {
        super.onStop()
        presenter.stopLoad()
    }

    override fun onBackPressed(): Boolean {
        router.backTo(OrdersListFragment.TAG)
        return true
    }

    override fun updateView(order: Order?) {

        view?.orderStatus?.text = StatusText.fromStatusCode(if (order?.canceled == 1) 111 else order?.status)

        view?.carInfo?.visibility = if (order?.car_info?.number.isNullOrBlank()) View.GONE else View.VISIBLE
        view?.carInfo?.text = "${order?.car_info?.color?.capitalize()} ${order?.car_info?.brand?.capitalize()} ${order?.car_info?.number?.toUpperCase()}"

        view?.secondAddress?.visibility = if (order?.points?.count() ?: 0 < 3) View.GONE else View.VISIBLE
        view?.secondCircle?.visibility = if (order?.points?.count() ?: 0 < 3) View.GONE else View.VISIBLE

        view?.firstAddress?.text = AddressBuilder().build(order?.points?.get(0))
        view?.secondAddress?.text = if (order?.points?.count() == 3) AddressBuilder().build(order?.points?.get(1)) else "${order?.points?.count()?.minus(2)} промежуточных адреса"
        view?.thirdAddress?.text = AddressBuilder().build(order?.points?.last())

        view?.orderPrice?.text = "${order?.cost} ${presenter.currency}"

        view?.btnCancelOrder?.setOnClickListener {
            alert?.dismiss()
            alert = activity!!.alert {
                message(getString(R.string.msg_confirm_cancel))
                positiveButton(R.string.yes) {
                    presenter?.cancelOrder(order?.id ?: "0")
                    alert?.dismiss()
                }
                negativeButton(R.string.no) {
                    alert?.dismiss()
                }
            }.show()

        }
        view?.btnCancelOrder?.visibility = if (order?.isCanCancel() != false) View.VISIBLE else View.GONE
        view?.btnSendLocation?.visibility = if (order?.isNeedCardPay() == true || order?.isAlreadyPayd() == true) View.VISIBLE else View.GONE
        view?.btnSendLocation?.isEnabled = order?.isNeedCardPay() == true
        view?.btnSendLocation?.text = when {
            order?.isNeedCardPay() == true -> "Оплатить картой"
            order?.isAlreadyPayd() == true -> "Оплачено"
            else -> ""
        }

        showDialogSelectCall(order?.rate?.params?.driverPhone)

        view?.btnSendLocation?.setOnClickListener { presenter.payByCard(order?.id ?: "0") }

        driverMarker = updateMarkerLocation(driverMarker, order?.coordinates?.driver, R.drawable.ic_driver_placeholder)
        orderMarker = updateMarkerLocation(orderMarker, order?.coordinates?.client, R.drawable.ic_client_placeholder)

    }

    override fun updateCoordinates(coordinates: Coordinates?, userLocation: GeoPoint?) {

        LocationParser().calculatePointsForBoundingBox(coordinates)?.let { coords ->

            if (!isZoomToSpan) {
                isZoomToSpan = true
                Handler().postDelayed({
                    zoomToSpan(coords)
                }, 200)
            }

            view?.btnCenterOrder?.setOnClickListener { zoomToSpan(coords) }

        }
                ?: userLocation?.let { view?.btnCenterOrder?.setOnClickListener { zoomTo(17, userLocation) } }

    }

    private fun zoomToSpan(coords: List<Double>) {
        BoundingBox(coords[0], coords[1], coords[2], coords[3]).let { box ->
            view?.map?.controller?.let {
                it.setCenter(box.center)
                it.zoomToSpan(box.latitudeSpan, box.longitudeSpan)
            }
        }
    }

    private fun updateMarkerLocation(marker: Marker?, coordinates: List<Double>?, drawableInt: Int): Marker? {

        return coordinates?.let {

            if (it.isEmpty()) return marker

            GeoPoint(it[0], it[1]).let { point ->

                marker?.let {
                    it.position = point
                    view?.map?.invalidate()
                    return it
                }

                return Marker(view?.map)
                        .also {
                            it.position = point
                            it.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
                            it.setIcon(ContextCompat.getDrawable(activity!!, drawableInt))
                        }
                        .also {
                            view?.map?.overlays?.add(it)
                            view?.map?.invalidate()
                        }

            }

        } ?: marker

    }

    override fun zoomTo(zoomLevel: Int, geoPoint: GeoPoint) {
        view?.map?.controller?.let {
            it.setZoom(zoomLevel)
            it.setCenter(geoPoint)
        }
    }

    override fun replaceMarker(point: GeoPoint?) {
    }

    override fun setUpUserLocation(point: GeoPoint?) {

        Marker(view?.map).also {
            it.position = point
            it.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER)
            it.setIcon(ContextCompat.getDrawable(activity!!, R.drawable.my_location))
        }.let {
            view?.map?.overlays?.add(it)
            view?.map?.invalidate()
        }

    }

    override fun dropSearchMyLocation() {
    }

    private fun showDialogSelectCall(driverPhone: String?) {

        view?.btnCallSelect?.setOnClickListener {
            alert?.dismiss()
            alert = AlertDialogBuilder(context!!)
            alert?.customView(layoutInflater.inflate(R.layout.dialog_call, null, false).also {
                it.callDriver.setOnClickListener {
                    driverPhone?.let { makeCall(it) }
                            ?: activity?.longToast("Водитель не указал номер")
                    alert?.dismiss()
                }
                it.callOperator.setOnClickListener {
                    presenter.makeCallToOperator()
                    alert?.dismiss()
                }
            })
            alert?.show()
        }

    }

}