package its.ru.minimum.pages.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.pages.order.OrderFragment
import kotlinx.android.synthetic.main.fragment_list.view.*
import org.koin.android.ext.android.get
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 07.11.17.
 */
class HistoryFragment: MenuFragment(), HistoryView, OrdersAdapter.AdapterListener {

    @InjectPresenter lateinit var presenter: HistoryPresenter

    private lateinit var adapter: OrdersAdapter
    private var popUpMenu: PopupMenu? = null

    @ProvidePresenter fun providePresenter() : HistoryPresenter =
            HistoryPresenter(get(), get(), get())

    companion object {
        val TAG = "HistoryFragment"
        fun instance() = HistoryFragment()
    }

    override fun getBaseTag(): String = TAG

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tuneToolbar(R.string.hist_title, true)

        adapter = OrdersAdapter(this, false)

        view.list insertAdapter adapter

        view.empty_list?.setText(R.string.empty_hist)
        view.btnReloadList?.setOnClickListener { presenter.reload() }

        presenter.start()

        view.swipeRefresh.setOnRefreshListener { presenter.refresh() }
    }

    override fun openMainFragment() {
        openDrawerItem(OrderFragment.TAG)
    }

    override fun stopRefresh() {
        view?.swipeRefresh?.isRefreshing = false
    }

    override fun onRepeatClick(item: Order) {
        presenter.repeatRoute(item, false)
    }

    override fun onOptionClick(item: Order, view: View?) {
        popUpMenu?.dismiss()
        popUpMenu = PopupMenu(view?.context, view)
                .also { it.inflate(R.menu.history_menu)  }
                .also {
                    it.setOnMenuItemClickListener {
                        when (it.itemId) {

                            R.id.reverse -> {
                                presenter.repeatRoute(item, true)
                                true
                            }

                            R.id.delete -> {
                                presenter.removeOrder(item.id)
                                true
                            }

                            else -> false

                        }
                    }
                }
        popUpMenu?.show()
    }

    override fun updateAdapter(list: List<Order>) {
        view?.empty_list?.visibility = if (list.isEmpty()) View.VISIBLE else View.INVISIBLE
        adapter.update(list)
    }

    override fun isPointFavorite(searchPoint: SearchPoint): Boolean {
        return presenter.isPointFavorite(searchPoint)
    }

    override fun updateAdapter() {
        adapter.notifyDataSetChanged()
    }

    override fun removeOrder(id: String) {
        adapter.removeById(id)
    }

    override fun addItems(list: List<Order>) {
        adapter.addNewItems(list)
    }

    override fun onItemClicked(item: Order) {
    }

    override fun onFavClick(searchPoint: SearchPoint) {
        router.navigateTo(DetailsFragment.TAG_FAVORITE, searchPoint)
    }

    override fun onLoadNewItems(page: Int, count: Int) {
        presenter.loadNextPage(page)
    }

    override fun onLoadItemsFail() {
        adapter.onGetNewItemsFailure()
    }

    override fun hideViews() {
        view?.empty_list?.visibility = View.INVISIBLE
        view?.no_internet_container?.visibility = View.INVISIBLE
    }

    override fun onInternetError() {
        view?.no_internet_container?.visibility = View.VISIBLE
    }

    override fun onPause() {
        popUpMenu?.dismiss()
        super.onPause()
    }

}