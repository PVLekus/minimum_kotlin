package its.ru.minimum.pages.order

import android.view.View
import its.ru.minimum.R
import its.ru.minimum.general.BaseVH
import its.ru.minimum.general.SimpleAdapter
import kotlinx.android.synthetic.main.item_order.view.*
import ru.its.data.utils.AddressBuilder
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 15.11.17.
 */
class MainAdapter(val adapterView: AdapterListener) : SimpleAdapter<SearchPoint>(R.layout.item_order) {

    override fun getHolder(view: View): BaseVH<SearchPoint> {

        return object : BaseVH<SearchPoint>(view) {
            override fun fill(item: SearchPoint) {
                view?.setOnClickListener { adapterView.onItemClick(item, position, this.itemViewType, it) }
                val position = this.adapterPosition
                view?.pointAddress?.text = AddressBuilder().build(item) ?: if (position == 0) "Откуда поедете?" else "Куда поедете?"
                view?.preOrder?.visibility = if (position == 0 && !item.preOrder.isNullOrBlank()) View.VISIBLE else View.INVISIBLE
                view?.preOrder?.text = "Предварительный заказ: ${item.preOrder ?: ""}"

                if (item.id != null) {
                    view?.pointImg?.setImageResource(if (position == 0) R.mipmap.ic_firstpoint_active else R.mipmap.ic_nextpoint_active)
                } else view?.pointImg?.setImageResource(R.mipmap.ic_point_inactive)

                view?.pointMenu?.setOnClickListener { adapterView.onItemClick(item, position, this.itemViewType, it) }
            }
        }

    }

    interface AdapterListener {
        fun onItemClick(item: SearchPoint, position: Int, type: Int, view: View?)
    }

}
