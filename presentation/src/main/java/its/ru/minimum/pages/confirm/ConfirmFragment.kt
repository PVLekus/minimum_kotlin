package its.ru.minimum.pages.confirm

import android.Manifest
import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crashlytics.android.Crashlytics
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_SUM
import its.ru.minimum.general.Resource
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.pages.order.order_details.OrderDetailsFragment
import kotlinx.android.synthetic.main.fragment_confirm.view.*
import kotlinx.android.synthetic.main.item_booking_address.view.*
import org.jetbrains.anko.toast
import org.koin.android.viewmodel.ext.android.viewModel
import ru.its.data.utils.AddressBuilder
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.utils.PhoneNormalizer
import java.util.*

/**
 * Created by oleg on 25.11.17.
 */
class ConfirmFragment : BaseFragment() {

    private val viewModel by viewModel<ConfirmViewModel>()

    companion object {
        val TAG = "ConfirmFragment"
        val PICK_CONTACT = 1090
        fun instance(sum: String) = ConfirmFragment().also { it.arguments = Bundle().also { it.putString(ORDER_SUM, sum) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_confirm

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.readArguments(arguments)
        tuneToolbar("Заказ", false)

        view.phoneBook?.setOnClickListener {
            checkPermission {
                openContacts()
            }
        }

        view.radio_cash?.isChecked = true

        viewModel.updatingPoints.observe(this, updatingPointsObserver)
        viewModel.updatingPreOrder.observe(this, updatingPreOrderObserver)
        viewModel.updatingComment.observe(this, updatingCommentObserver)
        viewModel.updatingCost.observe(this, updatingCostObserver)
        viewModel.updatingPhone.observe(this, updatingPhoneObserver)
        viewModel.onMakeOrder.observe(this, onSuccessMakeOrderObserver)

    }

    private fun checkPermission(func: ()->Unit) {

        val permissions = arrayOf<String>(Manifest.permission.READ_CONTACTS)
        val rationale = getString(R.string.phonebook_permission_rationale)
        val options = Permissions.Options()
                .setRationaleDialogTitle(getString(R.string.info))
                .setSettingsDialogTitle(getString(R.string.warning))

        Permissions.check(context, permissions, rationale, options, object: PermissionHandler() {
            override fun onGranted() {
                func.invoke()
            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                showToast(getString(R.string.no_permission))
            }
        })

    }

    private val updatingPointsObserver = Observer<Resource.SUCCESS<List<SearchPoint>>> {
        it?.getOnce {points ->

            val inflater = LayoutInflater.from(view?.context)
            val last = points.lastIndex

            view?.bookingAddressContainer?.removeAllViews()

            points.forEachIndexed { index, searchPoint ->

                inflater?.inflate(R.layout.item_booking_address, view?.bookingAddressContainer, false).also {
                    it?.pointCount?.text = "${index + 1}"
                    it?.pointAddress?.text = AddressBuilder().build(searchPoint)
                    it?.divider?.visibility = if (index == last) View.INVISIBLE else View.VISIBLE
                }?.let {
                    view?.bookingAddressContainer?.addView(it)
                }

            }

            view?.btnConfirm?.setOnClickListener {
                viewModel.makeOrder(points, view?.phoneEdit?.text?.toString(), view?.comment?.text?.toString())
            }

        }
    }

    private val updatingPreOrderObserver = Observer<Resource.SUCCESS<String>> {
        it?.getOnce {
            view?.time?.text = it
        }
    }

    private val updatingCommentObserver = Observer<Resource.SUCCESS<String>> {
        it?.getOnce {
            view?.comment?.setText(it ?: "")
        }
    }

    private val updatingCostObserver = Observer<Resource.SUCCESS<String>> {
        it?.getOnce {
            view?.sum?.text = "$it ${viewModel.currency}"
        }
    }

    private val updatingPhoneObserver = Observer<Resource.SUCCESS<String>> {
        it?.getOnce {
            view?.phoneEdit?.setText(it)
        }
    }

    private val onSuccessMakeOrderObserver = Observer<Resource<Order>> {
        when (it) {
            is Resource.LOADING -> startProgress("")
            is Resource.SUCCESS -> it.getOnce {
                stopProgress()
                router.navigateTo(OrderDetailsFragment.TAG, it)
            }
            is Resource.ERROR -> it.get {
                stopProgress()
                handleError(it, true)
            }
        }
    }

    private fun openContacts() {

        val uri = Uri.parse("content://contacts")
        val intent = Intent(Intent.ACTION_PICK, uri)
        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(intent, PICK_CONTACT)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
                activity!!.contentResolver.query(data?.data, null, null, null, null).use { cursor ->
                    if (cursor.moveToFirst()) {
                        if (cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)).toInt() > 0) {
                            cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))?.let {
                                view?.phoneEdit?.setText(PhoneNormalizer().normalize(it, true))
                            }
                        } else activity?.toast("Не удалось извлечь номер")
                    }
                }

            }
        } catch (e: Exception) {
            e?.printStackTrace()
            Crashlytics.logException(e)
            activity?.toast("Не удалось извлечь номер")
        }
    }

}