package its.ru.minimum.pages.search

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.general.navigation.BackButtonListener
import its.ru.minimum.mvp.BaseFragment
import kotlinx.android.synthetic.main.fragment_list.view.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import org.koin.android.ext.android.get
import ru.its.domain.model.SearchPointWrapper

/**
 * Created by oleg on 13.11.17.
 */
class SearchFragment: BaseFragment(), SearchContentView, BackButtonListener, SearchAdapter.AdapterListener {

    private lateinit var adapter: SearchAdapter
    @InjectPresenter lateinit var presenter: SearchPresenter

    @ProvidePresenter fun providePresenter(): SearchPresenter = SearchPresenter(get(), get(), get())

    companion object {
        val TAG = "SearchFragment"
        val TAG_FAVORITE = "SearchFragment_FAVOROTE"

        fun instance(): SearchFragment = SearchFragment()

        fun instance(orderPosition: Int) : SearchFragment {
            return SearchFragment().also {
                it.arguments = Bundle().also { it.putInt(ORDER_POSITION, orderPosition) }
            }
        }
    }

    override fun setToolbarTitle(title: Int) {

        val title = getString(title)

        tuneToolbar(title, false)?.let {

            it.findViewById<ImageButton>(R.id.rightButton)?.let {
                it.setImageDrawable(ContextCompat.getDrawable(it.context, R.drawable.ic_nav_map2))
                it.visibility = View.VISIBLE
                it.setOnClickListener {
                    presenter.openMap()
                }
            }

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.swipeRefresh.isEnabled = false

        presenter.readArguments(arguments)

        adapter = SearchAdapter(this)

        view.list?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        view.list?.adapter = adapter

        view.search?.visibility = View.VISIBLE
        view.search?.setOnClickListener { view.search.isIconified = false }
        view.search?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {

                view.empty_list?.setText(R.string.enter_minimum_text)

                if (newText?.length ?: 0 == 0) {
                    view.empty_list?.visibility = View.INVISIBLE
                    presenter.loadDefaultList()
                } else if (newText?.length ?: 0 < 3) {
                    view.empty_list?.visibility = View.VISIBLE
                    adapter.update(listOf())
                } else {
                    view.empty_list?.visibility = View.INVISIBLE
                    presenter.search(newText)
                }

                return false
            }
        })
        view.search?.setOnCloseListener {
            loadDefaults()
            false
        }

        loadDefaults()

    }

    override fun onResume() {
        super.onResume()
        if (view?.search?.query.isNullOrEmpty()) {
            hideViews()
            presenter.loadDefaultList()
        }
        presenter.init()
    }

    override fun onItemClick(item: SearchPointWrapper, type: Int) {

        when (type) {
            SearchAdapter.TYPE_ITEM -> {
                presenter.onItemClick(item)
            }
            SearchAdapter.TYPE_POINT, SearchAdapter.TYPE_HISTORY -> {
                view?.search?.setQuery("", false)
                presenter.onPointClick(item.point)
            }
        }

    }

    override fun loadDefaults() {
        hideViews()
        presenter.loadDefaultList()
    }

    override fun onInternetError() {
        adapter.update(listOf())
        view?.no_internet_container?.visibility = View.VISIBLE
    }

    override fun hideViews() {
        view?.empty_list?.visibility = View.INVISIBLE
        view?.no_internet_container?.visibility = View.INVISIBLE
    }

    override fun updateAdapter(list: List<SearchPointWrapper>) {
        view?.empty_list?.setText(R.string.empty_list_search)
        view?.empty_list?.visibility = if (list.isEmpty()) View.VISIBLE else View.INVISIBLE
        adapter.update(list)
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }

}
