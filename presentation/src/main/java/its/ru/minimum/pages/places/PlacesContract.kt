package its.ru.minimum.pages.places

import android.content.DialogInterface
import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.activities.main.MainActivity.Companion.POINT_TYPE
import its.ru.minimum.general.getIntOrNull
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.pages.details.DetailsFragment
import its.ru.minimum.utils.SchedulerUtils
import ru.its.domain.model.PointType
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.*
import ru.terrakok.cicerone.Router

/**
 * Created by oleg on 14.11.17.
 */
interface PlacesView: MinimumMvpView {
    fun updateAdapter(list: List<SearchPoint>)
    fun onInternetError()
    fun hideViews()
    fun setToolbarTitle(title: String)
    fun onSuccessUpdate()
}

@InjectViewState
class PlacesPresenter(private val airportsRepository: IAirportsRepository,
                      private val stationsRepository: IStationsRepository,
                      private val popularRepository: IPopularRepository,
                      private val bookingRepository: IBookingRepository,
                      private val router: Router): BasePresenter<PlacesView>() {

    private var type = PointType.POPULARS.name
    var orderPosition: Int? = null


    fun readArguments(args: Bundle?) {

        orderPosition = args.getIntOrNull(ORDER_POSITION)

        args?.let {
            type = it.getString(POINT_TYPE, PointType.POPULARS.name)
        }

        val toolbar_title = when (type) {

            PointType.AIRPORTS.name -> "Аэропорты"
            PointType.STATIONS.name -> "Вокзалы"
            PointType.POPULARS.name -> "Популярные"
            else -> "Популярные"
        }

        viewState.setToolbarTitle(toolbar_title)
    }

    fun openDetails(item: SearchPoint) {

        orderPosition?.let {
            if (item.metatype != 0 && it != 0) confirm(it, item)
            else router.navigateTo(DetailsFragment.TAG, Pair(it, item))
        } ?: router.navigateTo(DetailsFragment.TAG_FAVORITE, item)


    }

    fun start() {

        viewState.hideViews()
        viewState.startProgress("Загрузка..")

        getRepository(type)
                .getAll()
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    viewState.stopProgress()
                    it?.let { viewState.updateAdapter(it) }
                },{
                    viewState.stopProgress()
                    handleError(it, true, {})
                })

    }

    private fun confirm(position: Int, searchPoint: SearchPoint) {

        bookingRepository.updateItemByPosition(searchPoint,position, null)
                .subscribe({
                    viewState.onSuccessUpdate()
                },{
                    handleError(it, true, {})
                })
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
        viewState.onInternetError()
    }

    private fun getRepository(type: String) : IPointsRepository {

        return when(type) {

            PointType.AIRPORTS.name -> airportsRepository
            PointType.STATIONS.name -> stationsRepository
            PointType.POPULARS.name -> popularRepository
            else -> popularRepository

        }

    }

}