package its.ru.minimum.pages.history

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MenuView
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.utils.addToComposite
import ru.its.domain.model.Booking
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IBookingRepository
import ru.its.domain.repo.IFavoritesRepository
import ru.its.domain.repo.IHistoryRepository
import ru.its.domain.repo.SourceType
import rx.Observable
import rx.android.schedulers.AndroidSchedulers

/**
 * Created by oleg on 08.11.17.
 */
interface HistoryView : MinimumMvpView, MenuView {
    fun updateAdapter(list: List<Order>)
    fun updateAdapter()
    fun addItems(list: List<Order>)
    fun removeOrder(id: String)
    fun stopRefresh()
    fun hideViews()
    fun onLoadItemsFail()
    fun onInternetError()

    @StateStrategyType(SkipStrategy::class)
    fun openMainFragment()
}

@InjectViewState
class HistoryPresenter(private val bookingRepository: IBookingRepository,
                       private val historyRepository: IHistoryRepository,
                       private val favoritesRepository: IFavoritesRepository) : BasePresenter<HistoryView>() {

    private val favIds = mutableListOf<Int>()

    fun isPointFavorite(searchPoint: SearchPoint): Boolean {
        return searchPoint.id?.let { favIds.contains(it) } ?: false
    }

    fun repeatRoute(order: Order, isReverse: Boolean) {

        Observable.just(Booking())
                .doOnNext {
                    it.comment = order.comment ?: ""
                    it.points = order.points?.toMutableList() ?: mutableListOf(SearchPoint(), SearchPoint())
                    if (isReverse) it.points.reverse()
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    bookingRepository.save(it)
                    viewState.openMainFragment()
                }, {
                    handleError(it, true, {})
                })

    }

    fun start() {

        Observable.just(1)
                .doOnSubscribe {
                    viewState.hideViews()
                    viewState.startProgress("Загрузка истории..")
                }
                .flatMap { historyRepository.getFirstPage(SourceType.FIRST).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({
                    viewState.stopProgress()
                    it?.let { viewState.updateAdapter(it) }
                }, {
                    viewState.stopProgress()
                    handleError(it, false, {})
                }).addToComposite(compositeSubscription)

        favoritesRepository.getAll(SourceType.BOTH)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    favIds.clear()
                    it?.asSequence()?.forEach {
                        it.objectId?.let { favIds.add(it) }
                    }
                    viewState.updateAdapter()
                },{
                    it.printStackTrace()
                }).addToComposite(compositeSubscription)

    }

    fun removeOrder(id: String?) {
        id?.let {
            Observable.just(1)
                    .doOnSubscribe {
                        viewState.startProgress("Удаление..")
                    }
                    .flatMap { historyRepository.removeOrder(id).compose(SchedulerUtils.IoMainScheduler()) }
                    .subscribe({
                        viewState.stopProgress()
                        if (it) {
                            viewState.removeOrder(id!!)
                            historyRepository.clear()
                        }
                    }, {
                        viewState.stopProgress()
                        handleError(it, false, {})
                    })
        }
    }

    fun reload() {
        viewState.hideViews()
        viewState.startProgress("Загрузка истории..")
        refresh()
    }

    fun refresh() {
        historyRepository.refreshFirstPage()
                .compose(SchedulerUtils.IoMainScheduler())
                .doOnTerminate {
                    viewState.stopProgress()
                    viewState.stopRefresh()
                }
                .subscribe({
                    it?.let { viewState.updateAdapter(it) }
                }, {
                    handleError(it, false, {})
                })
    }

    fun loadNextPage(page: Int) {
        historyRepository.getPage(page)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    it?.second?.let { viewState.addItems(it) }
                }, {
                    viewState.onLoadItemsFail()
                    handleError(it, false, {})
                })

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
        viewState.onInternetError()
    }
}