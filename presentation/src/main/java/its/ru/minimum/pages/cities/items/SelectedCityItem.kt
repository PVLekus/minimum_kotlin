package its.ru.minimum.pages.cities.items

import android.support.v7.widget.AppCompatTextView
import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import its.ru.minimum.R
import its.ru.minimum.general.bindView
import its.ru.minimum.general.bindViews
import ru.its.domain.model.City

class SelectedCityItem(val city: City): AbstractItem<SelectedCityItem, SelectedCityItem.ViewHolder>() {

    override fun getIdentifier(): Long {
        return (city.hashCode() * 33).toLong()
    }

    override fun getType(): Int = R.id.item_selected_city

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun getLayoutRes(): Int = R.layout.item_selected_city

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as SelectedCityItem

        if (city != other.city) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + city.hashCode()
        return result
    }


    class ViewHolder(view: View): FastAdapter.ViewHolder<SelectedCityItem>(view) {

        private val name by bindView<AppCompatTextView>(R.id.selected_city_text)

        init {
            bindViews(itemView)
        }

        override fun bindView(item: SelectedCityItem, payloads: MutableList<Any>) {
            name.text = item.city.name
        }

        override fun unbindView(item: SelectedCityItem) {
            name.text = null
        }

    }

}