package its.ru.minimum.pages.cities

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.FastAdapter
import its.ru.minimum.R
import its.ru.minimum.general.Resource
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.general.navigation.BackButtonListener
import its.ru.minimum.general.navigation.BaseItemAdapter
import its.ru.minimum.general.navigation.FastAdapterExtension
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.mvp.activity_interfaces.ToolbarView
import its.ru.minimum.pages.cities.items.OtherCityItem
import its.ru.minimum.pages.cities.items.SelectedCityItem
import its.ru.minimum.pages.order.OrderFragment
import kotlinx.android.synthetic.main.fragment_cities.view.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by oleg on 09.11.17.
 */
class CitiesFragment: BaseFragment(), ToolbarView, BackButtonListener {

    private lateinit var adapter: BaseItemAdapter<OtherCityItem>
    private lateinit var headerAdapter: BaseItemAdapter<SelectedCityItem>
    private lateinit var fastAdapter: FastAdapter<*>
    private val viewModel by viewModel<CitiesViewModel>()

    companion object {
        val TAG = "CitiesFragment"
        fun instance() = CitiesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_cities

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneToolbar("", false)
        switchToolbar(false)

        view.toolbar_title.setText(R.string.cities_title)
        view.leftButton.setOnClickListener { activity?.onBackPressed() }

        view.search_city.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) = Unit

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                adapter.filter(p0)
            }
        })

        setAdapter()

        view.swipeRefresh.setOnRefreshListener {
            hideKeyboard()
            view.search_city.setText("")
            viewModel.refresh()
        }

        viewModel.loadingCities.observe(this, Observer {
            when (it) {
                is Resource.LOADING -> view.swipeRefresh.isRefreshing = true
                is Resource.SUCCESS -> it.getOnce {
                    view.swipeRefresh.isRefreshing = false
                    it.default?.let { headerAdapter.update(listOf(SelectedCityItem(it))) }
                    adapter.update(it.list.map { OtherCityItem(it, viewModel) })
                }
                is Resource.ERROR -> it.get {
                    view.swipeRefresh.isRefreshing = false
                    handleError(it, true)
                }
            }
        })
        viewModel.selectingCity.observe(this, Observer {
            when (it) {
                is Resource.LOADING -> startProgress("")
                is Resource.SUCCESS -> it.getOnce {
                    stopProgress()
                    activity?.onBackPressed()
                }
                is Resource.ERROR -> it.get {
                    stopProgress()
                    handleError(it,true)
                }
            }
        })

        view.search_city.requestFocus()

        viewModel.loadContent()

    }

    private fun setAdapter() {

        adapter = BaseItemAdapter()
        headerAdapter = BaseItemAdapter()
        fastAdapter = FastAdapterExtension.createAdapter(headerAdapter, adapter)


        fastAdapter.withOnClickListener { _, _, item, _ ->
            if (item is OtherCityItem) {
                startProgress("Загрузка тарифов..")
                viewModel.selectCity(item.city)
            }
            true
        }

        adapter.itemFilter.withFilterPredicate { item, constraint ->
            if (constraint == null) false
            else(item as? OtherCityItem)?.city?.name?.contains(constraint, true) == true
        }

        view!!.list insertAdapter fastAdapter

    }

    override fun onBackPressed(): Boolean {
        router.newRootScreen(OrderFragment.TAG)
        return true
    }
}