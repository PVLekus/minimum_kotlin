package its.ru.minimum.pages.geocode

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import kotlinx.android.synthetic.main.fragment_geocode.view.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.tileprovider.tilesource.ITileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker
import ru.its.domain.model.SearchPoint
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 14.11.17.
 */
class GeocodeFragment: BaseFragment(), GeocodeView, LocationListener {

    @InjectPresenter lateinit var presenter: GeocodePresenter
    private val tileSource by inject<ITileSource>()

    @ProvidePresenter
    fun providePresenter() : GeocodePresenter = GeocodePresenter(get(), get(), this, get())

    private var marker: Marker? = null
    private var user: Marker? = null
    private var isSearchMyPosition = true

    companion object {
        val TAG = "GeocodeFragment"
        val TAG_FAVORITE = "GeocodeFragment_FAVORITE"
        fun instance(orderPosition: Int): GeocodeFragment {
            return GeocodeFragment().also { it.arguments = Bundle().also { it.putInt(ORDER_POSITION, orderPosition) } }
        }
        fun instance() = GeocodeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_geocode

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.readArguments(arguments)
        tuneToolbar(R.string.geocode_title, false)

        view.map?.let {
            it.setTileSource(tileSource)
            it.setBuiltInZoomControls(true)
            it.setMultiTouchControls(true)

            it.overlays.add(MapEventsOverlay(object : MapEventsReceiver {
                override fun longPressHelper(p: GeoPoint?): Boolean = false

                override fun singleTapConfirmedHelper(p: GeoPoint?): Boolean {
                    replaceMarker(p)
                    view.pointAddress?.text = "Поиск.."
                    view.btnSaveMapPoint?.setOnClickListener {  }
                    presenter.searchPoint(p)
                    return false
                }
            }))

        }

        presenter.loadCityMap()
        presenter.start()

        checkPermission {
            presenter.startListenUserLocation()
        }
    }

    private fun checkPermission(func: ()->Unit) {

        val permissions = arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION)
        val rationale = getString(R.string.location_permission_rationale)
        val options = Permissions.Options()
                .setRationaleDialogTitle(getString(R.string.info))
                .setSettingsDialogTitle(getString(R.string.warning))

        Permissions.check(context, permissions, rationale, options, object: PermissionHandler() {
            override fun onGranted() {
                func.invoke()
            }

            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                showToast(getString(R.string.no_permission))
            }
        })

    }

    override fun dropSearchMyLocation() {
        isSearchMyPosition = false
    }

    override fun replaceMarker(point: GeoPoint?) {

        marker?.let {
            it.position = point
            view?.map?.invalidate()
            return
        }

        marker = Marker(view?.map).also {
            it.position = point
            it.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            it.setIcon(ContextCompat.getDrawable(activity!!, R.drawable.ic_client_placeholder))
        }

        view?.map?.overlays?.add(marker)
        view?.map?.invalidate()

    }

    override fun setZoomPoint(point: GeoPoint, zoom: Int) {
        view?.myLocation?.setOnClickListener {
            view?.map?.controller?.let {
                it.setCenter(point)
                it.zoomTo(zoom)
            }
        }
    }

    override fun setUpUserLocation(point: GeoPoint?) {

        if (isSearchMyPosition) {

            view?.map?.controller?.let {
                it.setCenter(point)
                it.zoomTo(18)
            }
            presenter?.searchPoint(point)
        }

        user?.let {
            it.position = point
            view?.map?.invalidate()
            return
        }

        user = Marker(view?.map).also {
            it.position = point
            it.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER)
            it.setIcon(ContextCompat.getDrawable(activity!!, R.drawable.my_location))
        }

        Observable.interval(1, TimeUnit.SECONDS)
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    view?.map?.overlays?.add(user)
                    view?.map?.invalidate()
                }
    }

    override fun dropViews(result: String) {
        view?.pointAddress?.text = result
        view?.btnSaveMapPoint?.setOnClickListener {  }
    }

    override fun fillViews(point: SearchPoint) {
        view?.pointAddress?.text = "${point.name}, ${point.house}"
        view?.btnSaveMapPoint?.setOnClickListener { presenter.openDetails(point) }
    }

    override fun zoomTo(zoomLevel: Int, geoPoint: GeoPoint) {
        view?.map?.controller?.let {
            it.setZoom(zoomLevel)
            it.setCenter(geoPoint)
        }
    }

}