package its.ru.minimum.pages.details

import android.os.Bundle
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.gson.Gson
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.activities.main.MainActivity.Companion.SEARCH_POINT
import its.ru.minimum.general.DetailsTextWatcher
import its.ru.minimum.general.bindView
import its.ru.minimum.general.inflate
import its.ru.minimum.general.navigation.BackButtonListener
import its.ru.minimum.mvp.BaseFragment
import kotlinx.android.synthetic.main.dialog_time.view.*
import org.jetbrains.anko.AlertDialogBuilder
import org.koin.android.ext.android.get
import ru.its.domain.model.SearchPoint
import ru.its.domain.utils.PickerHelper

/**
 * Created by oleg on 16.11.17.
 */
class DetailsFragment : BaseFragment(), DetailsView, BackButtonListener {

    private val mTimeDetails by bindView<RelativeLayout>(R.id.timeDetails)
    private val mFavDetails by bindView<RelativeLayout>(R.id.favDetails)
    private val mAddressDetails by bindView<RelativeLayout>(R.id.addressDetails)
    private val mHouseDetails by bindView<RelativeLayout>(R.id.houseDetails)
    private val mEnterDetails by bindView<RelativeLayout>(R.id.enterDetails)
    private val mCommentDetails by bindView<RelativeLayout>(R.id.commentDetails)

    private val mCommentText by bindView<AppCompatEditText>(R.id.commentText)
    private val mCommentIcon by bindView<AppCompatImageView>(R.id.imgComment)

    private val mEnterText by bindView<AppCompatEditText>(R.id.enterText)
    private val mEnterIcon by bindView<AppCompatImageView>(R.id.imgEnter)

    private val mHouseText by bindView<AppCompatEditText>(R.id.houseText)
    private val mHouseIcon by bindView<AppCompatImageView>(R.id.imgHouse)

    private val mFavText by bindView<AppCompatEditText>(R.id.favText)
    private val mFavIcon by bindView<AppCompatImageView>(R.id.imgFav)

    private val mAddressText by bindView<AppCompatTextView>(R.id.addressText)
    private val mTimeText by bindView<AppCompatTextView>(R.id.timeText)
    private val mSelectTime by bindView<AppCompatTextView>(R.id.selectTime)

    @InjectPresenter lateinit var presenter: DetailsPresenter

    @ProvidePresenter
    fun providePresenter() : DetailsPresenter = DetailsPresenter(get(),get(),get(), get(), get())

    private var alert: AlertDialogBuilder? = null

    private var houseTextWatcher: DetailsTextWatcher? = null
    private var enterTextWatcher: DetailsTextWatcher? = null
    private var commentTextWatcher: DetailsTextWatcher? = null
    private var favTextWatcher: DetailsTextWatcher? = null

    companion object {
        val TAG = "DetailsFragment"
        val TAG_FAVORITE = "DetailsFragment_FAVORITE"

        fun instance(searchPoint: SearchPoint): DetailsFragment {
            return DetailsFragment().also {
                it.arguments = Bundle().also {
                    it.putString(SEARCH_POINT, Gson().toJson(searchPoint))
                }
            }
        }

        fun instance(orderPosition: Int, searchPoint: SearchPoint): DetailsFragment {
            return DetailsFragment().also {
                it.arguments = Bundle().also {
                    it.putInt(ORDER_POSITION, orderPosition)
                    it.putString(SEARCH_POINT, Gson().toJson(searchPoint))
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.readArguments(arguments)
        presenter.init()
        presenter.checkPreOrder()

    }

    override fun updateView(orderPosition: Int?, searchPoint: SearchPoint?) {

        val title = getString(if (orderPosition == null) R.string.add_fav_title else if (orderPosition == 0) R.string.from_title else R.string.to_title)

        tuneToolbar(title, false)?.let {

            it.findViewById<ImageButton>(R.id.rightButton)?.let {
                it.setImageResource(R.drawable.ic_confirm)
                it.visibility = View.VISIBLE
                it.setOnClickListener { presenter.confirm(mFavText.text.toString(), mHouseText.text.toString(), mEnterText.text.toString(), mCommentText.text.toString()) }
            }
        }

        mAddressDetails.setOnClickListener { presenter.openSearch() }

        mFavDetails.visibility = if (orderPosition != null) View.GONE else View.VISIBLE

        searchPoint?.let {

            mHouseDetails.visibility = if (it.metatype == 0) View.VISIBLE else View.GONE
            mEnterDetails.visibility = if (it.metatype == 0) View.VISIBLE else View.GONE
            mCommentDetails.visibility = if (orderPosition == 0) View.VISIBLE else View.GONE

            mAddressText.text = it.name
        }

        if (mHouseDetails.visibility == View.VISIBLE) {
            houseTextWatcher = DetailsTextWatcher(mHouseIcon, R.mipmap.ic_point_house_inactive, R.mipmap.ic_firstpoint_house_active)
            mHouseText.addTextChangedListener(houseTextWatcher)
            mHouseText.setText(searchPoint?.house ?: "")
        }

        if (mEnterDetails.visibility == View.VISIBLE) {
            enterTextWatcher = DetailsTextWatcher(mEnterIcon, R.mipmap.ic_point_place_inactive, R.mipmap.ic_firstpoint_place_active)
            mEnterText.addTextChangedListener(enterTextWatcher)
            mEnterText.setText(searchPoint?.enter ?: "")

        }

        if (mCommentDetails.visibility == View.VISIBLE) {
            commentTextWatcher = DetailsTextWatcher(mCommentIcon, R.drawable.ic_comment_noactive, R.drawable.ic_comment_active)
            mCommentText.addTextChangedListener(commentTextWatcher)
        }

        if (mFavDetails.visibility == View.VISIBLE) {
            favTextWatcher = DetailsTextWatcher(mFavIcon, R.drawable.ic_favorite, R.drawable.ic_favorite_active)
            mFavText.addTextChangedListener(favTextWatcher)
        }

    }

    override fun showPreOrder(boolean: Boolean) {
        mTimeDetails.visibility = if (boolean && presenter.orderPosition == 0) View.VISIBLE else View.GONE
        if (!boolean || presenter.orderPosition != 0) return

        presenter.searchPoint?.preOrder?.let {
            if (it.isNotBlank()) mTimeText.text = it
        }
        mSelectTime.setOnClickListener {
            val picker = PickerHelper()

            alert?.dismiss()
            alert = AlertDialogBuilder(context!!)
            alert?.customView(layoutInflater.inflate(R.layout.dialog_time, null, false).also { time ->

                val dates = picker.getSortedListOfDates()
                val hours = picker.getSortedListOfHours()
                val minutes = picker.getSortedListOfMinutes()
                var date = mutableListOf<String>(dates[0],hours[0],minutes[0])

                time.p1.data = dates
                time.p2.data = hours
                time.p3.data = minutes

                time.p1.setOnItemSelectedListener { picker, data, position -> date[0] = dates[position] }
                time.p2.setOnItemSelectedListener { picker, data, position -> date[1] = hours[position] }
                time.p3.setOnItemSelectedListener { picker, data, position -> date[2] = minutes[position] }

                time.p2.data = hours
                time.p3.data = minutes

                time.selectDate.setOnClickListener {
                    val time = "${date[0]} ${date[1]}:${date[2]}"
                    mTimeText.text = time
                    presenter.searchPoint?.preOrder = time
                    alert?.dismiss()
                }
                time.selectNow.setOnClickListener {
                    mTimeText.text = "Сейчас"
                    presenter.searchPoint?.preOrder = null
                    alert?.dismiss()
                }

            })
            alert?.show()

        }
    }

    override fun updateComment(comment: String?) {
        mCommentText.setText(comment ?: "")
    }

    override fun onDestroyView() {
        houseTextWatcher?.let { mHouseText.removeTextChangedListener(it) }
        enterTextWatcher?.let { mEnterText.removeTextChangedListener(it) }
        commentTextWatcher?.let { mCommentText.removeTextChangedListener(it) }
        favTextWatcher?.let { mFavText.removeTextChangedListener(it) }
        super.onDestroyView()
    }

    override fun onBackPressed(): Boolean {
        presenter.onBackPressed()
        return true
    }



}