package its.ru.minimum.pages.card.web

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import ru.its.domain.repo.ICardsRepository

/**
 * Created by oleg on 10.03.18.
 */
interface BrowserView: MinimumMvpView {
    fun updateView(url: String)
}

@InjectViewState
class BrowserPresenter(private val cardsRepository: ICardsRepository): BasePresenter<BrowserView>() {

    fun init() {

        viewState.startProgress("Получение данных")

        cardsRepository.addCard()
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    viewState.stopProgress()
                    viewState.updateView(it)
                },{
                    viewState.stopProgress()
                    handleError(it, true, {})
                })

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }
}