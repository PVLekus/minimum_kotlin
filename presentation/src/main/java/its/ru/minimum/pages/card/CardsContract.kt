package its.ru.minimum.pages.card

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import ru.its.domain.repo.ICardsRepository

/**
 * Created by oleg on 10.03.18.
 */
interface CardsView: MinimumMvpView {

    @StateStrategyType(SingleStateStrategy::class)
    fun updateView(cardMask: String?)

    @StateStrategyType(SingleStateStrategy::class)
    fun onLoadCardFailure()

    @StateStrategyType(SkipStrategy::class)
    fun openWeb()

}

@InjectViewState
class CardsPresenter (private val cardsRepository: ICardsRepository): BasePresenter<CardsView>() {

    fun init() {

        viewState.startProgress("Загрузка карт..")

        cardsRepository.getCard()
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    viewState.stopProgress()
                    viewState.updateView(it?.cardMask)
                },{
                    viewState.stopProgress()
                    viewState.onLoadCardFailure()
                    handleError(it, true, {})
                })

    }

    fun editCard() {

        cardsRepository.clear()
        viewState.openWeb()

    }

    fun deleteCard() {

        viewState.startProgress("Удаление карты..")

        cardsRepository.removeCard()
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    cardsRepository.clear()
                    viewState.stopProgress()
                    viewState.updateView(null)
                },{
                    viewState.stopProgress()
                    handleError(it, true, {})
                })
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }
}