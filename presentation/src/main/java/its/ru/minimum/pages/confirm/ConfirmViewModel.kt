package its.ru.minimum.pages.confirm

import android.arch.lifecycle.MutableLiveData
import android.os.Bundle
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AppsFlyerLib
import its.ru.minimum.MinimumApp
import its.ru.minimum.activities.main.MainActivity
import its.ru.minimum.general.Resource
import its.ru.minimum.general.asError
import its.ru.minimum.general.asLoading
import its.ru.minimum.general.asSuccess
import its.ru.minimum.mvvm.BaseViewModel
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.remote.RutaxiAPI
import ru.its.data.repo.PrefHelper
import ru.its.data.utils.addToComposite
import ru.its.domain.exceptions.ViewModelException
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.model.StatusText
import ru.its.domain.repo.IBookingRepository
import ru.its.domain.repo.IUserRepository
import ru.its.domain.utils.PhoneNormalizer
import java.util.*

class ConfirmViewModel(private val bookingRepository: IBookingRepository,
                       private val userRepository: IUserRepository,
                       private val rutaxiAPI: RutaxiAPI,
                       private val prefHelper: PrefHelper): BaseViewModel() {

    val updatingPoints: MutableLiveData<Resource.SUCCESS<List<SearchPoint>>> = MutableLiveData()
    val updatingPreOrder: MutableLiveData<Resource.SUCCESS<String>> = MutableLiveData()
    val updatingComment: MutableLiveData<Resource.SUCCESS<String>> = MutableLiveData()
    val updatingCost: MutableLiveData<Resource.SUCCESS<String>> = MutableLiveData()
    val updatingPhone: MutableLiveData<Resource.SUCCESS<String>> = MutableLiveData()
    val onMakeOrder: MutableLiveData<Resource<Order>> = MutableLiveData()

    val currency by lazy { userRepository.getUser()?.city?.currency ?:"RUB" }
    private var sum = ""

    fun readArguments(args: Bundle?) {
        args?.let {
            sum = it.getString(MainActivity.ORDER_SUM, "")
        }

        updatingCost.value = sum.asSuccess()

        bookingRepository.get()
                .subscribe({
                    updatingPoints.value = it.points.filter { it.id != null }.asSuccess()
                    it.preOrderTime?.let { updatingPreOrder.value = it.asSuccess() }
                    updatingComment.value = it.comment.asSuccess()
                },{
                    handleError(it)
                })

        val rawPhone = userRepository.getUser()?.phone ?: ""
        val phone = PhoneNormalizer().normalize(rawPhone, true)

        updatingPhone.value = phone.asSuccess()
    }

    fun makeOrder(points: List<SearchPoint>?, phone: String?, comment: String?) {

        if (phone?.trim().isNullOrBlank()) {
            onMakeOrder.value = ViewModelException("Введите корректно номер телефона").asError()
            return
        }

        val token = prefHelper.get(PrefHelper.Key.TOKEN, String::class.java)

        bookingRepository.get()
                .doOnSubscribe {
                    onMakeOrder.value = asLoading()
                }
                .flatMap {  rutaxiAPI.makeOrder(it.points.filter { it.id != null }, phone, comment, preorderTime = it.getFormattedPreorderTime(), guid = token).compose(SchedulerUtils.IoMainScheduler()) }
                .subscribe({ id ->
                    bookingRepository.clear()
                    val eventValue = HashMap<String, Any>()
                    eventValue[AFInAppEventParameterName.REVENUE] = 0
                    eventValue[AFInAppEventParameterName.CONTENT_ID] = phone ?: ""
                    AppsFlyerLib.getInstance().trackEvent(MinimumApp.instance, "make_order", eventValue)
                    onMakeOrder.value = Order().also {
                        it.id = id
                        it.points = points
                        it.comment = comment
                        it.status = 2
                        it.statusMessage = StatusText.fromStatusCode(it.status)
                        it.cost = sum
                    }.asSuccess()
                },{
                    it.printStackTrace()
                    onMakeOrder.value = it.asError()
                }).addToComposite(composite)





    }

}