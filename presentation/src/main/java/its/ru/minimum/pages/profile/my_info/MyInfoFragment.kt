package its.ru.minimum.pages.profile.my_info

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.crashlytics.android.Crashlytics
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import kotlinx.android.synthetic.main.fragment_my_info.view.*
import org.jetbrains.anko.toast
import org.koin.android.ext.android.get
import ru.its.domain.utils.PhoneNormalizer

/**
 * Created by oleg on 19.12.17.
 */
class MyInfoFragment : BaseFragment(), MyInfoView {

    @InjectPresenter lateinit var presenter: MyInfoPresenter

    @ProvidePresenter
    fun providePesenter(): MyInfoPresenter = MyInfoPresenter(get())

    companion object {
        val TAG = "MyInfoFragment"
        val PICK_CONTACT = 1090
        fun instance() = MyInfoFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_my_info

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tuneToolbar(R.string.profile_my_info, false)

        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        view?.btnSaveUser?.setOnClickListener { presenter?.save(view.userName?.text?.toString(), view.userPhone?.text?.toString()) }

        view?.phoneBook?.setOnClickListener {
            if (checkPermission("android.permission.READ_CONTACTS")) openContacts()
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PICK_CONTACT)
        }

        presenter.init()

    }

    override fun onSaveSuccess() {
        activity?.toast("Сохранено")
        activity?.onBackPressed()
    }

    override fun updateView(name: String, phone: String) {

        view?.userName?.setText(name)
        view?.userPhone?.setText(PhoneNormalizer().normalize(phone, true))

    }

    private fun openContacts() {

        val uri = Uri.parse("content://contacts")
        val intent = Intent(Intent.ACTION_PICK, uri)
        intent.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(intent, PICK_CONTACT)

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PICK_CONTACT) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) openContacts()
            else activity?.toast("Нет требуемого разрешения на доступ")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {

                activity!!.contentResolver.query(data?.data, null, null, null, null).use { cursor ->
                    if (cursor.moveToFirst()) {
                        if (cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)).toInt() > 0) {
                            cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))?.let {
                                view?.userPhone?.setText(PhoneNormalizer().normalize(it, true))
                            }
                        } else activity?.toast("Не удалось извлечь номер")
                    }
                }

            }
        } catch (e: Exception) {
            e?.printStackTrace()
            Crashlytics.logException(e)
            activity?.toast("Не удалось извлечь номер")
        }
    }

    private fun checkPermission(permission: String): Boolean {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return context?.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        }

        return true

    }

}