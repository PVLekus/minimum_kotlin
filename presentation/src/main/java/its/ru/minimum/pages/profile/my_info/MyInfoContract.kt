package its.ru.minimum.pages.profile.my_info

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.MenuView
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import ru.its.domain.repo.IUserRepository
import ru.its.domain.utils.PhoneNormalizer

/**
 * Created by oleg on 19.12.17.
 */
interface MyInfoView: MinimumMvpView, MenuView {
    fun updateView(name: String, phone: String)
    fun onSaveSuccess()
}

@InjectViewState
class MyInfoPresenter (private val userRepository: IUserRepository): BasePresenter<MyInfoView>() {

    fun init() {
        userRepository.getUser()?.let {
            viewState.updateView(it.name, it.phone ?: "")
        }
    }

    fun save(name: String?, phone: String?) {

        if (name?.trim()?.isNullOrBlank() != false) {
            viewState.showDialog("Введите имя пользователя")
            return
        }

        if (!PhoneNormalizer().validateNumber(phone)) {
            viewState.showDialog("Введите корректно 10ти значный номер мобильного телефона")
            return
        }

        userRepository.run {
            updatePhone(phone!!)
            updateName(name)
        }

        viewState.reloadName()
        viewState.onSaveSuccess()

    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {
    }
}