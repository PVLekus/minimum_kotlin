package its.ru.minimum.pages.card.web

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import kotlinx.android.synthetic.main.fragment_web.view.*
import org.koin.android.ext.android.get

/**
 * Created by oleg on 10.03.18.
 */
class BrowserFragment : BaseFragment(), BrowserView {

    @InjectPresenter lateinit var presenter: BrowserPresenter

    @ProvidePresenter fun providePresenter() = BrowserPresenter(get())

    companion object {
        val TAG = "BrowserFragment"
        fun instance() = BrowserFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = container inflate R.layout.fragment_web

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.webView?.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                getView()?.progress_web?.visibility = View.INVISIBLE
            }
        }
        view.webView.settings.javaScriptEnabled = true
        tuneToolbar("Добавить карту", false)
        presenter.init()
    }

    override fun updateView(url: String) {
        view?.webView?.loadUrl(url)
    }
}