package its.ru.minimum.pages.login

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AppsFlyerLib
import its.ru.minimum.R
import its.ru.minimum.activities.login.LoginActivity.Companion.PHONE
import its.ru.minimum.activities.login.LoginViewModel
import its.ru.minimum.activities.main.MainActivity
import its.ru.minimum.general.Resource
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.utils.DeviceUtils
import kotlinx.android.synthetic.main.fragment_check_code.view.*
import org.jetbrains.anko.startActivity
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.set

/**
 * Created by oleg on 06.11.17.
 */
class CheckCodeFragment: BaseFragment() {

    private val viewModel by viewModel<LoginViewModel>()

    var phone: String = ""

    companion object {
        val TAG = "CheckCodeFragment"
        fun instance(phone: String) : CheckCodeFragment = CheckCodeFragment().also { it. arguments = Bundle().also { it.putString(PHONE, phone) } }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_check_code

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            phone = it.getString(PHONE, "")
        }

        view.text_code?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO){
                viewModel.checkCode(phone, view?.text_code?.text?.toString())
                false
            }
            true
        }

        view.sms_to_number?.text = getString(R.string.sms_to_number, phone)

        view.btnCheck?.setOnClickListener { viewModel.checkCode(phone, view.text_code?.text?.toString()) }

        viewModel.onCheckCode.observe(this, Observer {
            when (it) {
                is Resource.LOADING -> startProgress(it.message)
                is Resource.SUCCESS -> it.getOnce {
                    stopProgress()
                    val eventValue = HashMap<String, Any>()
                    eventValue[AFInAppEventParameterName.REVENUE] = 0
                    eventValue[AFInAppEventParameterName.CONTENT_ID] = phone!!
                    AppsFlyerLib.getInstance().trackEvent(context, "registration", eventValue)
                    activity?.startActivity<MainActivity>()
                    activity?.finish()
                }
                is Resource.ERROR -> it.get {
                    stopProgress()
                    DeviceUtils.showKeyboardForced(view.text_code)
                    handleError(it, false)
                }
            }
        })

    }

    override fun onStart() {
        super.onStart()
        DeviceUtils.showKeyboardForced(view?.text_code)
    }

    override fun onPause() {
        super.onPause()
        DeviceUtils.hideKeyboard(view)
    }

}