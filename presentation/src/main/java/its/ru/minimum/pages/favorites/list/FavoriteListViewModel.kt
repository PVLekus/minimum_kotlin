package its.ru.minimum.pages.favorites.list

import android.arch.lifecycle.MutableLiveData
import android.os.Bundle
import its.ru.minimum.activities.main.MainActivity.Companion.ORDER_POSITION
import its.ru.minimum.general.*
import its.ru.minimum.mvvm.BaseViewModel
import its.ru.minimum.pages.favorites.FavoriteItem
import its.ru.minimum.pages.order.OrderFragment
import its.ru.minimum.pages.search.SearchFragment
import its.ru.minimum.utils.SchedulerUtils
import ru.its.data.utils.addToComposite
import ru.its.domain.model.FavoritePoint
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IBookingRepository
import ru.its.domain.repo.IFavoritesRepository
import ru.its.domain.repo.SourceType
import ru.terrakok.cicerone.Router

class FavoriteListViewModel(private val favoritesRepository: IFavoritesRepository,
                            private val router: Router,
                            private val bookingRepository: IBookingRepository): BaseViewModel() {

    private var orderPosition: Int? = null

    val loadingFavorites = MutableLiveData<Resource<List<FavoriteItem>>>()
    val deletingFavorite = MutableLiveData<Resource<FavoriteItem>>()

    fun readArguments(arg: Bundle?) {

        orderPosition = arg.getIntOrNull(ORDER_POSITION)

        favoritesRepository.getAll(SourceType.BOTH)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    it?.let { loadingFavorites.value = it.map { FavoriteItem(it) }.asSuccess() }
                },{
                    it.printStackTrace()
                    loadingFavorites.value = it.asError()
                }).addToComposite(composite)

    }

    fun refresh() {
        favoritesRepository.getAll(SourceType.NETWORK)
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    it?.let { loadingFavorites.value = it.map { FavoriteItem(it) }.asSuccess() }
                },{
                    it.printStackTrace()
                    loadingFavorites.value = it.asError()
                }).addToComposite(composite)
    }

    fun newFavorite() {
        router.navigateTo(SearchFragment.TAG_FAVORITE)
    }

    fun selectFavorite(favoritePoint: FavoritePoint) {
        if (orderPosition != null) {
            val item = SearchPoint.from(favoritePoint)
            confirm(item)
        } /*else {
            router.navigateTo(DetailsFragment.TAG_FAVORITE, SearchPoint.from(favoritePoint))
        }*/
    }

    fun removePoint(favoritePoint: FavoritePoint) {
        deletingFavorite.value = asLoading()
        favoritesRepository.removeFavorite(favoritePoint)
                .flatMap { favoritesRepository.getAll(SourceType.CACHE) }
                .compose(SchedulerUtils.IoMainScheduler())
                .subscribe({
                    it?.let { deletingFavorite.value = FavoriteItem(favoritePoint).asSuccess() }
                },{
                    it.printStackTrace()
                    deletingFavorite.value = it.asError()
                }).addToComposite(composite)
    }

    private fun confirm(searchPoint: SearchPoint) {

        bookingRepository.updateItemByPosition(searchPoint,orderPosition!!, null)
                .subscribe({
                    router.backTo(OrderFragment.TAG)
                },{
                    handleError(it)
                })
    }
}