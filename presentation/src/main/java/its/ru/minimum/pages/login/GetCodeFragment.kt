package its.ru.minimum.pages.login

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.redmadrobot.inputmask.MaskedTextChangedListener
import its.ru.minimum.R
import its.ru.minimum.activities.login.LoginViewModel
import its.ru.minimum.general.Resource
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.utils.DeviceUtils
import kotlinx.android.synthetic.main.fragment_get_code.view.*
import org.koin.android.viewmodel.ext.android.viewModel


/**
 * Created by oleg on 28.09.17.
 */
class GetCodeFragment: BaseFragment() {

    private val viewModel by viewModel<LoginViewModel>()

    companion object {
        val TAG = "GetCodeFragment"
        fun instance() : GetCodeFragment = GetCodeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_get_code

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listener = MaskedTextChangedListener(
                "+7 ([000]) [000] [00] [00]",
                view.phone,
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(maskFilled: Boolean, extractedValue: String) {
                        println("extractedValue = $extractedValue")
                        println("maskFilled.toString() = ${maskFilled.toString()}")
                    }
                }
        )

        view.phone.addTextChangedListener(listener)
        view.phone.onFocusChangeListener = listener
        view.phone.hint = listener.placeholder()
        view.phone?.setOnEditorActionListener { _, actionId, _ ->

            if (actionId == EditorInfo.IME_ACTION_GO){
                viewModel.getCode(view.phone?.text?.toString())
                false
            }
            true
        }

        view.enterBtn?.setOnClickListener{
            viewModel.getCode(view.phone?.text?.toString())
        }

        viewModel.onGetCode.observe(this, Observer {
            when (it) {
                is Resource.LOADING -> startProgress(it.message)
                is Resource.SUCCESS -> it.getOnce {
                    stopProgress()
                    router.navigateTo(CheckCodeFragment.TAG, it)
                }
                is Resource.ERROR -> it.get {
                    stopProgress()
                    DeviceUtils.showKeyboardForced(view.phone)
                    handleError(it, true)
                }
            }
        })

    }

    override fun onStart() {
        super.onStart()
        DeviceUtils.showKeyboardForced(view?.phone)
    }

    override fun onPause() {
        super.onPause()
        DeviceUtils.hideKeyboard(view)
    }
}