package its.ru.minimum.pages.order.order_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.mikepenz.fastadapter.FastAdapter
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.general.navigation.BaseItemAdapter
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.pages.order.order_details.OrderDetailsFragment
import kotlinx.android.synthetic.main.fragment_list.view.*
import org.koin.android.ext.android.get

/**
 * Created by oleg on 20.11.17.
 */
class OrdersListFragment: MenuFragment(), OrdersListView {

    private lateinit var adapter: BaseItemAdapter<OrderItem>
    private lateinit var fastAdapter: FastAdapter<OrderItem>
    @InjectPresenter lateinit var presenter: OrdersListPresenter

    @ProvidePresenter fun providePresenter(): OrdersListPresenter = OrdersListPresenter(get())

    companion object {
        val TAG = "OrdersListFragment"
        fun instance() = OrdersListFragment()
    }

    override fun getBaseTag(): String = TAG

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_list

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneToolbar(R.string.current_title, true)

        adapter = BaseItemAdapter()
        fastAdapter = FastAdapter.with(adapter)

        fastAdapter.withOnClickListener { _, _, item, _ ->
            router.navigateTo(OrderDetailsFragment.TAG, item.order)
            true
        }

        view.list insertAdapter fastAdapter

        view.empty_list?.setText(R.string.no_orders)
        view.btnReloadList?.setOnClickListener { presenter.reload() }

        view.swipeRefresh?.setOnRefreshListener { presenter.refresh() }

        presenter.init()
    }

    override fun stopRefresh() {
        view?.swipeRefresh?.isRefreshing = false
    }

    override fun updateAdapter(list: List<OrderItem>) {
        view?.empty_list?.visibility = if (list.isEmpty()) View.VISIBLE else View.INVISIBLE
        adapter.update(list)
    }

    override fun hideViews() {
        view?.empty_list?.visibility = View.INVISIBLE
        view?.no_internet_container?.visibility = View.INVISIBLE
    }

    override fun onInternetError() {
        view?.no_internet_container?.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        presenter.startLoad()
    }

    override fun onStop() {
        super.onStop()
        presenter.stopLoad()
    }
}