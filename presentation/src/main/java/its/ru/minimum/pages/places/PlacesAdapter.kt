package its.ru.minimum.pages.places

import android.view.View
import its.ru.minimum.R
import its.ru.minimum.general.BaseVH
import its.ru.minimum.general.SimpleAdapter
import kotlinx.android.synthetic.main.item_search_simple.view.*
import ru.its.domain.model.SearchPoint
import ru.its.domain.model.StatusText

/**
 * Created by oleg on 14.11.17.
 */
class PlacesAdapter(val adapterView: AdapterListener): SimpleAdapter<SearchPoint>(R.layout.item_search_simple) {

    override fun getHolder(view: View): BaseVH<SearchPoint> {

        return object : BaseVH<SearchPoint>(view) {
            override fun fill(item: SearchPoint) {
                view?.setOnClickListener { adapterView.onItemClick(item) }
                view?.itemAddress?.text = item.name
                view?.itemType?.text = if (item.address?.trim().isNullOrEmpty() || item.metatype == 100) StatusText.fromObjectCode(item.metatype) else item.address
            }
        }

    }

    interface AdapterListener {

        fun onItemClick(item: SearchPoint)

    }

}