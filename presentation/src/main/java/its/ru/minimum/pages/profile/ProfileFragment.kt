package its.ru.minimum.pages.profile

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.graphics.drawable.RoundedBitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import its.ru.minimum.R
import its.ru.minimum.activities.start.StartActivity
import its.ru.minimum.general.inflate
import its.ru.minimum.general.makeRound
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.pages.card.CardsFragment
import its.ru.minimum.pages.profile.my_info.MyInfoFragment
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import ru.its.data.repo.PrefHelper
import ru.its.data.utils.ImageSaver
import java.util.*

/**
 * Created by oleg on 22.11.17.
 */
class ProfileFragment : MenuFragment(), ProfileView {

    companion object {

        val TAG = "ProfileFragment"
        val REQUEST_GALLERY = 3421
        fun instance() = ProfileFragment()

    }

    @InjectPresenter lateinit var presenter: ProfilePresenter
    private val prefHelper by inject<PrefHelper>()
    private var alert: AlertDialogBuilder? = null

    @ProvidePresenter
    fun providePresenter(): ProfilePresenter = ProfilePresenter(get(), get())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        switchToolbar(false)
        closeDrawer()

        view?.profileMenu?.setOnClickListener { openDrawer() }

        ImageSaver(activity)
                .setFileName("avatar.jpeg")
                .setDirectoryName("minimum")
                .createFile()?.let {

            Picasso.get()
                    .load(it)
                    .placeholder(R.mipmap.icon_user)
                    .into(view?.profile_avatar, object : Callback {
                        override fun onSuccess() {
                            view?.profile_avatar?.makeRound()
                        }

                        override fun onError(e: Exception?) {

                        }
                    })
        }



        view?.profile_avatar?.setOnClickListener {
            checkPermission {
                openGallery()
            }
        }

        view?.profileExit?.setOnClickListener {
            alert?.dismiss()
            alert = activity!!.alert {
                message(R.string.profile_exit_alert)
                positiveButton(R.string.yes) { presenter?.exitFromProfile() }
                negativeButton(R.string.no) { dismiss() }
            }.show()
        }

        view?.autoLocationField?.setOnClickListener {
            alert?.dismiss()
            val autoLocation = prefHelper.get(PrefHelper.Key.AUTO_LOCATION, Boolean::class.java) ?: false
            alert = activity!!.alert {
                message(if (autoLocation == true) "Данная функция включена" else "Данная функция отключена")
                positiveButton(if (autoLocation == true) R.string.disable else R.string.enable) {
                    prefHelper.put(PrefHelper.Key.AUTO_LOCATION, !autoLocation)
                    dismiss()
                }
            }.show()
        }

        view?.payField?.setOnClickListener { router.navigateTo(CardsFragment.TAG) }

        view?.myInfoField?.setOnClickListener { router.navigateTo(MyInfoFragment.TAG) }

    }

    private fun checkPermission(func: ()->Unit) {

        val permissions = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val rationale = getString(R.string.photo_permission_rationale)
        val options = Permissions.Options()
                .setRationaleDialogTitle(getString(R.string.info))
                .setSettingsDialogTitle(getString(R.string.warning))

        Permissions.check(context, permissions, rationale, options, object: PermissionHandler() {
            override fun onGranted() {
                func.invoke()
            }
            override fun onDenied(context: Context?, deniedPermissions: ArrayList<String>?) {
                super.onDenied(context, deniedPermissions)
                showToast(getString(R.string.no_permission))
            }
        })

    }

    override fun getBaseTag(): String = TAG

    override fun exit() {

        ImageSaver(activity)
                .setFileName("avatar.jpeg")
                .setDirectoryName("minimum")
                .deleteImage()
        activity?.startActivity<StartActivity>()
        activity?.finish()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_GALLERY && resultCode == Activity.RESULT_OK && data != null) {

            Picasso.get()
                    .load(data.data.toString())
                    .centerCrop()
                    .resize(300, 300)
                    .into(view?.profile_avatar, object : Callback {

                        override fun onSuccess() {
                            view?.profile_avatar?.makeRound()

                            ImageSaver(activity)
                                    .setFileName("avatar.jpeg").
                                    setDirectoryName("minimum").
                                    save((view?.profile_avatar?.drawable as RoundedBitmapDrawable).bitmap)
                            reloadAvatar()

                        }

                        override fun onError(e: Exception?) {
                            e?.printStackTrace()
                        }
                    })


        }

    }

    private fun openGallery() {
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, REQUEST_GALLERY)
    }

    override fun updateUserName(name: String) {
        profile_user_name.text = name
    }

    override fun onPause() {
        super.onPause()
        alert?.dismiss()
    }

    override fun onResume() {
        super.onResume()
        presenter.init()
    }

}