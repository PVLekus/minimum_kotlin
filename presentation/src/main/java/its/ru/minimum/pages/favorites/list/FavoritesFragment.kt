package its.ru.minimum.pages.favorites.list

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import its.ru.minimum.R
import its.ru.minimum.activities.main.MainActivity
import its.ru.minimum.general.Resource
import its.ru.minimum.general.bindView
import its.ru.minimum.general.inflate
import its.ru.minimum.general.insertAdapter
import its.ru.minimum.general.navigation.BaseItemAdapter
import its.ru.minimum.general.navigation.MenuFragment
import its.ru.minimum.pages.favorites.FavoriteItem
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.koin.android.viewmodel.ext.android.viewModel

class FavoritesFragment : MenuFragment() {

    private val mList by bindView<RecyclerView>(R.id.list)
    private val mAddFavorite by bindView<FloatingActionButton>(R.id.btn_add)
    private val mRefresh by bindView<SwipeRefreshLayout>(R.id.swipeRefresh)

    private lateinit var adapter: BaseItemAdapter<FavoriteItem>
    private lateinit var fastAdapter: FastAdapter<FavoriteItem>
    private val viewModel by viewModel<FavoriteListViewModel>()

    private var alert: AlertDialogBuilder? = null

    override fun getBaseTag(): String = TAG

    companion object {
        val TAG = "FavoritesFragment"
        val TAG_ORDER = "FavoritesFragment_order"

        fun instance() = FavoritesFragment()

        fun instance(orderPosition: Int): FavoritesFragment {
            return FavoritesFragment().also {
                it.arguments = Bundle().also { it.putInt(MainActivity.ORDER_POSITION, orderPosition) }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = container inflate R.layout.fragment_favorites

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneToolbar(getString(R.string.fav_title), arguments?.containsKey(MainActivity.ORDER_POSITION) != true)

        setAdapter()

        viewModel.loadingFavorites.observe(this, Observer {
            when (it) {
                is Resource.SUCCESS -> it.getOnce {
                    adapter.update(it)
                    mRefresh.isRefreshing = false
                }
                is Resource.ERROR -> it.get {
                    mRefresh.isRefreshing = false
                    handleError(it, false)
                }
            }
        })

        viewModel.deletingFavorite.observe(this, Observer {
            when (it) {
                is Resource.LOADING -> startProgress("")
                is Resource.SUCCESS -> it.getOnce {
                    stopProgress()
                    adapter.removeItem(it)
                }
                is Resource.ERROR -> {
                    it.get {
                        stopProgress()
                        handleError(it, false)
                    }
                }
            }
        })

        mAddFavorite.setOnClickListener { viewModel.newFavorite() }

        mRefresh.setOnRefreshListener { viewModel.refresh() }

        viewModel.readArguments(arguments)
    }

    private fun setAdapter() {

        adapter = BaseItemAdapter()
        fastAdapter = FastAdapter.with(adapter)

        fastAdapter.withOnClickListener { _, _, item, _ ->
            viewModel.selectFavorite(item.favoritePoint)
            true
        }

        fastAdapter.withEventHook(object : ClickEventHook<FavoriteItem>() {

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return if (viewHolder is FavoriteItem.ViewHolder) {
                    viewHolder.delete
                } else null
            }

            override fun onClick(v: View, position: Int, fastAdapter: FastAdapter<FavoriteItem>, item: FavoriteItem) {
                alert?.dismiss()
                alert = activity!!.alert {
                    message(getString(R.string.msg_confirm_remove))
                    positiveButton(R.string.yes) {
                        viewModel.removePoint(item.favoritePoint)
                        alert?.dismiss()
                    }
                    negativeButton(R.string.no) {
                        alert?.dismiss()
                    }
                }.show()
            }
        })

        mList insertAdapter fastAdapter

    }

    override fun onPause() {
        super.onPause()
        alert?.dismiss()
    }

}