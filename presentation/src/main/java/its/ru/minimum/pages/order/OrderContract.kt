package its.ru.minimum.pages.order

import android.content.DialogInterface
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import its.ru.minimum.BuildConfig
import its.ru.minimum.MinimumApp
import its.ru.minimum.activities.main.MainPresenter
import its.ru.minimum.general.isEmptyOrNull
import its.ru.minimum.mvp.BasePresenter
import its.ru.minimum.mvp.activity_interfaces.CallView
import its.ru.minimum.mvp.activity_interfaces.LocationListener
import its.ru.minimum.mvp.activity_interfaces.MinimumMvpView
import its.ru.minimum.utils.SchedulerUtils
import org.jetbrains.anko.toast
import ru.its.data.remote.RutaxiAPI
import ru.its.data.repo.PrefHelper
import ru.its.data.usecase.CitySelectUseCase
import ru.its.data.utils.LocationParser
import ru.its.data.utils.addToComposite
import ru.its.domain.model.City
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.*
import rx.Observable
import rx.Observer
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 15.11.17.
 */
interface OrderView: MinimumMvpView, CallView {

    @StateStrategyType(SkipStrategy::class)
    fun onGetCost()

    @StateStrategyType(SkipStrategy::class)
    fun onGetCostSuccess(cost: String)

    @StateStrategyType(SingleStateStrategy::class)
    fun onFindActiveOrders(list: List<Order>)

    @StateStrategyType(SingleStateStrategy::class)
    fun onFindCloserCity(city: City)

    @StateStrategyType(SkipStrategy::class)
    fun onChangeCity(city: City)

    @StateStrategyType(SingleStateStrategy::class)
    fun updateAdapter(list: List<SearchPoint>)

    @StateStrategyType(SingleStateStrategy::class)
    fun tuneToolbar(title: String?)

    @StateStrategyType(AddToEndStrategy::class)
    fun updateCurrency(currency: String)
}

@InjectViewState
class OrderPresenter(private val userRepository: IUserRepository,
                     private val bookingRepository: IBookingRepository,
                     private val citiesRepository: ICitiesRepository,
                     private val citySelectUseCase: CitySelectUseCase,
                     private val rutaxiAPI: RutaxiAPI,
                     private val prefHelper: PrefHelper,
                     private val ordersRepository: IOrdersRepository,
                     private val locationListener: LocationListener): BasePresenter<OrderView>() {

    private var ordersSubscription: Subscription? = null
    var currency = "RUB"

    fun makeCallToOperator() {
        userRepository.getUser()?.city?.rate?.params?.operatorPhone?.let {
            viewState.makeCall(it)
        } ?: viewState.showToast("Ошибка получения номера оператора")
    }

    fun tuneToolbar() {
        viewState.tuneToolbar(userRepository.getUser()?.city?.name)
    }

    fun getCost(list: List<SearchPoint>) {

        viewState.onGetCost()
        list.filter { it.id != null }?.let {

            if (it.count() < 2) {
                viewState.onGetCostSuccess("")
                return@let
            }

            rutaxiAPI
                    .getCost(it)
                    .compose(SchedulerUtils.IoMainScheduler())
                    .subscribe({
                        viewState.onGetCostSuccess(it)
                    },{
                        viewState.onGetCostSuccess("Ошибка")
                        handleError(it, true, {})
                    })

        }
    }

    fun deleteItemAtPosition(position: Int) {

        bookingRepository.get()
                .flatMap {
                    if (it.points.size < 3) bookingRepository.clearItemByPosition(position)
                    else bookingRepository.deleteItemByPosition(position)
                }
                .subscribe(OrderObserver())
    }

    fun start() {

        userRepository.getUser()?.city?.let {
            currency = it.currency
            viewState.updateCurrency(it.currency)
        }

        bookingRepository.get().map { it.points }
                .subscribe(OrderObserver())

        stopLoad()

        val time = MinimumApp.instance.firebaseRemoteConfig.getLong("time_orders_main")

        ordersSubscription = Observable.interval(0, time, TimeUnit.SECONDS)
                .flatMap { ordersRepository.getAll(SourceType.NETWORK) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказы не получены")
                    handleError(it, true, {}, false )
                }
                .onErrorReturn { null }
                .subscribe {
                    if (it != null) {
                        if (BuildConfig.DEBUG) MinimumApp.instance?.toast("Заказы получены")
                        viewState.onFindActiveOrders(it)
                    }
                }
    }

    fun stopLoad() {
        if (ordersSubscription?.isUnsubscribed == false) ordersSubscription?.unsubscribe()
    }

    fun changeCity(city: City) {
        onDestroy()
        viewState.startProgress("Загрузка тарифа..")
        executeUseCase(citySelectUseCase, city, {
            viewState.stopProgress()
            viewState.onChangeCity(city)
            MainPresenter.cityChangeSubject.onNext(city)
            start()
        },{
            viewState.stopProgress()
            start()
            handleError(it, true, {})
        })

    }

    fun checkCity() {

        if (prefHelper.get(PrefHelper.Key.AUTO_LOCATION, Boolean::class.java) == false) return
        if (prefHelper.get(PrefHelper.Key.AUTO_LOCATION_TEMP, Boolean::class.java) == false) return

        locationListener.startListenLocation(1)
                ?.filter { it != null }
                ?.take(1)
                ?.subscribe({ user ->

                    val parser = LocationParser()

                    citiesRepository.getAll(SourceType.CACHE)
                            .filter { !it.isEmptyOrNull() }
                            .flatMap { Observable.from(it) }
                            .flatMap { city -> parser.parseToLocation(city).map { Pair(it, city) } }
                            .toList()
                            .map { it.minBy { user!!.distanceTo(it.first) } }
                            .compose(SchedulerUtils.IoMainScheduler())
                            .subscribe({
                                it?.let { if (userRepository.getUser()?.city?.id != it.second.id) viewState.onFindCloserCity(it.second) }
                            },{
                                handleError(it, true, {}, false)
                            })

                },{
                    handleError(it, true, {}, false)
                })?.addToComposite(compositeSubscription)
    }



    fun addPoint() {
        bookingRepository.addNewPoint()
                .subscribe(OrderObserver())
    }

    fun moveItemUp(position: Int) {

        bookingRepository.dicreaseItemPosition(position)
                .subscribe(OrderObserver())
    }

    fun moveItemDown(position: Int) {

        bookingRepository.increaseItemPosition(position)
                .subscribe(OrderObserver())
    }

    override fun onInternetConnectionException(action: DialogInterface.() -> Unit) {

    }

    private inner class OrderObserver: Observer<List<SearchPoint>> {
        override fun onNext(t: List<SearchPoint>) {
            viewState.updateAdapter(t)
        }

        override fun onCompleted() = Unit

        override fun onError(e: Throwable?) {
            handleError(e, true, {})
        }
    }

}