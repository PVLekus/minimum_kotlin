package its.ru.minimum.pages.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import its.ru.minimum.R
import its.ru.minimum.general.BaseAdapter
import its.ru.minimum.general.BaseVH
import kotlinx.android.synthetic.main.item_search_select.view.*
import kotlinx.android.synthetic.main.item_search_separator.view.*
import kotlinx.android.synthetic.main.item_search_simple.view.*
import ru.its.domain.model.PointType
import ru.its.domain.model.SearchPointWrapper
import ru.its.domain.model.StatusText

/**
 * Created by oleg on 13.11.17.
 */
class SearchAdapter(val adapterView: AdapterListener) : BaseAdapter<SearchPointWrapper>() {

    companion object {
        val TYPE_SEPARATOR = 3
        val TYPE_POINT = 6
        val TYPE_HISTORY = 9
        val TYPE_ITEM = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH<SearchPointWrapper> {

        return when (viewType) {

            TYPE_SEPARATOR -> SearchSeparatorVH(LayoutInflater.from(parent.context).inflate(R.layout.item_search_separator, parent, false))
            TYPE_POINT -> SearchPointVH(LayoutInflater.from(parent.context).inflate(R.layout.item_search_simple, parent, false))
            TYPE_HISTORY -> HistoryPointVH(LayoutInflater.from(parent.context).inflate(R.layout.item_search_simple, parent, false))
            TYPE_ITEM -> SearchSelectVH(LayoutInflater.from(parent.context).inflate(R.layout.item_search_select, parent, false))
            else -> SearchSeparatorVH(LayoutInflater.from(parent.context).inflate(R.layout.item_search_separator, parent, false))

        }

    }

    override fun getItemViewType(position: Int): Int = list[position].type


    inner class SearchPointVH (private val view: View) : BaseVH<SearchPointWrapper>(view) {

        override fun fill(item: SearchPointWrapper) {
            view?.setOnClickListener { adapterView.onItemClick(item, itemViewType) }
            view?.itemAddress?.text = item.point?.name
            view?.itemType?.text = if (item.point?.address?.trim().isNullOrEmpty() || item.point?.metatype == 100) StatusText.fromObjectCode(item.point?.metatype) else item.point?.address
        }
    }

    inner class HistoryPointVH (private val view: View) : BaseVH<SearchPointWrapper>(view) {

        override fun fill(item: SearchPointWrapper) {
            view?.setOnClickListener { adapterView.onItemClick(item, itemViewType) }
            view?.itemAddress?.text = item.point?.address
            view?.itemType?.text = StatusText.fromObjectCode(item?.point?.metatype)
        }
    }

    inner class SearchSelectVH (private val view: View) : BaseVH<SearchPointWrapper>(view) {

        override fun fill(item: SearchPointWrapper) {
            view?.setOnClickListener { adapterView.onItemClick(item, itemViewType) }
            view?.typeIcon?.setImageResource( when(item.typeSelect){

                PointType.AIRPORTS -> R.drawable.ic_plane
                PointType.STATIONS -> R.drawable.ic_train
                PointType.POPULARS -> R.mipmap.ic_point_inactive
                PointType.FAVORITES -> R.drawable.ic_favorite
            })

            view?.typeText?.text = item.name
        }
    }

    inner class SearchSeparatorVH (private val view: View) : BaseVH<SearchPointWrapper>(view) {

        override fun fill(item: SearchPointWrapper) {

            view?.separatorText?.text = item.name ?: ""
            view?.separatorLine?.visibility = if (item.name.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE

        }
    }

    interface AdapterListener {
        fun hideViews()
        fun onItemClick(item: SearchPointWrapper, type: Int)
    }

}