package its.ru.minimum.pages.card

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import its.ru.minimum.R
import its.ru.minimum.general.inflate
import its.ru.minimum.mvp.BaseFragment
import its.ru.minimum.pages.card.web.BrowserFragment
import kotlinx.android.synthetic.main.fragment_cards.view.*
import org.koin.android.ext.android.get

/**
 * Created by oleg on 10.03.18.
 */
class CardsFragment: BaseFragment(), CardsView {

    @InjectPresenter lateinit var presenter: CardsPresenter

    @ProvidePresenter fun providePresenter() = CardsPresenter(get())

    private var popUpMenu: PopupMenu? = null

    companion object {
        private val AGENSY_PHONE = "83422403856"
        private val AGENSY_SITE = "https://autopays.ru/oferta-rutaxi.html"
        val TAG = "CardsFragment"
        fun instance() = CardsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = container inflate R.layout.fragment_cards

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneToolbar("Карты", false)

        view.text_call_to_agency?.setOnClickListener { makeCall(AGENSY_PHONE) }
        view.text_user_agreement?.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(AGENSY_SITE)))
        }

    }

    override fun openWeb() {
        router.navigateTo(BrowserFragment.TAG)
    }

    override fun onLoadCardFailure() {
        view?.text_card?.visibility = View.VISIBLE
        view?.image_card?.visibility = View.VISIBLE
        view?.image_card?.setImageDrawable(ContextCompat.getDrawable(activity!!, R.mipmap.ic_appbar_refresh))
        view?.text_card?.text = "Ошибка загрузки"

        view?.text_card?.setOnClickListener {
            view?.progress_card?.visibility = View.VISIBLE
            view?.text_card?.visibility = View.INVISIBLE
            view?.image_card?.visibility = View.INVISIBLE
        }

    }

    override fun onResume() {
        super.onResume()
        presenter.init()
    }

    override fun updateView(cardMask: String?) {

        view?.progress_card?.visibility = View.INVISIBLE

        view?.text_card?.visibility = View.VISIBLE
        view?.image_card?.visibility = View.VISIBLE

        if (cardMask == null) {

            view?.image_card?.setImageDrawable(ContextCompat.getDrawable(activity!!, R.mipmap.ic_card_green))
            view?.text_card?.text = "Добавить карту"
            view?.image_more?.visibility = View.INVISIBLE
            view?.text_card?.setOnClickListener { presenter.editCard() }

        } else {

            view?.image_card?.setImageDrawable(ContextCompat.getDrawable(activity!!, R.mipmap.ic_card_green))
            view?.text_card?.text = cardMask
            view?.image_more?.visibility = View.VISIBLE
            view?.text_card?.setOnClickListener {

                popUpMenu?.dismiss()
                popUpMenu = PopupMenu(view?.context, view?.image_more)
                        .also {
                            it.inflate(R.menu.card_menu)
                        }.also {

                    it.setOnMenuItemClickListener {

                        when (it.itemId) {

                            R.id.card_delete -> presenter.deleteCard().let { true }

                            R.id.card_edit -> presenter.editCard().let { true }

                            else -> false
                        }
                    }

                }
                popUpMenu?.show()

            }

        }

    }
}