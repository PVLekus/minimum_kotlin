package its.ru.minimum.pages.favorites

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import its.ru.minimum.R
import its.ru.minimum.general.bindView
import its.ru.minimum.general.bindViews
import ru.its.data.utils.AddressBuilder
import ru.its.domain.model.FavoritePoint

class FavoriteItem(val favoritePoint: FavoritePoint): AbstractItem<FavoriteItem, FavoriteItem.ViewHolder>() {

    override fun getIdentifier(): Long {
        return (favoritePoint.hashCode() * 33).toLong()
    }

    private val builder = AddressBuilder()

    fun getAddress(): String = builder.build(favoritePoint) ?: ""

    override fun getType(): Int = R.id.item_favorite

    override fun getViewHolder(v: View): ViewHolder = ViewHolder(v)

    override fun getLayoutRes(): Int = R.layout.item_favorite

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as FavoriteItem

        if (favoritePoint != other.favoritePoint) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + favoritePoint.hashCode()
        return result
    }


    class ViewHolder(view: View): FastAdapter.ViewHolder<FavoriteItem>(view) {

        private val name by bindView<AppCompatTextView>(R.id.name)
        private val address by bindView<AppCompatTextView>(R.id.address)
        val delete by bindView<AppCompatImageView>(R.id.ic_delete)

        init {
            bindViews(itemView)
        }

        override fun unbindView(item: FavoriteItem) {

        }

        override fun bindView(item: FavoriteItem, payloads: MutableList<Any>) {
            name.text = item.favoritePoint.name
            address.text = item.getAddress()
        }
    }

}