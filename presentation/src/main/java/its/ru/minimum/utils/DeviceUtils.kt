package its.ru.minimum.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

class DeviceUtils {

    companion object {

        fun showKeyboardForced(view: View?) {

            view?.requestFocus()
            val imManager = view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imManager?.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
            imManager?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

        }

        fun hideKeyboard(view: View?) {
            val imManager = view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imManager?.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

}