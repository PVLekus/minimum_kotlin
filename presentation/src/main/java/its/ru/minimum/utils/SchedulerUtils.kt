package its.ru.minimum.utils

import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by oleg on 05.03.18.
 */
class SchedulerUtils {

    companion object {

        fun <T>IoMainScheduler(): Observable.Transformer<T,T> {
            return Observable.Transformer { it.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()) }
        }

    }

}