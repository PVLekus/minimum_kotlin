package its.ru.minimum.mvvm

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import its.ru.minimum.general.Resource
import its.ru.minimum.utils.SchedulerUtils
import ru.its.domain.usecase.UseCase
import rx.subscriptions.CompositeSubscription

abstract class BaseViewModel: ViewModel() {

    val errorHappen: MutableLiveData<Resource.ERROR<Throwable>> = MutableLiveData()

    protected val composite = CompositeSubscription()

    fun handleError(throwable: Throwable) {
        errorHappen.value = Resource.ERROR(throwable)
    }

    fun <Result, Params> executeUseCase(useCase: UseCase<Result, Params>, params: Params, onNext: (t: Result) -> Unit, onError: (e: Throwable) -> Unit) {
        useCase.createObservable(params).compose(SchedulerUtils.IoMainScheduler()).subscribe(onNext, onError)
    }

    override fun onCleared() {
        super.onCleared()
        composite.clear()
    }

}

data class ProgressInfo(
        val show: Boolean,
        val message: String = ""
)