package its.ru.minimum.mvvm

import android.content.DialogInterface
import android.util.Log
import android.view.inputmethod.InputMethodManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.crashlytics.android.Crashlytics
import its.ru.minimum.R
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AlertDialogBuilder
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.koin.android.ext.android.inject
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.exceptions.InternetConnectionException
import ru.its.domain.exceptions.ViewModelException
import ru.its.domain.model.StatusText

open class BaseActivity: MvpAppCompatActivity() {

    protected var dialog: AlertDialogBuilder? = null
    private val manager by inject<InputMethodManager>()

    fun handleError(throwable: Throwable?, silent: Boolean, show: Boolean = true) {

        var message = StatusText.fromErrorCode(null)

        when(throwable) {
            is InternetConnectionException -> {
                message = StatusText.internetError()
                if (silent && show) toast(message)
            }
            is ApiResponseException -> {
                message = throwable.message ?: StatusText.fromErrorCode(null)
                if (silent && show) toast(message)
                else if (!silent && show) showDialog(message)
            }
            is ViewModelException -> {
                message = throwable.message ?: return
                if (silent && show) toast(message)
                else if (!silent && show) showDialog(message)
            }
            else -> {
                Crashlytics.log(Log.ERROR, "BaseActivity", "info \n${throwable?.message}")
                Crashlytics.logException(throwable)
                if (silent && show) toast(message)
                else if (silent && show) showDialog(message)
            }
        }

    }

    fun hideKeyboard() {
        manager.hideSoftInputFromWindow(fragment_container.windowToken, 0)
    }

    fun showDialog(message: String) {
        dialog?.dismiss()
        dialog = alert {
            message(message)
            cancellable(false)
            positiveButton(R.string.ok) { dismiss() }
        }.show()
    }

    fun showRepeatDialog(message: Int, positiveText: Int, callback: DialogInterface.() -> Unit) {

        dialog?.dismiss()
        dialog = alert {
            message(message)
            cancellable(false)
            positiveButton(positiveText, callback)
            negativeButton(R.string.close) {
                dismiss()
                finish()
            }
        }
                .show()

    }

}