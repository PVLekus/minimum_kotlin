package its.ru.minimum

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals
import ru.its.domain.model.Booking
import java.text.SimpleDateFormat
import java.util.*

object Preordertest: Spek({

    describe("a booking with preorder") {

        val booking = Booking()

        on("set time with Сегодня") {

            booking.preOrderTime = "Сегодня 23:50"

            it("should be today date") {
                assertEquals("${SimpleDateFormat("dd.MM.yyyy").format(Date())} 23:50", booking.getFormattedPreorderTime())
            }

        }

        on("set time with Завтра") {

            booking.preOrderTime = "Завтра 23:50"

            it("should be today date") {
                assertEquals("${SimpleDateFormat("dd.MM.yyyy").format(Date(Date().time + 86_400_000))} 23:50", booking.getFormattedPreorderTime())
            }

        }

    }

})