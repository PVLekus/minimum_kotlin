package its.ru.minimum

import ru.its.domain.utils.PhoneNormalizer
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals

/**
 * Created by oleg on 06.11.17.
 */
object PhoneNormalizerTest : Spek({

    describe("a PhoneNormalizer") {

        val normalizer = PhoneNormalizer()

        on("normalize phone +7 (927) 926-45-44 without clear prefix") {

            val normal_phone = normalizer.normalize("+7 (927) 926-45-44", false)

            it("normal_phone should be +79279264544") {

                assertEquals("+79279264544", normal_phone)

            }

        }

        on("normalize phone +7 (927) 926-45-44 clear prefix") {

            val normal_phone = normalizer.normalize("+7 (927) 926-45-44", true)

            it("normal_phone should be 9279264544") {

                assertEquals("9279264544", normal_phone)

            }

        }

        on("normalize phone 8 (927) 926-45-44 clear prefix") {

            val normal_phone = normalizer.normalize("8 (927) 926-45-44", true)

            it("normal_phone should be 9279264544") {

                assertEquals("9279264544", normal_phone)

            }

        }

    }

})