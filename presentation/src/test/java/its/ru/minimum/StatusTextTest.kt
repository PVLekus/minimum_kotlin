package its.ru.minimum

import ru.its.domain.model.StatusText
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert

/**
 * Created by oleg on 20.10.17.
 */
object StatusTextTest: Spek({

    describe("a StatusText") {

        on("get error text with unknown code") {

            val errorText = StatusText.fromErrorCode(1234)

            it("should return text of constant UNKNOWN_ERROR") {
                Assert.assertEquals(StatusText.UNKNOWN_ERROR, errorText)
            }
        }

        on("get error text with NULL code") {

            val errorText = StatusText.fromErrorCode(null)

            it("should return text of constant UNKNOWN_ERROR") {
                Assert.assertEquals(StatusText.UNKNOWN_ERROR, errorText)
            }
        }

        on("get status text with unknown code") {

            val errorText = StatusText.fromStatusCode(1234)

            it("should return text of constant STATUS_NONAME") {
                Assert.assertEquals(StatusText.STATUS_NONAME, errorText)
            }
        }

        on("get status text with NULL code") {

            val errorText = StatusText.fromStatusCode(null)

            it("should return text of constant STATUS_NONAME") {
                Assert.assertEquals(StatusText.STATUS_NONAME, errorText)
            }
        }

        on("get object text with unknown code") {

            val errorText = StatusText.fromObjectCode(1234)

            it("should return text of constant OBJECT_TYPE_OTHER") {
                Assert.assertEquals(StatusText.OBJECT_TYPE_OTHER, errorText)
            }
        }

        on("get object text with NULL code") {

            val errorText = StatusText.fromObjectCode(null)

            it("should return text of constant OBJECT_TYPE_OTHER") {
                Assert.assertEquals(StatusText.OBJECT_TYPE_OTHER, errorText)
            }
        }

    }

})