package its.ru.minimum

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import ru.its.domain.model.SearchPoint
import ru.its.data.utils.JsonMaker
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals

/**
 * Created by oleg on 22.10.17.
 */
object JsonMakerTest : Spek({

    given("a JsonMaker") {

        var maker = JsonMaker()

        beforeEachTest { maker = JsonMaker() }

        on("maker addProperty name with type String") {

            maker.addProperty("name", "name")

            it("result = {\"name\":\"name\"}") {
                assertEquals("{\"name\":\"name\"}", maker.make().toString())
            }

        }

        on("maker twice addProperty name with type String") {

            maker.addProperty("name", "name")
            maker.addProperty("name", "surname")

            it("in result last property = {\"name\":\"surname\"}") {
                assertEquals("{\"name\":\"surname\"}", maker.make().toString())
            }

        }

        on("add to maker orderObject") {

            maker.addElement("Order", JsonObject())


            var resultObject = JsonParser().parse(maker.make().toString()).asJsonObject

            it("should has jsonObject Order") {

                assertEquals(true, resultObject.has("Order"))

            }

        }


    }

    given("a JsonMaker and List<SearchPoint>") {

        var maker = JsonMaker()

        var firstPoint = SearchPoint()
        firstPoint.id = 234
        firstPoint.metatype = 100

        var secondPoint = SearchPoint()
        secondPoint.id = 23
        secondPoint.house = "22"
        secondPoint.enter = ""
        secondPoint.metatype = 0

        var list = listOf(firstPoint, secondPoint)

        on("createOrderJsonObject from List<SearchPoint>") {

            val orderObject = maker.createOrderJsonObject(list)

            it("orderObject should have jsonArray with count 2") {
                assertEquals(2, orderObject.getAsJsonArray("points").count())
            }

            val arrayItem = orderObject.getAsJsonArray("points").get(1).asJsonObject

            it("jsonArray has last SearchPoint with house = 22") {
                assertEquals("22", arrayItem.get("house").asString)
            }


        }


    }

})