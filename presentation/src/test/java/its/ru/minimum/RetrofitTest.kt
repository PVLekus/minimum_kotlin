package its.ru.minimum

import okhttp3.OkHttpClient
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.mockito.Mockito
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.its.data.remote.IRutaxiAPI
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.model.City
import ru.its.domain.model.StatusText
import ru.its.domain.model.User
import ru.its.domain.repo.IUserRepository
import rx.observers.TestSubscriber
import java.io.File

/**
 * Created by oleg on 29.10.17.
 */
object RetrofitTest: Spek({

    describe("a test API client") {

        var testSubscriber = TestSubscriber<Any>()
        val path = "/Users/oleg/android/minimum_kotlin/presentation/src/main/assets/"

        var mockWebServer: MockWebServer? = null
        var s: String = ""
        var user: User? = null

        var retrofit: RutaxiAPI? = null


        beforeGroup {
            user = User()
            mockWebServer = MockWebServer()
            s = mockWebServer?.url("/").toString()

            val service = Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(OkHttpClient())
                    .baseUrl(s)
                    .build()
                    .create(IRutaxiAPI::class.java)

            retrofit = RutaxiAPI(Mockito.mock(IUserRepository::class.java), service, false)
        }

        beforeEachTest { testSubscriber = TestSubscriber() }

        afterGroup { mockWebServer?.shutdown() }

        on("set dispatcher for getCities() with wrong json") {

            mockWebServer?.setDispatcher(object : Dispatcher() {
                override fun dispatch(request: RecordedRequest?): MockResponse {
                    if (request?.path == "/cities/") {
                        return MockResponse().setResponseCode(200).setBody("{\"phonetype\":\"N95\",\"cat\":\"WP\"}")
                    }
                    return MockResponse().setResponseCode(404)
                }
            })

            retrofit!!
                    .getCities()
                    .toBlocking()
                    .subscribe(testSubscriber)

            it("testSubscriber should return ApiResponseException") {

                testSubscriber.assertError(ApiResponseException::class.java)

            }

            it("ApiResponseException message should be like StatusText.fromErrorCode(null)") {

                var exception = testSubscriber.onErrorEvents[0]

                assertEquals(StatusText.fromErrorCode(null), exception.message)

            }

        }

        on("set dispatcher for getCities() with broken json") {

            mockWebServer?.setDispatcher(object : Dispatcher() {
                override fun dispatch(request: RecordedRequest?): MockResponse {
                    if (request?.path == "/cities/") {
                        return MockResponse().setResponseCode(200).setBody("{\"phonetype\":\"N95\",\"cat\":\"WP")
                    }
                    return MockResponse().setResponseCode(404)
                }
            })

            retrofit!!
                    .getCities()
                    .toBlocking()
                    .subscribe(testSubscriber)

            it("testSubscriber should return ApiResponseException") {

                testSubscriber.assertError(ApiResponseException::class.java)

            }

            it("ApiResponseException message should be like StatusText.fromErrorCode(null)") {

                var exception = testSubscriber.onErrorEvents[0]

                assertEquals(StatusText.fromErrorCode(null), exception.message)

            }

        }

        on("set dispatcher for getCities() with normal json, but with server error code") {

            mockWebServer?.setDispatcher(object : Dispatcher() {
                override fun dispatch(request: RecordedRequest?): MockResponse {
                    if (request?.path == "/cities/") {
                        return MockResponse().setResponseCode(200).setBody(File("${path}not_success_with_status.json").readText())
                    }
                    return MockResponse().setResponseCode(404)
                }
            })

            retrofit!!
                    .getCities()
                    .toBlocking()
                    .subscribe(testSubscriber)

            it("testSubscriber should return ApiResponseException") {

                testSubscriber.assertError(ApiResponseException::class.java)

            }

            it("ApiResponseException message should be like StatusText.fromErrorCode(429)") {

                var exception = testSubscriber.onErrorEvents[0]

                assertEquals(StatusText.fromErrorCode(429), exception.message)

            }

        }

        on("set dispatcher for getCities() with normal success json") {

            mockWebServer?.setDispatcher(object : Dispatcher() {
                override fun dispatch(request: RecordedRequest?): MockResponse {
                    if (request?.path == "/cities/") {
                        return MockResponse().setResponseCode(200).setBody(File("${path}success_city_list.json").readText())
                    }
                    return MockResponse().setResponseCode(404)
                }
            })

            retrofit!!
                    .getCities()
                    .toBlocking()
                    .subscribe(testSubscriber)

            it("should return no errors") {
                testSubscriber.assertNoErrors()
            }

            it("onNext() should call only once") {
                testSubscriber.assertValueCount(1)
            }

            it("List<City> should be not empty") {
                Assert.assertEquals(true, (testSubscriber.onNextEvents[0] as List<City>).isNotEmpty())
            }

            it("count of list items should be 2") {
                Assert.assertEquals(2, (testSubscriber.onNextEvents[0] as List<City>).count())
            }

        }

        on("set dispatcher for makeOrder() and check path") {

            var path = ""

            mockWebServer?.setDispatcher(object : Dispatcher() {
                override fun dispatch(request: RecordedRequest?): MockResponse {
                    if (request?.path?.startsWith("/order/") == true) {
                        path = request?.path ?: "error"
                        return MockResponse().setResponseCode(200).setBody("{\"phonetype\":\"N95\",\"cat\":\"WP\"}")
                    }
                    return MockResponse().setResponseCode(404)
                }
            })

            retrofit!!
                    .makeOrder(listOf())
                    .toBlocking()
                    .subscribe(testSubscriber)

            it("path should be /order/?expand=order") {

                assertEquals("/order/?expand=order", path)

            }

        }

    }



})