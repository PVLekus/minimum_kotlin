package its.ru.minimum

import ru.its.domain.model.Center
import ru.its.domain.model.City
import ru.its.data.utils.LocationParser
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals
import org.osmdroid.util.GeoPoint
import rx.observers.TestSubscriber

/**
 * Created by oleg on 15.11.17.
 */
object LocationParserTest : Spek({

    given("a LocationParser") {

        var subscription = TestSubscriber<GeoPoint>()

        beforeEachTest {
            subscription = TestSubscriber()
        }

        val parser = LocationParser()

        on("parse null city") {

            parser.parse(null)
                    .subscribe(subscription)

            it("should NPE") {
                subscription.assertError(NullPointerException::class.java)
            }

            it("NPE message should be = null city") {

                val message = subscription.onErrorEvents[0].message
                assertEquals("null city", message)
            }

        }

        on("parse city with null center") {

            parser.parse(City().also { it.center = null })
                    .subscribe(subscription)

            it("should NPE") {
                subscription.assertError(NullPointerException::class.java)
            }

        }

        on("parse city with center lat null") {

            parser.parse(City().also {
                it.center = Center().also {
                    it.lat = null
                    it.lon = "50.90"
                }
            })
                    .subscribe(subscription)

            it("should NPE") {
                subscription.assertError(NullPointerException::class.java)
            }

        }

        on("parse city with normal center") {

            parser.parse(City().also {
                it.center = Center().also {
                    it.lat = "40.90"
                    it.lon = "50.90"
                }
            })
                    .subscribe(subscription)

            it("should no error") {
                subscription.assertNoErrors()
            }

            it("should return geopoint with lat=40.90 and lon=50.90 ") {

                val point = subscription.onNextEvents[0]

                assertEquals(GeoPoint(40.90, 50.90).toDoubleString(), point.toDoubleString())

            }

        }

    }

})