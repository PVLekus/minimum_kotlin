package its.ru.minimum

import com.google.gson.JsonParseException
import com.google.gson.JsonParser
import ru.its.domain.model.City
import ru.its.domain.model.Order
import ru.its.domain.model.SearchPoint
import ru.its.domain.exceptions.ApiResponseException
import ru.its.data.utils.ApiResponseUtil
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert
import rx.observers.TestSubscriber
import java.io.File

/**
 * Created by oleg on 20.10.17.
 */
object ApiResponseUtilTest: Spek({

    var testSubscriber = TestSubscriber<Any>()
    val ASSET_BASE_PATH = "/Users/oleg/Idea/Minimum_kotlin/app/src/main/assets/"

    given("a ApiResponseUtil") {

        beforeEachTest {

            testSubscriber = TestSubscriber()

        }

        on("set wrong json for city") {

            val jsonObject = JsonParser().parse("{\"phonetype\":\"N95\",\"cat\":\"WP\"}").asJsonObject

            ApiResponseUtil().castToList(jsonObject,"cities", City::class.java)
                    .subscribe(testSubscriber)

            it("should return JsonParseException") {
                testSubscriber.assertError(JsonParseException::class.java)
            }

        }

        on("set json \"success\":false and without \"status\"") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "not_success_without_status.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"cities", City::class.java)
                    .subscribe(testSubscriber)

            it("should return JsonParseException") {
                testSubscriber.assertError(JsonParseException::class.java)
            }

        }

        on("set json \"success\":false and \"status\": 429") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "not_success_with_status.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"cities", City::class.java)
                    .subscribe(testSubscriber)

            it("should return ApiResponseException") {
                testSubscriber.assertError(ApiResponseException::class.java)
            }

        }

        on("set json \"success\":true but wrong name of parameter") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "success_but_wrong_parameters.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"cities", City::class.java)
                    .subscribe(testSubscriber)

            it("should return JsonParseException") {
                testSubscriber.assertError(JsonParseException::class.java)
            }

        }

        on("set normal json for get List<City>") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "success_city_list.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"cities", City::class.java)
                    .subscribe(testSubscriber)

            it("should return no errors") {
                testSubscriber.assertNoErrors()
            }

            it("onNext() should call only once") {
                testSubscriber.assertValueCount(1)
            }

            it("List<City> should be not empty") {
                Assert.assertEquals(true, (testSubscriber.onNextEvents[0] as List<City>).isNotEmpty())
            }

            it("count of list items should be 2") {
                Assert.assertEquals(2, (testSubscriber.onNextEvents[0] as List<City>).count())
            }

        }

    }

    given("a ApiResponseUtil test List<Order>") {

        var testSubscriber = TestSubscriber<List<Order>>()

        on("set normal json for get List<City>") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "normal_get_list_orders.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"orders", Order::class.java)
                    .subscribe(testSubscriber)

            it("should return no errors") {
                testSubscriber.assertNoErrors()
            }

            it("onNext() should call only once") {
                testSubscriber.assertValueCount(1)
            }

            it("List<Order> should be not empty") {
                Assert.assertEquals(true, testSubscriber.onNextEvents[0].isNotEmpty())
            }

            it("count of list items should be 4") {
                Assert.assertEquals(4, testSubscriber.onNextEvents[0].count())
            }

            it("last order client lat = 54.456546") {
                Assert.assertEquals(54.456546, testSubscriber.onNextEvents[0][3].coordinates?.client?.get(0))
            }

        }

    }

    given("a ApiResponse test castObject<String>") {

        var testSubscriber = TestSubscriber<String>()

        on("set normal json for get String (token)") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "token_normal_json.json").readText()).asJsonObject

            ApiResponseUtil().castToObject(jsonObject,"access_token", String::class.java)
                    .subscribe(testSubscriber)

            it("should return no errors") {
                testSubscriber.assertNoErrors()
            }

            it("onNext() should call only once") {
                testSubscriber.assertValueCount(1)
            }

            it("token is hyMWhvBaM") {
                Assert.assertEquals("hyMWhvBaM", testSubscriber.onNextEvents[0])
            }

        }

    }

    given("a ApiResponse test castToList<SearchPoint>") {

        var testSubscriber = TestSubscriber<List<SearchPoint>>()

        on("set normal json for get List<SearchPoint>") {

            val jsonObject = JsonParser().parse(File(ASSET_BASE_PATH + "stations_normal_json.json").readText()).asJsonObject

            ApiResponseUtil().castToList(jsonObject,"objects", SearchPoint::class.java)
                    .subscribe(testSubscriber)

            it("should return no errors") {
                testSubscriber.assertNoErrors()
            }

            it("onNext() should call only once") {
                testSubscriber.assertValueCount(1)
            }

            it("List<SearchPoint> should be not empty") {
                Assert.assertEquals(true, testSubscriber.onNextEvents[0].isNotEmpty())
            }

            it("count of list items should be 5") {
                Assert.assertEquals(5, testSubscriber.onNextEvents[0].count())
            }

            it("last point name = ж/д ст. Санаторная") {
                Assert.assertEquals("ж/д ст. Санаторная", testSubscriber.onNextEvents[0][4].name)
            }

        }

    }

})