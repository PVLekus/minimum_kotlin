package its.ru.minimum

import ru.its.data.utils.ActualStateChecker
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue

/**
 * Created by oleg on 30.11.17.
 */
object ActualStateTest : Spek({

    describe("a ActualStateChecker") {

        val checker = ActualStateChecker()

        on("date after 5 days with countsOfDay 5") {

            val isActual = checker.isDateActualForDays(System.currentTimeMillis() - 432000000L, 5)

            it("should be true") {
                assertTrue(isActual)
            }

        }

        on("date after 6 days with countsOfDay 5") {

            val isActual = checker.isDateActualForDays(System.currentTimeMillis() - 518400000L, 5)

            it("should be false") {
                assertFalse(isActual)
            }

        }

        on("date is null with countsOfDay 5") {

            val isActual = checker.isDateActualForDays(null, 5)

            it("should be false") {
                assertFalse(isActual)
            }

        }

    }

})