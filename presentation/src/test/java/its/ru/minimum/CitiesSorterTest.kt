package its.ru.minimum

import ru.its.domain.model.City
import ru.its.data.utils.CitiesSorter
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on
import org.junit.Assert.assertEquals
import rx.observers.TestSubscriber

/**
 * Created by oleg on 09.11.17.
 */
object CitiesSorterTest: Spek({

    given("a CitiesSorter and List<City> for sort") {

        var testSubscriber = TestSubscriber<List<City>>()

        beforeEachTest {
            testSubscriber = TestSubscriber()
        }

        val sorter = CitiesSorter()

        val list = listOf(City().also { it.name = "ufa"}, City().also { it.name = "moscow"}, City().also { it.name = "abakan"})

        on("sort with exist city") {

            val selectedCity = City().also { it.name = "moscow" }

            sorter.sort(selectedCity, list)
                    .subscribe(testSubscriber)

            it("testSubscriber should no errors") {
                testSubscriber.assertNoErrors()
            }

            it("testSubscriber should onNext once") {
                testSubscriber.assertValueCount(1)
            }

            it("testSubscriber should return list with first element moscow") {
                val first = testSubscriber.onNextEvents[0][0].name
                assertEquals("moscow", first)
            }

            it("testSubscriber should return list with second element abakan") {
                val second = testSubscriber.onNextEvents[0][1].name
                assertEquals("abakan", second)
            }

        }

        on("sort with null selected city") {

            sorter.sort(null, list)
                    .subscribe(testSubscriber)

            it("testSubscriber should return NPE") {
                testSubscriber.assertError(NullPointerException::class.java)
            }

        }

        on("sort with null cities") {

            val selectedCity = City().also { it.name = "moscow" }

            sorter.sort(selectedCity, null)
                    .subscribe(testSubscriber)

            it("testSubscriber should return NPE") {
                testSubscriber.assertError(NullPointerException::class.java)
            }

        }

        on("sort with null") {

            sorter.sort(null, null)
                    .subscribe(testSubscriber)

            it("testSubscriber should return NPE") {
                testSubscriber.assertError(NullPointerException::class.java)
            }

        }

    }

})