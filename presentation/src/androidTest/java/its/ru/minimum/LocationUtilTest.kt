package its.ru.minimum

import android.location.Location
import android.support.test.runner.AndroidJUnit4
import ru.its.domain.model.Center
import ru.its.domain.model.City
import ru.its.data.utils.LocationUtil
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import rx.observers.TestSubscriber

/**
 * Created by oleg on 16.11.17.
 */
@RunWith(AndroidJUnit4::class)
class LocationUtilTest {

    var subscription = TestSubscriber<City?>()

    val util = LocationUtil()

    val cities = mutableListOf<City>()

    @Before
    fun tune() {

        subscription = TestSubscriber()

        cities.add(City().also {
            it.id = "sterlitamak"
            it.center = Center().also {
                it.lat = "53.639876"
                it.lon = "55.948989"
            }
        })

        cities.add(City().also {
            it.id = "ufa"
            it.center = Center().also {
                it.lat = "54.722504"
                it.lon = "55.984387"
            }
        })

        cities.add(City().also {
            it.id = "kazan"
            it.center = Center().also {
                it.lat = "55.842871"
                it.lon = "49.106943"
            }
        })

    }

    @Test
    fun testLocationLikeCenterCity() {

        val location = Location("")
        location.latitude = 54.722504
        location.longitude = 55.984387

        util.findNearestCity(location, cities)
                .subscribe(subscription)

        subscription.assertNoErrors()
        subscription.assertValueCount(1)

        val city = subscription.onNextEvents[0]

        assertEquals("ufa", city?.id)

    }

}