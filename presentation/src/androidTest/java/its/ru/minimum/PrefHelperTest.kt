package its.ru.minimum

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import ru.its.data.repo.PrefHelper
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by oleg on 19.10.17.
 */
@RunWith(AndroidJUnit4::class)
class PrefHelperTest {

    @Before
    fun tune() {

        PrefHelper.initInstance(InstrumentationRegistry.getTargetContext())

    }

    @Test
    fun getWithoutPut() {

        PrefHelper.remove(PrefHelper.Key.TEST)

        val password = PrefHelper.get(PrefHelper.Key.TEST, String::class.java)

        assertEquals(null, password)

    }

    @Test
    fun putStringAngGetSame() {

        PrefHelper.put(PrefHelper.Key.TEST, "12345")

        val password = PrefHelper.get(PrefHelper.Key.TEST, String::class.java)

        assertEquals("12345", password)

    }

    @Test
    fun putStringAndGetFalseBoolean() {

        PrefHelper.put(PrefHelper.Key.TEST, "12345")

        val password = PrefHelper.get(PrefHelper.Key.TEST, Boolean::class.java)

        assertEquals(false, password)

    }

    @Test
    fun putLongAndGetSame() {

        PrefHelper.put(PrefHelper.Key.TEST, 1234L)

        val password = PrefHelper.get(PrefHelper.Key.TEST, Long::class.java)

        assertEquals(1234L, password)

    }

    @Test
    fun putIntAndGetSameLong() {

        PrefHelper.put(PrefHelper.Key.TEST, 1234)

        val password = PrefHelper.get(PrefHelper.Key.TEST, Long::class.java)

        assertEquals(1234L, password)

    }

}