# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/oleg/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**

# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*

# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

-keepattributes *Annotation*,SourceFile,LineNumberTable

-dontwarn org.osmdroid.tileprovider.modules.NetworkAvailabliltyCheck
-dontwarn its.ru.minimum.pages.details.DetailsPresenter$confirm$1$2
-dontwarn its.ru.minimum.pages.geocode.GeocodeFragment$onViewCreated$1$1
-dontwarn its.ru.minimum.pages.history.HistoryPresenter$removeOrder$1$5
-dontwarn its.ru.minimum.pages.order.OrderPresenter$getCost$2$2
-dontwarn its.ru.minimum.pages.details.DetailsPresenter$confirm$1$3$1
-dontwarn its.ru.minimum.pages.details.DetailsPresenter$confirm$1$5
-dontwarn its.ru.minimum.pages.details.DetailsPresenter$confirm$1$4
-dontwarn its.ru.minimum.pages.history.HistoryPresenter$removeOrder$1$4$1

-dontwarn sun.misc.**
-dontwarn com.appsflyer.**
-dontwarn androidx.media.**

-dontwarn com.android.installreferrer

-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

-dontnote rx.internal.util.PlatformDependent

-keep class com.google.android.gms.internal.** { *; }


