package ru.its.data.remote

import android.util.Log
import com.crashlytics.android.Crashlytics
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.stream.MalformedJsonException
import retrofit2.http.*
import ru.its.data.utils.ApiResponseUtil
import ru.its.data.utils.JsonMaker
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.exceptions.InternetConnectionException
import ru.its.domain.model.*
import ru.its.domain.repo.IUserRepository
import rx.Observable
import java.io.EOFException
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by oleg on 13.09.17.
 */
interface IRutaxiAPI {

    @GET("1.0.0/cities/")
    fun getCities(@Query("expand") expand: String = "defaultRate,currency"): Observable<JsonObject>

    @GET("1.0.0/favorites/")
    fun getFavorites(@Header("Authorization") token: String, @Query("city") city: String, @Query("expand") expand: String = "object"): Observable<JsonObject>

    @POST("1.0.0/favorites/")
    fun addFavorite(@Header("Authorization") token: String,  @Query("city") city: String, @Body body: JsonObject): Observable<JsonObject>

    @DELETE("1.0.0/favorites/{id}/")
    fun removeFromFavorite(@Header("Authorization") token: String,@Path("id") id: String,  @Query("city") city: String): Observable<JsonObject>

    @GET("1.0.0/rates/")
    fun getRates(@Query("city") city: String): Observable<JsonObject>

    @POST("1.0.0/code/")
    fun sendCode(@Body body: JsonObject) : Observable<JsonObject>

    @PUT("1.0.0/code/")
    fun checkCode(@Body body: JsonObject) : Observable<JsonObject>

    @PUT("1.0.0/ping/")
    fun checkToken(@Header("Authorization") token: String) : Observable<JsonObject>

    @GET("1.0.0/search/")
    fun searchObjects(@Query("city") city: String,
                      @Query("query") address: String) : Observable<JsonObject>

    @GET("1.0.0/airports/")
    fun searchAirports(@Query("city") city: String) : Observable<JsonObject>

    @GET("1.0.0/stations/")
    fun searchStations(@Query("city") city: String) : Observable<JsonObject>

    @GET("1.0.0/populars/")
    fun searchPopulars(@Query("city") city: String) : Observable<JsonObject>

    @POST("1.0.0/near/")
    fun searchNearObjects(@Body body: JsonObject) : Observable<JsonObject>

    //need token
    @GET("1.0.0/history/points/")
    fun searchInHistory(@Header("Authorization") token: String, @Query("city") city: String) : Observable<JsonObject>

    @GET("1.0.0/history/")
    fun getCurrentOrders(@Header("Authorization") token: String, @Query("city") city: String, @Query("expand") expand: String = "car_info,rate,review,options,payments,point.isFavorite", @Query("currents") currents: Boolean = true) : Observable<JsonObject>

    @GET("1.0.0/history/")
    fun getHistoryOrders(@Header("Authorization") token: String,
                         @Query("city") city: String,
                         @Query("page") page: Int,
                         @Query("per-page") count: Int,
                         @Query("expand") expand: String = "car_info,rate,review,payments,point.isFavorite") : Observable<JsonObject>

    @POST("1.0.0/order/")
    fun makeOrder(@Header("Authorization") token: String, @Body body: JsonObject, @Query("expand") expand: String = "order") : Observable<JsonObject>

    @POST("1.0.0/cost/")
    fun getCost(@Body body: JsonObject) : Observable<JsonObject>

    @POST("1.0.0/order/{id}/cancel/")
    fun cancelOrder(@Header("Authorization") token: String, @Path("id") id: String, @Query("city") city: String, @Query("expand") expand: String = "order") : Observable<JsonObject>

    @POST("1.0.0/order/{id}/pay/")
    fun payCard(@Header("Authorization") token: String, @Path("id") id: String, @Query("city") city: String): Observable<JsonObject>

    @GET("1.0.0/card/")
    fun getCards(@Header("Authorization") token: String, @Query("city") city: String) : Observable<JsonObject>

    @PUT("1.0.0/card/")
    fun addCard(@Header("Authorization") token: String, @Body body: JsonObject) : Observable<JsonObject>

    @DELETE("1.0.0/card/")
    fun removeCard(@Header("Authorization") token: String, @Query("city") city: String, @Query("card") mask: String) : Observable<JsonObject>

    @DELETE("1.0.0/order/{id}/")
    fun removeOrder(@Header("Authorization") token: String, @Path("id") id: String, @Query("city") city: String): Observable<JsonObject>

}

class RutaxiAPI(private val userRepository: IUserRepository, private val service: IRutaxiAPI, private val handleError: Boolean) {

    private val token
            get() = "Bearer ${userRepository.getUser()?.token ?: ""}"

    private val cityId
            get() = userRepository.getUser()?.city?.id ?: ""

    private val cityObj
            get() = JsonObject().also { it.addProperty("city", cityId) }

    fun payCard(orderId: String): Observable<Boolean> {

        return service.payCard(token, orderId, cityId)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext {
                    throw handleError(it, "payCard")
                }

    }

    fun removeCard(mask: String): Observable<Boolean> {

        return service.removeCard(token, cityId, mask)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext {
                    throw handleError(it, "removeCard")
                }


    }

    fun addCard(): Observable<String> {

        return service.addCard(token, cityObj)
                .flatMap { ApiResponseUtil().castToObject(it, "url", String::class.java) }
                .onErrorResumeNext {
                    throw handleError(it, "addCard")
                }

    }

    fun getCards(): Observable<List<Card>?> {

        return service.getCards(token, cityId)
                .flatMap { ApiResponseUtil().castToList(it, "list", Card::class.java) }
                .onErrorResumeNext {
                    throw handleError(it, "getCards")
                }

    }
    

    fun getCities() : Observable<List<City>?> {

        return service
                .getCities()
                .flatMap { ApiResponseUtil().castToList(it, "cities", City::class.java) }
                .onErrorResumeNext {
                    throw handleError(it, "getCities")
                }

    }


    fun getRates(city: City? = null) : Observable<List<Rate>> {

        return service
                .getRates(city?.id ?: cityId)
                .flatMap { ApiResponseUtil().castToList(it, "rates", Rate::class.java) }
                .doOnNext { if (it?.isNotEmpty() != true) throw NullPointerException(city?.id ?: cityId + " - rates is empty or null") }
                .onErrorResumeNext { throw handleError(it, "getRates") }

    }

    fun removeFromFavorite(id: String): Observable<Boolean> {
        return service.removeFromFavorite(token, id, cityId)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext { throw handleError(it, "removeFromFavorite") }
    }

    fun addFavorite(searchPoint: SearchPoint, name: String?): Observable<Boolean> {

        val json = JsonObject()
        json.addProperty("city", cityId)
        json.addProperty("object_id", searchPoint.id)
        if (!name.isNullOrBlank()) json.addProperty("name", name)
        if (searchPoint.metatype == 0) {
            if (!searchPoint.house.isNullOrBlank()) json.addProperty("house", searchPoint.house)
            if (!searchPoint.enter.isNullOrBlank()) json.addProperty("enter", searchPoint.enter)
        }

        return service
                .addFavorite(token, cityId, json)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext { throw handleError(it, "addFavorite") }
    }

    fun sendCode(phone: String) : Observable<Boolean> {

        val json = JsonObject()
        json.addProperty("phone", phone)

        return service
                .sendCode(json/*JsonMaker.makeJsonForSendCode(phone)*/)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext { throw handleError(it, "sendCode") }

    }

    fun checkCode(phone: String?, code: String?) : Observable<String> {

        return service
                .checkCode(JsonMaker.makeJsonForCheckCode(phone, code))
                .flatMap { ApiResponseUtil().castToObject(it, "access_token", String::class.java) }
                .onErrorResumeNext { throw handleError(it, "checkCode") }

    }

    fun checkToken() : Observable<Boolean> {

        return service
                .checkToken("Bearer $token")
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext { throw handleError(it, "checkToken") }

    }

    fun removeOrder(id: String): Observable<Boolean> {
        return service.removeOrder(token, id,cityId)
                .flatMap { ApiResponseUtil().isResponseSuccess(it) }
                .onErrorResumeNext { throw handleError(it, "removeOrder") }
    }

    fun searchObjects(address: String) : Observable<List<SearchPoint>> {

        return service
                .searchObjects(cityId, address)
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "searchObjects") }

    }

    fun getAirportsPoints() : Observable<List<SearchPoint>> {

        return service
                .searchAirports(cityId)
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "getAirportsPoints") }

    }

    fun getStationPoints() : Observable<List<SearchPoint>> {

        return service
                .searchStations(cityId)
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "getStationPoints") }

    }

    fun getPopularPoints() : Observable<List<SearchPoint>> {

        return service
                .searchPopulars(cityId)
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "getPopularPoints") }

    }

    fun getHistoryPoints() : Observable<List<SearchPoint>> {

        return service
                .searchInHistory(token, cityId)
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "getHistoryPoints") }

    }

    fun getFavoritePoints(): Observable<List<FavoritePoint>?> {

        return service
                .getFavorites(token, cityId)
                .flatMap { ApiResponseUtil().castToList(it, "objects", FavoritePoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "getFavoritePoints") }

    }

    fun getHistoryOrders(page: Int, count: Int): Observable<List<Order>?> {

        return service
                .getHistoryOrders(token, cityId, page, count)
                .flatMap { ApiResponseUtil().castToList(it, "orders", Order::class.java) }
                .onErrorResumeNext { throw handleError(it, "getHistoryOrders") }

    }

    fun getCurrentOrders() : Observable<List<Order>?> {

        return service
                .getCurrentOrders(token, cityId)
                .flatMap { ApiResponseUtil().castToList(it, "orders", Order::class.java) }
                .onErrorResumeNext { throw handleError(it, "getCurrentOrders") }

    }

    fun searchNearObjects(lat: Double, lon: Double) : Observable<List<SearchPoint>> {

        return service
                .searchNearObjects(JsonMaker.makeJsonForNearObjects(lat, lon, cityId))
                .flatMap { ApiResponseUtil().castToList(it, "objects", SearchPoint::class.java) }
                .onErrorResumeNext { throw handleError(it, "searchNearObjects") }

    }

    fun getCost(points: List<SearchPoint>) : Observable<String> {

        return service
                .getCost(JsonMaker.makeJsonForCost(points, cityId))
                .flatMap { ApiResponseUtil().castToObject(it, "cost", String::class.java) }
                .onErrorResumeNext { throw handleError(it, "getCost") }

    }

    fun makeOrder(points: List<SearchPoint>, phone: String? = null, comment: String? = null, preorderTime: String? = null, guid: String? = null) : Observable<String> {

        return service
                .makeOrder(token = token, body = JsonMaker.makeJsonForOrder(points, cityId, phone, comment, preorderTime = preorderTime, guid = guid))
                .flatMap { ApiResponseUtil().castToObject(it, "order_id", String::class.java) }
                .onErrorResumeNext {
                    throw handleError(it, "makeOrder")
                }
    }

    fun cancelOrder(id: String) : Observable<Order> {

        return service
                .cancelOrder(token, id, cityId)
                .flatMap { ApiResponseUtil().castToObject(it, "order", Order::class.java) }
                .onErrorResumeNext { throw handleError(it, "cancelOrder") }
    }

    private fun handleError(throwable: Throwable?, info: String): Throwable  {

        throwable?.printStackTrace()

        return when (throwable) {

            is ApiResponseException -> throwable

            is UnknownHostException, is SocketTimeoutException -> InternetConnectionException("")

            is IOException -> {

                if (throwable is EOFException || throwable is JsonParseException || throwable is MalformedJsonException) {
                    sendServerCrash(throwable, info)
                    ApiResponseException(StatusText.fromErrorCode(null))
                } else {
                    InternetConnectionException("")
                }

            }
            else -> {
                sendServerCrash(throwable, info)
                ApiResponseException(StatusText.fromErrorCode(null))
            }

        }
    }

    private fun sendServerCrash(throwable: Throwable?, info: String) {
        if (!handleError) return
            Crashlytics.log(Log.ERROR, "SERVER", "$info \n${throwable?.message}")
            Crashlytics.logException(throwable)
    }

}