package ru.its.data.remote

import android.content.Context
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import ru.its.data.utils.AvailabilityUtils
import java.io.FileNotFoundException
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

/**
 * Created by oleg on 03.03.18.
 */
class CacheInterceptor(private val context: Context): Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response? {

        val cacheMode = !AvailabilityUtils.hasInternetAccess(context)
        val builder1 = chain?.request()?.newBuilder()
        // support forcing network for retrofit methods by adding header /Cache-Control: no-cache/
        if (chain?.request()?.cacheControl() == null || !chain.request().cacheControl().noCache()) {
            builder1?.cacheControl(if (cacheMode) CACHE_CC else NETWORK_CC)
        }
        var response: Response? = null
        val request = builder1?.build()
        try {
            response = chain?.proceed(request)

            if (cacheMode && response!!.code() == 504) {
                response.body()!!.close()
                throw FileNotFoundException("No cache")
            }
        } catch (e: IOException) {
            if (response != null && response.body() != null) {
                response.body()!!.close()
            }
            if (e is SocketTimeoutException) {
                response = chain?.proceed(builder1?.cacheControl(CACHE_CC)?.build())

                if (response!!.code() == 504) {
                    response.body()!!.close()
                    throw FileNotFoundException("No cache")
                }
            } else {
                throw e
            }
        }

        return response

    }

    companion object {

        private val MAX_AGE = 60
        private val MAX_STALE = 60 * 60 * 24 * 28

        private val NETWORK_CC: CacheControl by lazy { CacheControl.Builder().maxAge(MAX_AGE, TimeUnit.SECONDS).build() }
        private val CACHE_CC: CacheControl by lazy { CacheControl.Builder().onlyIfCached().maxStale(MAX_STALE, TimeUnit.SECONDS).build() }
    }

}