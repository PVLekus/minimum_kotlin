package ru.its.data.remote

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.http.RealResponseBody
import okio.GzipSource
import okio.Okio
import java.io.IOException

class EncodingInterceptor: Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val compressedRequest = chain.request().newBuilder()
                .header("Accept-Encoding", "gzip")
                .build()
        val response = chain.proceed(compressedRequest)
        return unzip(response)
    }

    @Throws(IOException::class)
    private fun unzip(response: Response): Response {
        if (response.body() == null) {
            return response
        }

        //check if we have gzip response
        val contentEncoding = response.headers().get("Content-Encoding")

        //this is used to decompress gzipped responses
        if (contentEncoding != null && contentEncoding == "gzip") {
            val contentLength = response.body()!!.contentLength()
            val responseBody = GzipSource(response.body()!!.source())
            val strippedHeaders = response.headers().newBuilder().build()
            return response.newBuilder().headers(strippedHeaders)
                    .body(RealResponseBody(response.body()!!.contentType()!!.toString(), contentLength, Okio.buffer(responseBody)))
                    .build()
        } else {
            return response
        }
    }

}