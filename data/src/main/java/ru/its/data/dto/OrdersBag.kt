package ru.its.data.dto

import com.google.gson.annotations.SerializedName
import ru.its.data.repo.PrefsModel
import ru.its.domain.model.Order

/**
 * Created by oleg on 03.03.18.
 */
class OrdersBag: PrefsModel() {

    @SerializedName("id")
    var id: String = ""
    @SerializedName("orders")
    var orders: List<Order>? = null

}

class HistoryBag: PrefsModel() {

    @SerializedName("id")
    var id: String = ""
    @SerializedName("orders")
    var orders: List<Order>? = null

}