package ru.its.data.dto

import com.google.gson.annotations.SerializedName
import ru.its.data.repo.PrefsModel
import ru.its.domain.model.City

/**
 * Created by oleg on 03.03.18.
 */
class CitiesBag: PrefsModel() {

    @SerializedName("cities")
    var cities: List<City>? = null

}