package ru.its.data.dto

import com.google.gson.annotations.SerializedName
import ru.its.data.repo.PrefsModel
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 03.03.18.
 */
open class PointsBag : PrefsModel() {

    @SerializedName("id")
    var id: String = ""
    @SerializedName("points")
    var points: List<SearchPoint>? = null

}