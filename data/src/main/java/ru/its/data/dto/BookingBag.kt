package ru.its.data.dto

import com.google.gson.annotations.SerializedName
import ru.its.data.repo.PrefsModel
import ru.its.domain.model.Booking

/**
 * Created by oleg on 03.03.18.
 */
class BookingBag: PrefsModel() {

    @SerializedName("booking")
    var booking: Booking? = null

}