package ru.its.data.dto

import com.google.gson.annotations.SerializedName
import ru.its.data.repo.PrefsModel
import ru.its.domain.model.User

/**
 * Created by oleg on 03.03.18.
 */
class UserBag: PrefsModel() {

    @SerializedName("user")
    var user: User? = null

}