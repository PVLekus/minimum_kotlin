package ru.its.data.usecase

import ru.its.data.remote.RutaxiAPI
import ru.its.data.repo.PrefHelper
import ru.its.domain.model.City
import ru.its.domain.model.User
import ru.its.domain.repo.*
import ru.its.domain.usecase.UseCase
import rx.Observable

/**
 * Created by oleg on 15.12.17.
 */
class ClearCacheUseCase(private val historyPointsRepository: IHistoryPointsRepository,
                        private val airportsRepository: IAirportsRepository,
                        private val stationsRepository: IStationsRepository,
                        private val popularRepository: IPopularRepository,
                        private val ordersRepository: IOrdersRepository,
                        private val bookingRepository: IBookingRepository,
                        private val cardsRepository: ICardsRepository,
                        private val historyRepository: IHistoryRepository,
                        private val favoritesRepository: IFavoritesRepository): UseCase<Boolean, Boolean>() {

    override fun createObservable(params: Boolean): Observable<Boolean> {
        return Observable.just(params)
                .doOnNext {
                    bookingRepository.clear()
                    ordersRepository.clear()
                    historyRepository.clear()
                    historyPointsRepository.clear()
                    airportsRepository.clear()
                    stationsRepository.clear()
                    popularRepository.clear()
                    cardsRepository.clear()
                    favoritesRepository.clear()
                }
    }
}

class CitySelectUseCase (private val userRepository: IUserRepository, private  val clearCacheUseCase: ClearCacheUseCase, private val rutaxiAPI: RutaxiAPI) : UseCase<Boolean, City>() {
    override fun createObservable(params: City): Observable<Boolean> {

        return rutaxiAPI.getRates(params)
                .doOnNext {
                    userRepository.updateCity(params)
                    userRepository.updateRate(it?.get(0))
                }
                .flatMap { clearCacheUseCase.createObservable(true) }

    }
}

class UserExitUseCase (private val userRepository: IUserRepository, private  val clearCacheUseCase: ClearCacheUseCase) : UseCase<Boolean, Boolean>() {

    override fun createObservable(params: Boolean): Observable<Boolean> {

        return Observable.just(params)
                .doOnNext {
                    userRepository.clear()
                }
                .flatMap { clearCacheUseCase.createObservable(true) }

    }
}

class InitPrefsConstUseCase(private val userRepository: IUserRepository, private val prefHelper: PrefHelper): UseCase<Boolean, Boolean>() {

    override fun createObservable(params: Boolean): Observable<Boolean> {

        return Observable.just(params)
                .doOnNext {
                    if (prefHelper.get(PrefHelper.Key.AUTO_LOCATION, Boolean::class.java) == null) prefHelper.put(PrefHelper.Key.AUTO_LOCATION, true)
                    prefHelper.put(PrefHelper.Key.AUTO_LOCATION_TEMP, true)
                    if (userRepository.getUser() == null) userRepository.save(User())
                }

    }
}