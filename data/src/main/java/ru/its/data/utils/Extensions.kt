package ru.its.data.utils

import com.google.gson.reflect.TypeToken
import rx.Observable
import rx.Subscription
import rx.subscriptions.CompositeSubscription

inline fun <reified T> genericType() = object: TypeToken<T>() {}.type

fun Subscription.addToComposite(compositeSubscription: CompositeSubscription) {
    compositeSubscription.add(this)
}

fun <T> T.toObservable() : Observable<T> = Observable.just(this)
