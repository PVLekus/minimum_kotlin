package ru.its.data.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by oleg on 03.03.18.
 */
class AvailabilityUtils {

    companion object {

        fun hasInternetAccess(context: Context): Boolean {
            var hasInternet = false
            val cm = context
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = cm.activeNetworkInfo
            if (networkInfo != null && networkInfo.isConnected) {
                hasInternet = true
            }

            return hasInternet
        }

    }

}