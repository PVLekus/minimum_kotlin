package ru.its.data.utils

import android.location.Location
import ru.its.domain.model.City
import rx.Observable

/**
 * Created by oleg on 15.11.17.
 */
class LocationUtil {

    private val parser = LocationParser()

    fun findNearestCity(location: Location, cities: List<City>) : Observable<City?> {

        return Observable.from(cities)
                .flatMap { city -> parser.parse(city).map { Pair(it, city) } }
                .map { pair ->
                    Location("").also {
                        it.latitude = pair.first.latitude
                        it.longitude = pair.first.longitude
                    }.let {
                        Pair(getDistanceInNumber(location, it), pair.second)
                    }
                }
                .toList()
                .map { it.minBy { it.first } }
                .map { it?.second }

    }

    fun isPointInCity(location: Location, city: City?) : Observable<Boolean> {

        return parser.parseToLocation(city)
                .map {
                    getDistanceInNumber(it, location)
                }
                .map { it < 50 }

    }

    private fun getDistanceInNumber(start: Location, dest: Location) : Int = (start.distanceTo(dest)/1000).toInt()

}