package ru.its.data.utils

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import ru.its.domain.model.Booking
import java.lang.RuntimeException

/**
 * Created by oleg on 23.11.17.
 */
class OrderRequestBuilder {

    fun makeJsonObject(booking: Booking, city: String, phone: String?): JsonObject {

        val mainObject = JsonObject()
        mainObject.addProperty("city", city)

        val orderObject = JsonObject()
        orderObject.addProperty("comment", booking.comment)
        phone?.let { orderObject.addProperty("additional_phone", phone) }

        val pointsArray = JsonArray()
        booking.points?.forEach {
            val pointObject = JsonObject()
            pointObject.addProperty("object_id", it.id ?: throw RuntimeException("No id or object_id"))
            pointObject.addProperty("house", it.house)
            pointObject.addProperty("enter", it.enter)
            pointsArray.add(pointObject)
        }

        orderObject.add("points", pointsArray)

        mainObject.add("Order", orderObject)

        return mainObject
    }

}