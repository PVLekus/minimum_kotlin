package ru.its.data.utils

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build

/**
 * Created by oleg on 15.11.17.
 */
class PermissionRequester(val activity: Activity) {

    fun requestPermissions(permission: String, requestCode: Int) {

        if (!checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permission, requestCode)
            }
        }

    }

    fun checkPermission(permission: String): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            return activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        }
        return true

    }

}