package ru.its.data.utils

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import ru.its.domain.exceptions.ApiResponseException
import ru.its.domain.model.StatusText
import rx.Observable

/**
 * Created by oleg on 19.10.17.
 */
class ApiResponseUtil {

    fun isResponseSuccess(jsonObject: JsonObject) : Observable<Boolean> {

        return Observable.just(jsonObject)
                .map { Pair(it.get("success")?.asBoolean, it.getAsJsonObject("data")?.get("status")?.asInt) }
                .doOnNext { if (it.first == null) throw JsonParseException("no success") }
                .doOnNext { if (!it.first!! && it.second == null) throw JsonParseException("no status") }
                .doOnNext { if (!it.first!!) throw ApiResponseException(StatusText.fromErrorCode(it.second)) }
                .map { true }

    }

    fun <T> castToObject(jsonObject: JsonObject, dataName: String, classOfT: Class<T>) : Observable<T> {

                return isResponseSuccess(jsonObject)
                .map { jsonObject.getAsJsonObject("data")?.get(dataName) }
                .doOnNext { if (it == null) throw JsonParseException("no data for parameter $dataName") }
                .map { it?.toString() }
                .map {
                    Gson().fromJson<T>(it, classOfT)
                }

    }

    fun <T> castToList(jsonObject: JsonObject, dataName: String, classOfT: Class<T>) : Observable<List<T>> {

        return isResponseSuccess(jsonObject)
                .map { jsonObject.getAsJsonObject("data")?.getAsJsonArray(dataName) }
                .doOnNext { if (it == null) throw JsonParseException("no data for parameter $dataName") }
                .flatMap { Observable.from(it) }
                .map { it?.toString() }
                .map {
                    Gson().fromJson<T>(it, classOfT)
                }
                .toList()

    }

}