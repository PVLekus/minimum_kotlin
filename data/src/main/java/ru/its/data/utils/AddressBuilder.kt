package ru.its.data.utils

import ru.its.domain.model.FavoritePoint
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 16.11.17.
 */
class AddressBuilder {

    fun build(searchPoint: SearchPoint?) : String? {

         return searchPoint?.let {
             if (it.metatype == 0)  {
                 var address = it.name
                 it.house?.let { address+= ", $it" }
                 address
             } else if (!it.name?.trim().isNullOrEmpty()) it.name
             else it.address

        }

    }

    fun build(favoritePoint: FavoritePoint?): String? {

        return favoritePoint?.let {

            var address = ""

            if (it.metatype == 0)  {
                it.details?.name?.let { address+=it }
                address += " • ${it.house ?: " "}"
                it.enter?.let { address+=" • $it" }
            } else {
                it.details?.type?.let { address+=it.capitalize() }
                it.details?.address?.let { address+=" • $it" }
            }

            return address
        }

    }

}