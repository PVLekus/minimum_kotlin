package ru.its.data.utils

import android.location.Location
import org.osmdroid.util.GeoPoint
import ru.its.domain.model.City
import ru.its.domain.model.Coordinates
import rx.Observable

/**
 * Created by oleg on 15.11.17.
 */
class LocationParser {

    fun parse(city: City?) : Observable<GeoPoint> {

        return Observable.just(city)
                .doOnNext { if (it == null) throw NullPointerException("null city") }
                .doOnNext { if (it?.center == null) throw NullPointerException("center for city ${it?.name} null") }
                .map { it?.center!! }
                .doOnNext { if (it.lat.isNullOrEmpty() || it.lon.isNullOrEmpty()) throw NullPointerException("lat/lon for city ${city?.name} is ${it.lat} and ${it.lon}") }
                .map { Pair(it.lat!!.toDouble(), it.lon!!.toDouble()) }
                .map { GeoPoint(it.first, it.second) }


    }

    fun parseToLocation(city: City?) : Observable<Location> {

        return Observable.just(city)
                .doOnNext { if (it == null) throw NullPointerException("null city") }
                .doOnNext { if (it?.center == null) throw NullPointerException("center for city ${it?.name} null") }
                .map { it?.center!! }
                .doOnNext { if (it.lat.isNullOrEmpty() || it.lon.isNullOrEmpty()) throw NullPointerException("lat/lon for city ${city?.name} is ${it.lat} and ${it.lon}") }
                .map { Pair(it.lat!!.toDouble(), it.lon!!.toDouble()) }
                .map { pair ->
                    Location("").also {
                        it.latitude = pair.first
                        it.longitude = pair.second
                    }
                }


    }

    fun calculatePointsForBoundingBox(coordinates: Coordinates?) : List<Double>? {

        if (coordinates?.driver?.isNotEmpty() != true) return null
        if (coordinates?.client?.isNotEmpty() != true) return null

        val north = maxOf(coordinates?.driver?.get(0)!!,coordinates?.client?.get(0)!!) + 0.0008
        val east = maxOf(coordinates?.driver?.get(1)!!,coordinates?.client?.get(1)!!) + 0.0008
        val south = minOf(coordinates?.driver?.get(0)!!,coordinates?.client?.get(0)!!) - 0.0008
        val west = minOf(coordinates?.driver?.get(1)!!,coordinates?.client?.get(1)!!) - 0.0008

        return listOf(north, east, south, west)
    }



}