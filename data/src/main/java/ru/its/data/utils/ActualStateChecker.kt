package ru.its.data.utils

import java.util.*


/**
 * Created by oleg on 03.11.17.
 */
class ActualStateChecker {

    fun isDateActualForDays(date: Long?, countOfDays: Int): Boolean {

        return date?.let {
            Calendar.getInstance()
                    .also {
                        it.set(Calendar.HOUR_OF_DAY, 0)
                        it.set(Calendar.MINUTE, 0)
                        it.set(Calendar.SECOND, 0)
                        it.set(Calendar.MILLISECOND, 0)
                    }
                    .also {
                        it.add(Calendar.DATE, -countOfDays)
                    }
                    .timeInMillis < date
        } ?: false


    }


}