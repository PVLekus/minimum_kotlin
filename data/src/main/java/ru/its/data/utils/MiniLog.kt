package ru.its.data.utils

import android.util.Log
import java.lang.Exception

/**
 * Created by oleg on 09.05.17.
 */
class MiniLog {

    companion object {

        val DEBUG = true

        fun i(tag: String, string: String) { if (DEBUG) Log.i(tag, string) }
        fun e(tag: String, string: String) { if (DEBUG) Log.e(tag, string) }
        fun e(tag: String, string: String, e: Exception) { if (DEBUG) Log.e(tag, string, e) }
        fun d(tag: String, string: String) { if (DEBUG) Log.d(tag, string) }
        fun w(tag: String, string: String) { if (DEBUG) Log.w(tag, string) }

    }

}