package ru.its.data.utils

import ru.its.domain.model.City
import rx.Observable

/**
 * Created by oleg on 09.11.17.
 */
class CitiesSorter {

    fun sort(selectedCity: City?, cities: List<City>?) : Observable<List<City>> {

        return Observable.just(1)
                .doOnNext { if (selectedCity == null) throw NullPointerException("empty selected city") }
                .doOnNext { if (cities == null) throw NullPointerException("empty cities") }
                .map { cities!! }
                .map {
                    it.sortedBy { it.name }
                }
                .map {
                    it.filter { it.name != selectedCity?.name }
                }
                .map {
                    mutableListOf<City>().also { list ->
                        list.add(selectedCity!!)
                        list.addAll(it)
                    }
                }

    }

}