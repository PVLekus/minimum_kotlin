package ru.its.data.utils

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import ru.its.domain.model.SearchPoint

/**
 * Created by oleg on 22.10.17.
 */
class JsonMaker {

    private var jsonObject = JsonObject()


    companion object {

        private val TAG = "JsonMaker"

        fun makeJsonForOrder(points: List<SearchPoint>, city: String, phone: String? = null, comment: String? = null, preorderTime: String? = null, guid: String? = null) : JsonObject {

            var maker = JsonMaker()
            maker.addProperty("city", city)
            maker.addElement("Order", maker.createOrderJsonObject(points).also { order ->

                phone?.let { order.addProperty("additional_phone", it) }
                comment?.let { order.addProperty("comment", it) }
                preorderTime?.let {
                    order.addProperty("is_preorder", 1)
                    order.addProperty("preorder_time", it)
                }
                guid?.let { order.addProperty("guid", it) }

            })



            return maker.make()
        }

        fun makeJsonForCost(points: List<SearchPoint>, city: String, preorderTime: String? = null) : JsonObject {

            val maker = JsonMaker()

            maker.addProperty("city", city)
            maker.addElement("Order", maker.createOrderJsonObject(points).also { order ->
                preorderTime?.let {
                    order.addProperty("is_preorder", 1)
                    order.addProperty("preorder_time", it)
                }
            })

            return maker.make()

        }

        fun makeJsonForRates(city: String) : JsonObject = JsonMaker().addProperty("city", city).make()

        fun makeJsonForSendCode(phone: String) : JsonObject = JsonMaker().addProperty("phone", phone).make()

        fun makeJsonForCheckCode(phone: String?, code: String?) : JsonObject =
                JsonMaker()
                        .addProperty("phone", phone)
                        .addProperty("code", code)
                        .make()

        fun makeJsonForNearObjects(lat: Double, lon: Double, city: String) : JsonObject =
                JsonMaker()
                        .addProperty("latitude", lat)
                        .addProperty("longitude", lon)
                        .addProperty("city", city)
                        .make()
    }

    fun <T> addProperty(name: String, value: T): JsonMaker {

        when (value) {
            is String -> jsonObject.addProperty(name, value)
            is Char -> jsonObject.addProperty(name, value)
            is Number -> jsonObject.addProperty(name, value)
            is Boolean -> jsonObject.addProperty(name, value)
            else -> {
                MiniLog.e(TAG, "Property name: $name. You can put only: String, Char, Number, Boolean!")
            }
        }
        return this
    }

    fun addElement(name: String, element: JsonElement): JsonMaker {
        jsonObject.add(name, element)
        return this
    }

    fun make() : JsonObject  = jsonObject

    fun createOrderJsonObject(points: List<SearchPoint>) : JsonObject {

        var orderJsonObject = JsonObject()
        orderJsonObject.add("points", createJsonArray(points))

        return orderJsonObject
    }

    private fun createJsonArray(points: List<SearchPoint>) : JsonArray {

        var array = JsonArray()

        points.forEach { array.add(createPointJsonObject(it)) }

        return array
    }

    private fun createPointJsonObject(point: SearchPoint) : JsonObject {

        var pointJsonObject = JsonObject()

        pointJsonObject.addProperty("object_id", point.id ?: throw RuntimeException("No id or object_id"))

        if (point.metatype == 0) {
            pointJsonObject.addProperty("house", point.house)
            pointJsonObject.addProperty("enter", point.enter)
        }

        return pointJsonObject
    }

}