package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.dto.HistoryBag
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.model.Order
import ru.its.domain.repo.IHistoryRepository
import ru.its.domain.repo.SourceType
import rx.Observable

class HistoryRepository (prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI): MultiSourcesPrefsRepository<HistoryBag>(prefHelper, gson), IHistoryRepository {


    override fun countOfActualDays(): Int = 3

    override fun getFromNetwork(): Observable<HistoryBag?> {
        return rutaxiAPI.getHistoryOrders(1, 20)
                .filter { it != null }
                .map { orders ->

                    HistoryBag().also {
                        it.id = getKey().name
                        it.orders = orders?.map { if (it.status == 0) it.also { it.status = 104 } else it }
                    }.let { save(it) }

                }
    }

    override fun refreshFirstPage(): Observable<List<Order>?> {
       return getFromNetwork()
                .map { it?.orders }
    }

    override fun getFromPrefs(): Observable<HistoryBag?> {
        return Observable.just(1)
                .map { getFirst(HistoryBag::class.java) }
    }

    override fun getKey(): PrefHelper.Key = PrefHelper.Key.ORDERS_HISTORY
    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_HISTORY_LOAD


    override fun getFirstPage(sourceType: SourceType): Observable<List<Order>?> {
        return get(sourceType).map { it?.orders }
    }

    override fun getPage(page: Int): Observable<Pair<Int, List<Order>?>> {
        return rutaxiAPI.getHistoryOrders(page, 20)
                .map {
                    Pair(page, it)
                }
    }

    override fun removeOrder(id: String): Observable<Boolean> {
        return rutaxiAPI.removeOrder(id)
    }
}