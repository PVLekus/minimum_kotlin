package ru.its.data.repo

import android.content.Context
import android.content.SharedPreferences
import ru.its.data.utils.MiniLog


/**
 * Created by oleg on 08.05.17.
 */
class PrefHelper  {

    private var mPref: SharedPreferences? = null
    private var mEditor: SharedPreferences.Editor? = null
    private val TAG = "PrefHelper"
    private var SETTINGS_NAME = "default_settings"

    constructor(context: Context) {
        initInstance(context)
    }

    fun initInstance(context: Context) { mPref = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE) }

    fun remove(key: Key) {
        doEdit()
        mEditor?.remove(key.name)
        mEditor?.commit()
    }

    fun <T> put(key: Key, value: T?) {

        value?.let {
            doEdit()
            when(it) {
                is String -> mEditor?.putString(key.name, it)
                is Long -> mEditor?.putString(key.name, it.toString())
                is Int -> mEditor?.putString(key.name, it.toString())
                is Boolean -> mEditor?.putString(key.name, it.toString())
                else -> {
                    MiniLog.e(TAG, "Key: ${key.name}. You can put only: String, Long, Boolean!")
                }
            }

            mEditor?.commit()
        }

    }

    fun <T> get(key: Key, classOfT: Class<T>) : T? {


        return when {
            classOfT.isAssignableFrom(String::class.java) -> mPref?.getString(key.name, null) as T?
            classOfT.isAssignableFrom(Long::class.java) -> mPref?.getString(key.name, null)?.toLongOrNull() as T?
            classOfT.isAssignableFrom(Boolean::class.java) -> mPref?.getString(key.name, null)?.equals("true") as T?
            else -> {
                MiniLog.e(TAG, "Key: ${key.name}. You can get only: String, Long, Boolean! You try get ${classOfT.name} class")
                null
            }
        }

    }

    private fun doEdit() {
        if (mEditor == null) {
            mEditor = mPref?.edit()
        }
    }

    enum class Key {

        TEST,
        CITIES_LIST,
        HISTORY_POINTS,
        AIRPORTS_POINTS,
        POPULAR_POINTS,
        STATION_POINTS,
        USER,
        AVATAR,
        AUTO_LOCATION,
        AUTO_LOCATION_TEMP,
        ORDERS_HISTORY,
        ORDERS_CURRENT,
        TIME_CITIES_LOAD,
        TIME_HISTORY_LOAD,
        TIME_HISTORY_POINTS_LOAD,
        TIME_AIRPORTS_LOAD,
        TIME_STATIONS_LOAD,
        TIME_POPULAR_LOAD,
        TIME_ORDERS_LOAD,
        TIME_TOKEN_CHECK,
        LIST_USER_POINTS,
        TEMP_PHONE,
        FAVORITE_POINTS,
        TOKEN

    }

}