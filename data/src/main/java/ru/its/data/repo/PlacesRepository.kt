package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.*
import rx.Observable

/**
 * Created by oleg on 14.11.17.
 */
class HistoryPointsRepository(prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI, private val countOfActualDays: Long): PointsPrefsRepository(prefHelper, gson), IHistoryPointsRepository {

    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_HISTORY_POINTS_LOAD
    override fun getKey(): PrefHelper.Key = PrefHelper.Key.HISTORY_POINTS

    override fun countOfActualDays(): Int = countOfActualDays.toInt()

    override fun loadPoints(): Observable<List<SearchPoint>> = rutaxiAPI.getHistoryPoints()

    override fun getAll(): Observable<List<SearchPoint>?> = get(SourceType.BOTH).map { it?.points }
}

class AirportsRepository (prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI, private val countOfActualDays: Long): PointsPrefsRepository(prefHelper, gson), IAirportsRepository {

    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_AIRPORTS_LOAD
    override fun getKey(): PrefHelper.Key = PrefHelper.Key.AIRPORTS_POINTS

    override fun countOfActualDays(): Int = countOfActualDays.toInt()

    override fun loadPoints(): Observable<List<SearchPoint>> = rutaxiAPI.getAirportsPoints()

    override fun getAll(): Observable<List<SearchPoint>?> = get(SourceType.FIRST).map { it?.points }

}

class StationsRepository (prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI, private val countOfActualDays: Long): PointsPrefsRepository(prefHelper, gson), IStationsRepository {

    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_STATIONS_LOAD
    override fun getKey(): PrefHelper.Key = PrefHelper.Key.STATION_POINTS

    override fun countOfActualDays(): Int = countOfActualDays.toInt()

    override fun loadPoints(): Observable<List<SearchPoint>> = rutaxiAPI.getStationPoints()

    override fun getAll(): Observable<List<SearchPoint>?> = get(SourceType.FIRST).map { it?.points }

}

class PopularRepository (prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI, private val countOfActualDays: Long): PointsPrefsRepository(prefHelper, gson), IPopularRepository {

    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_POPULAR_LOAD
    override fun getKey(): PrefHelper.Key = PrefHelper.Key.POPULAR_POINTS

    override fun countOfActualDays(): Int = countOfActualDays.toInt()

    override fun loadPoints(): Observable<List<SearchPoint>> = rutaxiAPI.getPopularPoints()

    override fun getAll(): Observable<List<SearchPoint>?> = get(SourceType.FIRST).map { it?.points }

}