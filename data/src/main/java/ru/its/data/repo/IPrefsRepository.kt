package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.dto.PointsBag
import ru.its.data.utils.ActualStateChecker
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.SourceType
import rx.Observable

/**
 * Created by oleg on 30.11.17.
 */
abstract class MultiSourcesPrefsRepository<E : PrefsModel> (prefHelper: PrefHelper, gson: Gson) : BasePrefsRepository<E>(prefHelper, gson) {

    protected fun get(sourceType: SourceType) : Observable<E?> {

        return when (sourceType) {
            SourceType.CACHE -> getFromPrefs()
            SourceType.NETWORK -> getFromNetwork().doOnNext { prefHelper.put(getDateKey(), System.currentTimeMillis()) }
            SourceType.BOTH, SourceType.FIRST ->
                Observable.concat(getFromPrefs(), getFromNetwork().doOnNext { prefHelper.put(getDateKey(), System.currentTimeMillis()) })
                        .filter { filterForMultiplySources() }
                        .filter { it != null }
                        .take(if (sourceType == SourceType.FIRST) 1 else 2)
        }
    }

    abstract fun getDateKey(): PrefHelper.Key
    abstract fun countOfActualDays() : Int
    abstract fun getFromNetwork() : Observable<E?>
    abstract fun getFromPrefs() : Observable<E?>

    fun filterForMultiplySources() : Boolean {
        val dateOfLastLoad = prefHelper.get(getDateKey(), Long::class.java)
        return ActualStateChecker().isDateActualForDays(date = dateOfLastLoad, countOfDays = countOfActualDays())
    }

    override fun clear() {
        super.clear()
        prefHelper.put(getDateKey(), 0)
    }

}

abstract class PointsPrefsRepository(prefHelper: PrefHelper, gson: Gson) : MultiSourcesPrefsRepository<PointsBag>(prefHelper, gson) {

    override fun getFromNetwork(): Observable<PointsBag?> {

        return loadPoints()
                .filter { it != null }
                .map { points ->

                    PointsBag().also {
                        it.id = getKey().name
                        it.points = mutableListOf<SearchPoint>().also { it.addAll(points) }
                    }
                }
                .map { save(it) }

    }

    override fun getFromPrefs(): Observable<PointsBag?> {
        return Observable.just(1)
                .map { getFirst(PointsBag::class.java) }
    }

    abstract fun loadPoints() : Observable<List<SearchPoint>>
}

abstract class BasePrefsRepository<T: PrefsModel> (val prefHelper: PrefHelper, val gson: Gson){

    abstract fun getKey(): PrefHelper.Key

    fun save(item: T): T = item.also { prefHelper.put(getKey(), gson.toJson(it)) }
    open fun clear(): Unit = prefHelper.remove(getKey())
    fun getJson(): String? = prefHelper.get(getKey(), String::class.java)

    open fun getFirst(clazz: Class<T>): T? {
        return getJson()
                ?.let {
                    try {
                        Gson().fromJson<T>(it, clazz)
                    } catch (e: Exception) {
                        e?.printStackTrace()
                        clear()
                        null
                    }
                }

    }

}

abstract class PrefsModel