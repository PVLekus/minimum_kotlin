package ru.its.data.repo

import rx.Observable

/**
 * Created by oleg on 09.03.18.
 */
abstract class MSMemoryRepository<E> {

    private var item: E? = null

    abstract fun getFromNetwork() : Observable<E?>

    fun clear() {
        item = null
    }

    fun get() : Observable<E> {

        return Observable.concat(Observable.just(item), getFromNetwork().doOnNext { item = it })
                .filter{ it != null }
                .take(1)
                .map { it!! }
    }

}