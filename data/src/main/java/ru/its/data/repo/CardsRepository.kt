package ru.its.data.repo

import ru.its.data.remote.RutaxiAPI
import ru.its.domain.model.Card
import ru.its.domain.repo.ICardsRepository
import rx.Observable

/**
 * Created by oleg on 09.03.18.
 */
class CardsRepository(private val rutaxiAPI: RutaxiAPI): MSMemoryRepository<List<Card>>(), ICardsRepository {

    override fun getFromNetwork(): Observable<List<Card>?>  = rutaxiAPI.getCards()

    override fun getCard(): Observable<Card?> = get().map { it.firstOrNull() }

    override fun removeCard(): Observable<Boolean> {
        return getCard()
                .flatMap { rutaxiAPI.removeCard(it?.cardMask ?: "") }
                .doOnNext { clear() }
    }

    override fun addCard(): Observable<String> {
        return rutaxiAPI.addCard()
                .doOnNext { clear() }
    }
}