package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.dto.CitiesBag
import ru.its.data.remote.RutaxiAPI
import ru.its.domain.model.City
import ru.its.domain.repo.ICitiesRepository
import ru.its.domain.repo.SourceType
import rx.Observable

/**
 * Created by oleg on 30.10.17.
 */
class CitiesRepository(prefHelper: PrefHelper, gson: Gson, private val rutaxiAPI: RutaxiAPI, private val countOfActualDays: Long) : MultiSourcesPrefsRepository<CitiesBag>(prefHelper, gson), ICitiesRepository {

    override fun getDateKey(): PrefHelper.Key = PrefHelper.Key.TIME_CITIES_LOAD

    override fun getKey(): PrefHelper.Key = PrefHelper.Key.CITIES_LIST

    override fun getFromNetwork(): Observable<CitiesBag?> {

        return rutaxiAPI.getCities()
                .filter { it != null }
                .flatMap { Observable.from(it) }
                .filter { it.rate != null }
                .toList()
                .map { cities ->
                    CitiesBag().also {
                        it.cities = cities
                    }.let { save(it) }
                }

    }

    override fun getFromPrefs(): Observable<CitiesBag?> = Observable.just(1).map { getFirst(CitiesBag::class.java) }

    override fun countOfActualDays(): Int = countOfActualDays.toInt()

    override fun isCityExist(city: City?): Boolean {

        if (city == null) return false

        if (city.id == null) return false

        return getFirst(CitiesBag::class.java)?.cities?.firstOrNull { it.id == city.id } != null
    }

    override fun getFirstCity(name: String): City? = getFirst(CitiesBag::class.java)?.cities?.firstOrNull { it.name == name }

    override fun getFirstCity(): City? = getFirst(CitiesBag::class.java)?.cities?.firstOrNull()

    override fun getAll(sourceType: SourceType): Observable<List<City>?> {
        return get(sourceType).map { it?.cities }
    }
}