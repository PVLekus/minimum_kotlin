package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.dto.UserBag
import ru.its.domain.model.City
import ru.its.domain.model.Rate
import ru.its.domain.model.User
import ru.its.domain.repo.IUserRepository

/**
 * Created by oleg on 02.11.17.
 */
class UserRepository(prefHelper: PrefHelper, gson: Gson) : BasePrefsRepository<UserBag>(prefHelper, gson), IUserRepository {

    override fun getKey(): PrefHelper.Key = PrefHelper.Key.USER

    override fun updatePhone(phone: String): User? = getFirst(UserBag::class.java)?.let { save(it.also { it.user?.phone = phone }) }?.let { it.user }
    override fun updateName(name: String): User? = getFirst(UserBag::class.java)?.let { save(it.also { it.user?.name = name }) }?.let { it.user }
    override fun updateToken(token: String): User? = getFirst(UserBag::class.java)?.let { save(it.also { it.user?.token = token }) }?.let { it.user }
    override fun updateCity(city: City): User? = getFirst(UserBag::class.java)?.let { save(it.also { it.user?.city = city }) }?.let { it.user }
    override fun updateRate(rate: Rate?): User? = getFirst(UserBag::class.java)?.let { save(it.also { it.user?.city = it.user?.city?.also { it.rate = rate } }) }?.let { it.user }
    override fun getUser(): User? = getFirst(UserBag::class.java)?.user
    override fun save(user: User?): User? = save(UserBag().also { it.user = user })?.let { it.user }

    override fun isCardPayment(): Boolean = getUser()?.city?.rate?.params?.cardPayment == "1"

    override fun clear() {
        super.clear()
        prefHelper.remove(PrefHelper.Key.TOKEN)
    }

}