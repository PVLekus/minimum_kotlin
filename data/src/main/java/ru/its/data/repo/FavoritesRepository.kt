package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.remote.RutaxiAPI
import ru.its.data.utils.genericType
import ru.its.domain.model.FavoritePoint
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IFavoritesRepository
import ru.its.domain.repo.SourceType
import rx.Observable

class FavoritesRepository(private val prefHelper: PrefHelper,
                          private val rutaxiAPI: RutaxiAPI,
                          private val gson: Gson): IFavoritesRepository {

    private var favorites: MutableList<FavoritePoint>? = null

    override fun addToFavorite(searchPoint: SearchPoint, name: String?): Observable<Boolean> {
        return rutaxiAPI.addFavorite(searchPoint, name)
    }

    override fun removeFavorite(favoritePoint: FavoritePoint): Observable<Boolean> {
        return rutaxiAPI.removeFromFavorite(favoritePoint.id!!)
                .doOnNext {
                    favorites?.remove(favoritePoint)
                    val json = gson.toJson(favorites)
                    prefHelper.put(PrefHelper.Key.FAVORITE_POINTS, json)
                }
    }

    fun fetchFavorites(): Observable<List<FavoritePoint>?> {
        return rutaxiAPI.getFavoritePoints()
                .doOnNext {
                    it?.let {
                        fillMap(it)
                        val json = gson.toJson(it)
                        prefHelper.put(PrefHelper.Key.FAVORITE_POINTS, json)
                    }
                }
    }

    fun getFavorites(): Observable<List<FavoritePoint>?> {
        if (favorites?.isEmpty() == false) {
            return Observable.just(favorites)
        } else {
            val json = prefHelper.get(PrefHelper.Key.FAVORITE_POINTS, String::class.java) ?: return Observable.just(null)
            val type = genericType<List<FavoritePoint>>()
            val list = gson.fromJson<List<FavoritePoint>>(json, type)
            fillMap(list)
            return Observable.just(list)
        }

    }

    fun fillMap(list: List<FavoritePoint>) {
        favorites = mutableListOf()
        favorites?.addAll(list)
    }

    override fun getAll(sourceType: SourceType): Observable<List<FavoritePoint>?> {

        return when (sourceType) {

            SourceType.CACHE -> getFavorites()
            SourceType.NETWORK -> fetchFavorites()
            SourceType.BOTH, SourceType.FIRST ->
                Observable.concat(getFavorites(), fetchFavorites())
                        .filter { it != null }
                        .take(if (sourceType == SourceType.FIRST) 1 else 2)
        }

    }

    override fun clear() {
        favorites?.clear()
        prefHelper.remove(PrefHelper.Key.FAVORITE_POINTS)
    }
}