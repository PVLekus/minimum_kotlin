package ru.its.data.repo

import com.google.gson.Gson
import ru.its.data.dto.BookingBag
import ru.its.domain.model.Booking
import ru.its.domain.model.SearchPoint
import ru.its.domain.repo.IBookingRepository
import rx.Observable

/**
 * Created by oleg on 15.11.17.
 */
class BookingRepository (prefHelper: PrefHelper, gson: Gson) : BasePrefsRepository<BookingBag>(prefHelper, gson), IBookingRepository {

    override fun getKey(): PrefHelper.Key = PrefHelper.Key.LIST_USER_POINTS

    override fun getFirst(clazz: Class<BookingBag>): BookingBag? {
        var bag = super.getFirst(clazz) ?: save(BookingBag().also { it.booking = Booking() })
        if (bag.booking?.points?.count()!! < 2) bag = save(BookingBag().also { it.booking = Booking() })

        return (super.getFirst(BookingBag::class.java) ?: bag) .also { it?.booking?.points?.get(0)?.preOrder = it?.booking?.preOrderTime }
    }

    override fun setCardPay(isCard: Boolean) {
        getFirst(BookingBag::class.java)?.let { save(it.also { it.booking?.isCardPay = isCard }) }
    }

    override fun save(booking: Booking): Booking {
        return getFirst(BookingBag::class.java)!!.let { save(it.also { it.booking = booking }) }.booking!!
    }

    override fun get(): Observable<Booking> {
        return Observable.just(1).map { getFirst(BookingBag::class.java)?.booking!! }
    }

    override fun updateComment(comment: String?): Observable<Booking> {

        return get()
                .map { booking ->
                    booking.also {
                        comment?.let { booking.comment = it }
                    }
                }
                .map { save(it) }

    }

    override fun updateItemByPosition(searchPoint: SearchPoint, position: Int, comment: String?): Observable<List<SearchPoint>> {

        return get()
                .map { booking ->
                    booking.also {
                        if (position == 0) it.preOrderTime = searchPoint.preOrder
                        it.points.removeAt(position)
                        it.points.add(position, searchPoint)
                        if (comment != null && position == 0) booking.comment = comment!!
                    }
                }
                .map { save(it) }
                .map { it.points }

    }

    override fun deleteItemByPosition(position: Int): Observable<List<SearchPoint>> {
        return get()
                .map { it.also { it.points.removeAt(position) } }
                .map { save(it) }
                .map { it.points }

    }

    override fun clearItemByPosition(position: Int): Observable<List<SearchPoint>> {
        return get()
                .map { it.also { it.points[position] = SearchPoint() } }
                .map { save(it) }
                .map { it.points }
    }

    override fun addNewPoint(): Observable<List<SearchPoint>> {
        return get()
                .map {
                    it.also { it.points.add(SearchPoint()) }
                }
                .doOnNext { println(Gson().toJson(it)) }
                .map { save(it) }
                .map { it.points }
    }

    override fun dicreaseItemPosition(position: Int): Observable<List<SearchPoint>> {

        return get()
                .map {
                    it.also {
                        if (position > 0) {
                            val point = it.points.removeAt(position)
                            it.points.add(position - 1, point)
                        }
                    }
                }
                .map { save(it) }
                .map { it.points }

    }

    override fun increaseItemPosition(position: Int): Observable<List<SearchPoint>> {

        return get()
                .map {
                    it.also {
                        if (it.points.lastIndex != position) {
                            val point = it.points.removeAt(position)
                            it.points.add(position + 1, point)
                        }
                    }
                }
                .map { save(it) }
                .map { it.points }

    }

}