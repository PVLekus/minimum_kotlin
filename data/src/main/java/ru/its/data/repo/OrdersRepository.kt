package ru.its.data.repo

import ru.its.domain.model.Order
import ru.its.domain.repo.IHistoryRepository
import ru.its.domain.repo.IOrdersRepository
import ru.its.domain.repo.SourceType
import rx.Observable

/**
 * Created by oleg on 07.11.17.
 */
class OrdersRepository(private val historyRepository: IHistoryRepository) :IOrdersRepository {

    private var orders: MutableList<Order>? = null


    fun getFromNetwork(): Observable<List<Order>?> {

        return historyRepository.getFirstPage(SourceType.NETWORK)
                .filter { it != null }
                .map { it?.filter { it.status ?: 0 in 1..99  && it.canceled != 1} }
                .doOnNext {
                    if (orders == null) orders = mutableListOf()
                    orders?.clear()
                    orders?.addAll(it!!)
                }
    }

    override fun updateOrder(order: Order): Observable<Order> =
            Observable.just(order)
                    .doOnNext {
                        val index = orders?.indexOf(it) ?: -1
                        if (index >= 0) orders?.set(index, it)
                    }

    override fun getAll(sourceType: SourceType): Observable<List<Order>?> {
        return when (sourceType) {

            SourceType.CACHE -> Observable.just(orders)
            SourceType.NETWORK -> getFromNetwork()
            SourceType.BOTH, SourceType.FIRST ->
                Observable.concat(Observable.just(orders), getFromNetwork())
                        .filter { it != null }
                        .take(if (sourceType == SourceType.FIRST) 1 else 2)
        }
    }

    override fun getOrder(id: String): Observable<Order?> {
        return historyRepository.getFirstPage(SourceType.NETWORK)
                .doOnNext {
                    it?.filter { it.status ?: 0 in 1..99  && it.canceled != 1}
                            ?.let {
                                if (orders == null) orders = mutableListOf()
                                orders?.clear()
                                orders?.addAll(it)
                            }
                }
                .map {
                    it?.firstOrNull { it.id == id }
                }
    }

    override fun clear() {
        orders = null
    }
}